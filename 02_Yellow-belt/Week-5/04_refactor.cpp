// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class People {
 public:
  People(const string &type, const string &name) : type_(type), name_(name) {}

  virtual void Walk(const string &destination) const {
	cout << GetTypeAndName() << " walks to: " << destination << endl;
  }
  string GetType() const {
	return type_;
  }
  string GetName() const {
	return name_;
  }
  string GetTypeAndName() const {
	return GetType() + ": " + GetName();
  }

 private:
  const string type_;
  const string name_;
};

class Student : public People {
 public:
  Student(const string &name, const string &favourite_song) : People("Student", name), favourite_song_(favourite_song) {}

  void Learn() const {
	cout << GetTypeAndName() << " learns" << endl;
  }
  void Walk(const string &destination) const override {
	People::Walk(destination);
	SingSong();
  }
  void SingSong() const {
	cout << GetTypeAndName() << " sings a song: " << GetFavoriteSong() << endl;
  }
  string GetFavoriteSong() const {
	return favourite_song_;
  }

 private:
  const string favourite_song_;
};

class Teacher : public People {
 public:
  Teacher(const string &name, const string &subject) : People("Teacher", name), subject_(subject) {}

  void Teach() const {
	cout << GetTypeAndName() << " teaches: " << subject_ << endl;
  }

 private:
  const string subject_;
};

class Policeman : public People {
 public:
  Policeman(const string &name) : People("Policeman", name) {}

  void Check(const People &p) const {
	cout << GetTypeAndName() << " checks " << p.GetType() << ". " << p.GetType() << "'s name is: " << p.GetName() << endl;
  }
};

void VisitPlaces(const People &p, const vector<string> &places) {
  for (const string &pl: places) {
	p.Walk(pl);
  }
}

int main() {
  Teacher t("Jim", "Math");
  Student s("Ann", "We will rock you");
  Policeman p("Bob");

  VisitPlaces(t, {"Moscow", "London"});
  p.Check(s);
  VisitPlaces(s, {"Moscow", "London"});
  return 0;
}