// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>

using namespace std;

class A {
 public:
  A(string name): name_(name) {
  }
 private:
  string name_;
};

class B : public A {
 public:
  B(string name) : A(name), base_(name) {
  }
 private:
  A base_;
};



int main() {


  return 0;
}