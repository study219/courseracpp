// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <memory>
#include <vector>
#include <string>
#include <sstream>
#include <iomanip>
#include <cmath>

using namespace std;

class Figure {
 public:
  virtual string Name() const = 0;
  virtual double Perimeter() const = 0;
  virtual double Area() const = 0;
};

class Triangle : public Figure {
 public:
  Triangle(const string &name, const double &a, const double &b, const double &c)
	  : name_(name), perimeter_(a + b + c), area_(sqrt(perimeter_/2*(perimeter_/2-a)*(perimeter_/2-b)*(perimeter_/2-c))) {
  }
  string Name() const override {
	return name_;
  }
  double Perimeter() const override {
	return perimeter_;
  }
  double Area() const override {
	return area_;
  }
 private:
  const string name_;
  const double perimeter_;
  const double area_;
};

class Rect : public Figure {
 public:
  Rect(const string &name, const double &a, const double &b)
	  : name_(name), perimeter_(2 * (a + b)), area_(a * b) {
  }
  string Name() const override {
	return name_;
  }
  double Perimeter() const override {
	return perimeter_;
  }
  double Area() const override {
	return area_;
  }
 private:
  const string name_;
  const double perimeter_;
  const double area_;
};

class Circle : public Figure {
 public:
  Circle(const string &name, const double &a)
	  : name_(name), perimeter_(2 * 3.14 * a), area_(3.14 * a * a) {
  }
  string Name() const override {
	return name_;
  }
  double Perimeter() const override {
	return perimeter_;
  }
  double Area() const override {
	return area_;
  }
 private:
  const string name_;
  const double perimeter_;
  const double area_;
};

shared_ptr<Figure> CreateFigure(istringstream &is) {
  string name;
  is >> name;
  if (name == "CIRCLE") {
	double a;
	is >> a;
	return make_shared<Circle>(name, a);
  } else if (name == "TRIANGLE") {
	double a, b, c;
	is >> a >> b >> c;
	return make_shared<Triangle>(name, a, b, c);
  } else {
	double a, b;
	is >> a >> b;
	return make_shared<Rect>(name, a, b);
  }
}

int main() {
  vector<shared_ptr<Figure>> figures;
  for (string line; getline(cin, line);) {
	istringstream is(line);

	string command;
	is >> command;
	if (command == "ADD") {
	  // Пропускаем "лишние" ведущие пробелы.
	  // Подробнее об std::ws можно узнать здесь:
	  // https://en.cppreference.com/w/cpp/io/manip/ws
	  is >> ws;
	  figures.push_back(CreateFigure(is));
	} else if (command == "PRINT") {
	  for (const auto &current_figure: figures) {
		cout << fixed << setprecision(3)
			 << current_figure->Name() << " "
			 << current_figure->Perimeter() << " "
			 << current_figure->Area() << endl;
	  }
	}
  }

  return 0;
}