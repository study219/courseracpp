// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <utility>

using namespace std;

class Animal {
 public:
  // ваш код
  explicit Animal(string  t) : Name(std::move(t)){}

  const string Name;
};


class Dog : public Animal{
 public:
  // ваш код
  explicit Dog(const string& t) : Animal(t){}

  void Bark() {
	cout << Name << " barks: woof!" << endl;
  }
};


int main() {
	Dog d("DOG");
	d.Bark();


  return 0;
}