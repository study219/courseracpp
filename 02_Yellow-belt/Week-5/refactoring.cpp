#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Person {
 public:
  Person(const string &name, const string& profession) :Name(name) , Profession(profession){}
  virtual void Walk(const string& destination) const {
	cout << Profession<<": " << Name << " walks to: " << destination << endl;
  };

  string ProfessionAndName() const {
	return Profession + ": " + Name ;
  }
 public:
  const string Name;
  const string Profession;
};

class Student:public Person {
 public:
  Student(const string& name, const string& favouriteSong):Person(name, "Student"), FavouriteSong(favouriteSong){}

  void Learn()const  {
	cout << ProfessionAndName() << " learns" << endl;
  }
  void Walk(const string& destination) const override{
	Person::Walk(destination);
	SingSong();
  }
  void SingSong() const{
	cout << ProfessionAndName() << " sings a song: " << FavouriteSong << endl;
  }

 private:
  const string FavouriteSong;
};

class Teacher:public Person {
 public:
  Teacher(const string& name, const string& subject):Person(name, "Teacher"), Subject(subject){}

  void Teach() const{
	cout << ProfessionAndName() << " teaches: " << Subject << endl;
  }
 public:
  const string Subject;
};

class Policeman: public Person {
 public:
  Policeman(const string& name):Person(name, "Policeman") {}

  void Check(const Person &t)  const{
	cout << ProfessionAndName() << " checks "<< t.Profession<<". " << t.Profession << "'s name is: " << t.Name << endl;
  }
};

void VisitPlaces(Person& p, const vector<string>& places) {
  for (auto it : places) {
	p.Walk(it);
  }
}

int main() {
  Teacher t("Jim", "Math");
  Student s("Ann", "We will rock you");
  Policeman p("Bob");

  VisitPlaces(t, { "Moscow", "London" });
  p.Check(s);
  VisitPlaces(s, { "Moscow", "London" });
  return 0;
}
