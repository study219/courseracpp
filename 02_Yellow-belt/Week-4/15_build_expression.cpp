// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <deque>
#include <string>

using namespace std;



int main() {
  string x;
  cin >> x;
  deque<string> expression;
  expression.push_back(x);


  int q;
  cin >> q;
  cin.ignore(1);
  for (int i = 0; i < q; ++i) {
	expression.push_front("(");
	expression.emplace_back(")");
	string line;
	getline(cin,line);
	expression.emplace_back(" ");
	expression.push_back(line);
  }

  for (const auto& s: expression) {
		cout << s;
  }
  cout << endl;
  return 0;
}