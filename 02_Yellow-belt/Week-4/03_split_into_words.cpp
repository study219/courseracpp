// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

vector<string> SplitIntoWords(const string& s){
  vector<string> result;
  auto it_begin = begin(s);
  auto it = begin(s);
  while (it != end(s)){
	it_begin = it;
	it = find(it_begin,  s.end(), ' ');
	result.emplace_back(it_begin,it);
	if (it != end(s)){
	  ++it;
	}
  }
  return result;
}

int main() {
  string s = "C Cpp Java Python";

  vector<string> words = SplitIntoWords(s);
  cout << words.size() << " ";
  for (auto it = begin(words); it != end(words); ++it) {
	if (it != begin(words)) {
	  cout << "/";
	}
	cout << *it;
  }
  cout << endl;

  return 0;
}