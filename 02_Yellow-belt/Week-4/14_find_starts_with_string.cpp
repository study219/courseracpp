// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <set>
#include <functional>
#include <numeric>
#include <vector>
#include <iostream>
#include <iterator>
#include <functional>

using namespace std;

template<typename RandomIt>
pair<RandomIt, RandomIt> FindStartsWith(RandomIt range_begin, RandomIt range_end, const string &prefix) {
  return equal_range(range_begin, range_end, prefix, [prefix](auto x, auto y) {
	if (x == prefix && x.size() < y.size()) {
	  y.resize(x.size());
	}
	return x < y;
  });
}

int main() {
  const vector<string> sorted_strings = {"moscow", "motovilikha", "murmansk"};

  const auto mo_result =
	  FindStartsWith(begin(sorted_strings), end(sorted_strings), "mo");
  for (auto it = mo_result.first; it != mo_result.second; ++it) {
	cout << *it << " ";
  }
  cout << endl;

  const auto mt_result =
	  FindStartsWith(begin(sorted_strings), end(sorted_strings), "mt");
  cout << (mt_result.first - begin(sorted_strings)) << " " <<
	   (mt_result.second - begin(sorted_strings)) << endl;

  const auto na_result =
	  FindStartsWith(begin(sorted_strings), end(sorted_strings), "na");
  cout << (na_result.first - begin(sorted_strings)) << " " <<
	   (na_result.second - begin(sorted_strings)) << endl;

  return 0;
}