// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <set>
#include <functional>
#include <numeric>
#include <vector>
#include <iostream>
#include <iterator>
#include <functional>

using namespace std;

set<int>::const_iterator FindNearestElement(const set<int> &numbers, int border) {
  auto first = numbers.lower_bound(border);
  if (first == numbers.begin()) {
	return numbers.begin();
  }
  auto previos = prev(first);
  if (first == numbers.end()){
	return previos;
  }

  if (border - *previos <= *first - border){
	return previos;
  }
  return  first;
}

int main() {
  set<int> numbers = {1, 4, 6};
  cout <<
	   *FindNearestElement(numbers, 0) << " " <<
	   *FindNearestElement(numbers, 3) << " " <<
	   *FindNearestElement(numbers, 5) << " " <<
	   *FindNearestElement(numbers, 6) << " " <<
	   *FindNearestElement(numbers, 100) << endl;

  set<int> empty_set;

  cout << (FindNearestElement(empty_set, 8) == end(empty_set)) << endl;
  return 0;
}