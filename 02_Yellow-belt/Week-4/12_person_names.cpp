// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <map>

using namespace std;

class Person {
 public:
  void ChangeFirstName(int year, const string &first_name) {
	// добавить факт изменения имени на first_name в год year
	first_name_[year] = first_name;
  }
  void ChangeLastName(int year, const string &last_name) {
	// добавить факт изменения фамилии на last_name в год year
	last_name_[year] = last_name;
  }
  string GetFullName(int year) {
	// получить имя и фамилию по состоянию на конец года year
	//return last_name.lower_bound(year)->second + " " + first_name.lower_bound(year)->second;
	if (first_name_.begin()->first > year && last_name_.begin()->first > year) {
	  return "Incognito";
	} else {
	  string fn;
	  string ln;

	  /*  for (const auto&[kYear, kFullname]: full_name_) {
		  if (kYear <= year && !kFullname.first_name.empty()) {
			fn = kFullname.first_name;
		  }
		  if (kYear <= year && !kFullname.last_name.empty()) {
			ln = kFullname.last_name;
		  }
		}*/
	  auto fn_it = first_name_.upper_bound(year);
	  if (fn_it != first_name_.begin()) {
		fn = prev(fn_it)->second;
	  }

	  auto ln_it = last_name_.upper_bound(year);
	  if (ln_it != last_name_.begin()) {
		ln = prev(ln_it)->second;
	  }

	  /* for (const auto&[kYear, kFirstName]: first_name_) {
		 if (kYear <= year ) {
		   fn = kFirstName;
		 }
	   }

	   for (const auto&[kYear, kLastName]: last_name_) {
		 if (kYear <= year ) {
		   ln = kLastName;
		 }
	   }*/

	  if (fn.empty()) {
		return ln + " with unknown first name";
	  } else if (ln.empty()) {
		return fn + " with unknown last name";
	  } else {
		return fn + " " + ln;
	  }
	}

  }
 private:
  // приватные поля
  map<int, string> first_name_;
  map<int, string> last_name_;
};

int main() {
  Person person;

  person.ChangeFirstName(1965, "Polina");
  person.ChangeLastName(1967, "Sergeeva");
  for (int year: {1900, 1965, 1990}) {
	cout << person.GetFullName(year) << endl;
  }

  person.ChangeFirstName(1970, "Appolinaria");
  for (int year: {1969, 1970}) {
	cout << person.GetFullName(year) << endl;
  }

  person.ChangeLastName(1968, "Volkova");
  for (int year: {1969, 1970}) {
	cout << person.GetFullName(year) << endl;
  }

  return 0;
}