// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <ctime>
#include <numeric>
#include <tuple>

using namespace std;

class Date {
 public:
  Date() = default;
  Date(int year, int month, int day) : year_(year - 1900), month_(month - 1), day_(day) {}
  bool operator==(const Date &rhs) const {
	return year_ == rhs.year_ &&
		month_ == rhs.month_ &&
		day_ == rhs.day_;
  }
  bool operator!=(const Date &rhs) const {
	return !(rhs == *this);
  }
  bool operator<(const Date &rhs) const {
	return tie(year_, month_, day_) < tie(rhs.year_, rhs.month_, rhs.day_);
  }
  bool operator>(const Date &rhs) const {
	return rhs < *this;
  }
  bool operator<=(const Date &rhs) const {
	return !(rhs < *this);
  }
  bool operator>=(const Date &rhs) const {
	return !(*this < rhs);
  }
  friend ostream &operator<<(ostream &os, const Date &date) {
	os << "year_: " << date.year_ << " month_: " << date.month_ << " day_: " << date.day_;
	return os;
  }

  friend istream &operator>>(istream &is, Date &date) {
	is >> date.year_;
	is.ignore(1);
	is >> date.month_;
	is.ignore(1);
	is >> date.day_;

	date.year_ -= 1900;
	date.month_ -= 1;
	return is;
  }
  int GetYear() const {
	return year_;
  }
  int GetMonth() const {
	return month_;
  }
  int GetDay() const {
	return day_;
  }
 private:
  int year_;
  int month_;
  int day_;
};

int DateDistance(const Date &date_from, const Date &date_to) {
  struct std::tm a = {0, 0, 0, date_from.GetDay(), date_from.GetMonth(), date_from.GetYear()}; /* June 24, 2004 */
  struct std::tm b = {0, 0, 0, date_to.GetDay(), date_to.GetMonth(), date_to.GetYear()};
  std::time_t x = std::mktime(&a);
  std::time_t y = std::mktime(&b);
  if (x != (std::time_t)(-1) && y != (std::time_t)(-1)) {
	double difference = std::difftime(y, x) / (60 * 60 * 24);
	return static_cast<int>(difference);
  }
  return -1;
}

const Date kStartDate = Date(2000, 01, 01);
const Date kEndDate = Date(2100, 01, 01);
const size_t kDayCount = DateDistance(kStartDate, kEndDate);

int main() {
  // Show time/date using default formatting
  cout.precision(25);
  vector<uint64_t> budget(kDayCount, .0);
  vector<uint64_t> partial_sums(kDayCount, .0);
  int q;
  cin >> q;

  while (q) {

	Date date{};
	cin >> date;
	auto index = DateDistance(kStartDate, date);
	uint64_t value;
	cin >> value;
	budget[index] += value;
	--q;
  }
  partial_sum(budget.begin(), budget.end(), partial_sums.begin());

  cin >> q;
  while (q) {
	Date date_from{}, date_to{};
	cin >> date_from >> date_to;

	auto start_index = DateDistance(kStartDate, date_from);
	auto finish_index = DateDistance(kStartDate, date_to);

	if (start_index > 0) {
	  cout << partial_sums[finish_index] - partial_sums[start_index - 1] << '\n';
	} else {
	  cout << partial_sums[finish_index] << '\n';
	}
	--q;
  }

  return 0;
}