// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <set>
#include <functional>
#include <numeric>
#include <vector>
#include <iostream>
#include <iterator>
#include <functional>

using namespace std;

bool f(int a) {
  return a > 0;
}


template <typename RandomIt>
pair<RandomIt, RandomIt> FindSegment(
	RandomIt range_begin, RandomIt range_end, int left, int right) {
  // ТЕЛО ФУНКЦИИ

  return {lower_bound(range_begin, range_end, left),
		  lower_bound(range_begin, range_end, right)};
}


int main() {

  vector<int> s = {-2, -1, 0, 1, 2, 3, 4, 5,6,7,8};
  auto p = FindSegment(s.begin(),  s.end(), 1,3);

  for (auto i = p.first; i < p.second; ++i) {
	cout << *i << " ";
  }

 /* set<int> s = {-2, -1, 0, 1, 2, 3, 4, 5};
  vector<int> v;

  v.assign(begin(s), end(s));
  auto it = remove_if(begin(v), end(v), f);
  v.erase(it, end(v));

   for (auto& t: v) {
		 cout << t << " ";
   }*/

 /* vector<int> vec;
  auto vector_begin = begin(vec);
  auto vector_end = end(vec);

  string str;
  auto string_begin = begin(str);
  auto string_end = end(str);

  set<int> s;
  auto set_begin = begin(s);
  auto set_end = end(s);

  partial_sum(set_begin, set_end, back_inserter(vec));*/

  return 0;
}