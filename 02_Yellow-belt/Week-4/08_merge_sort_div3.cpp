// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

template <typename RandomIt>
void MergeSort(RandomIt range_begin, RandomIt range_end){
  if (range_end - range_begin < 2){
	return;
  }
  vector<typename RandomIt::value_type> result(range_begin, range_end);
  auto fist_part = result.begin() + result.size() / 3;
  auto second_part = result.begin() + result.size() / 3 * 2;
  MergeSort(result.begin(), fist_part);
  MergeSort(fist_part, second_part);
  MergeSort(second_part, result.end());
  vector<typename RandomIt::value_type> temp;
  merge(result.begin(),fist_part,fist_part,second_part, back_inserter(temp));
  merge(temp.begin(),temp.end(),second_part,result.end(), range_begin);
 // merge(result.begin(),middle,middle,result.end(),range_begin);
}

int main() {
  vector<int> v = {6, 4, 7, 6, 4, 4, 0, 1, 5};
  MergeSort(begin(v), end(v));
  for (int x : v) {
	cout << x << " ";
  }
  cout << endl;
  return 0;
}