// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

enum class Gender {
  FEMALE,
  MALE
};

struct Person {
  int age;  // возраст
  Gender gender;  // пол
  bool is_employed;  // имеет ли работу
};

template<typename InputIt>
int ComputeMedianAge(InputIt range_begin, InputIt range_end) {
  if (range_begin == range_end) {
	return 0;
  }
  vector<typename InputIt::value_type> range_copy(range_begin, range_end);
  auto middle = begin(range_copy) + range_copy.size() / 2;
  nth_element(
	  begin(range_copy), middle, end(range_copy),
	  [](const Person &lhs, const Person &rhs) {
		return lhs.age < rhs.age;
	  }
  );
  return middle->age;
}

void PrintStats(vector<Person> persons) {
  cout << "Median age = " << ComputeMedianAge(persons.begin(), persons.end()) << endl;
  auto females_it =
	  partition(persons.begin(), persons.end(), [](const Person &p) {
		return p.gender == Gender::FEMALE;
	  });
  cout << "Median age for females = "
	   << ComputeMedianAge(persons.begin(), females_it) << endl;
  cout << "Median age for males = "
	   << ComputeMedianAge(females_it, persons.end()) << endl;

  auto females_employed_it =
	  partition(persons.begin(), females_it, [](const Person &p) {
		return p.is_employed;
	  });

  cout << "Median age for employed females = "
	   << ComputeMedianAge(persons.begin(), females_employed_it) << endl;
  cout << "Median age for unemployed females = "
	   << ComputeMedianAge(females_employed_it, females_it) << endl;

  auto males_employed_it =
	  partition(females_it, persons.end(), [](const Person &p) {
		return p.is_employed;
	  });

  cout << "Median age for employed males = "
	   << ComputeMedianAge(females_it, males_employed_it) << endl;
  cout << "Median age for unemployed males = "
	   << ComputeMedianAge(males_employed_it, persons.end()) << endl;

}

int main() {
  vector<Person> persons = {
	  {31, Gender::MALE, false},
	  {40, Gender::FEMALE, true},
	  {24, Gender::MALE, true},
	  {20, Gender::FEMALE, true},
	  {80, Gender::FEMALE, false},
	  {78, Gender::MALE, false},
	  {10, Gender::FEMALE, false},
	  {55, Gender::MALE, true},
  };
  PrintStats(persons);
  return 0;
}