// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

int main() {
  int n;
  cin >> n;
  vector<int> permutation(n);
  for (auto &i: permutation) {
	i = n--;
  }

  do {
	for (auto &i: permutation) {
	  cout << i << ' ';
	}
	cout << endl;
  } while (std::prev_permutation(permutation.begin(), permutation.end()));

  return 0;
}