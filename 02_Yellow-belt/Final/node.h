// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#pragma once
#include "date.h"
#include <memory>

enum class LogicalOperation {
  And,
  Or
};

enum class Comparison {
  Less,
  LessOrEqual,
  Greater,
  GreaterOrEqual,
  Equal,
  NotEqual
};

class Node {
 public:
  virtual bool Evaluate(const Date& date, const string& event) const = 0;
};

class EmptyNode : public Node {
 public:
  EmptyNode() = default;
  bool Evaluate(const Date& date, const string& event) const override;
};

template<typename T>
bool Comporator(const Comparison& cmp, const T& lhs, const T& rhs );



class DateComparisonNode : public Node {
 public:
	DateComparisonNode(const Comparison& cmp, const Date& date);
	bool Evaluate(const Date& date, const string& event) const override;
 private:
  const Comparison cmp_;
  const Date date_;
};

class EventComparisonNode : public Node {
 public:
  EventComparisonNode(const Comparison& cmp, const string& event);
  bool Evaluate(const Date& date, const string& event) const override;
 private:
  const Comparison cmp_;
  const string event_;
};

class LogicalOperationNode : public Node {
 public:
  LogicalOperationNode(const LogicalOperation& op, const shared_ptr<Node>& left, const shared_ptr<Node>& right);
  bool Evaluate(const Date& date, const string& event) const override;
 private:
  const LogicalOperation op_;
  const shared_ptr<Node> left_;
  const shared_ptr<Node> right_;
};


