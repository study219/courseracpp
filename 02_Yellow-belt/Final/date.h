// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#pragma once
#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
#include <tuple>

using namespace std;

class Date {
 public:
  Date() = default;
  Date(int year, int month, int day);
  int GetYear() const;
  int GetMonth() const;
  int GetDay() const;
  string GetData() const;

  bool operator<(const Date &rhs) const;
  bool operator>(const Date &rhs) const;
  bool operator<=(const Date &rhs) const;
  bool operator>=(const Date &rhs) const;
  bool operator==(const Date &rhs) const;
  bool operator!=(const Date &rhs) const;
  friend ostream &operator<<(ostream &os, const Date &date);
  friend istream &operator>>(istream &os, Date &date);
 private:
  int year_;
  int month_;
  int day_;
};


Date ParseDate(istream &is);
