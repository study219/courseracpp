// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "database.h"

void Database::Add(const Date &date, const string &event) {
  if (db_unique_[date].insert(event).second) {
	db_[date].push_back(event);
  }

}

void Database::Print(ostream &os) const {
  for (const auto&[kDate, kEvents]: db_) {
	for (const auto &k_event: kEvents) {
	  os << kDate << " " << k_event << endl;
	}
  }
}

string Database::Last(const Date &date) const {
  if (db_.empty() || db_.begin()->first > date) {
	throw invalid_argument("No entries");
  }

  auto date_bigger = db_.upper_bound(date);
  if (date_bigger == db_.begin()) {
	throw invalid_argument("No entries");
  }
  --date_bigger;

  return date_bigger->first.GetData() + " " + date_bigger->second.back();
}

int Database::RemoveIf(const function<bool(const Date &, const string &)> &predicate) {
  int result = 0;
  vector<Date> date_to_delete;
  for (auto &[date, events]: db_) {

	auto pred = [predicate, &date = date](const string &event) {
	  return predicate(date, event);
	};

	auto find_it = stable_partition(events.begin(), events.end(), pred);
	result += static_cast<int>(distance(events.begin(), find_it)); // result += find_it - events.begin();
	events.erase(events.begin(), find_it);
	db_unique_[date] = set<string>(events.begin(), events.end());

	if (events.empty()) {
	  date_to_delete.push_back(date);
	}
  }
  for (const auto &x: date_to_delete) {
	db_.erase(x);
	db_unique_.erase(x);
  }
  return result;
}

vector<pair<Date, string>> Database::FindIf(const function<bool(const Date &, const string &)> &predicate) const {
  vector<pair<Date, string>> result;
  for (const auto &[date, events]: db_) {
	for (const auto &event: events) {
	  if (predicate(date, event)) {
		result.emplace_back(date, event);
	  }
	}
  }
  return result;
}
