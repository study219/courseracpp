// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "date.h"

Date::Date(int year, int month, int day) : year_(year), month_(month), day_(day) {
  if (month > 12 || month < 1) throw domain_error("Month value is invalid: " + to_string(month));
  if (day > 31 || day < 1) throw domain_error("Day value is invalid: " + to_string(day));
}
int Date::GetYear() const {
  return year_;
}
int Date::GetMonth() const {
  return month_;
}
int Date::GetDay() const {
  return day_;
}
bool Date::operator>(const Date &rhs) const {
  return rhs < *this;
}
bool Date::operator<=(const Date &rhs) const {
  return !(rhs < *this);
}
bool Date::operator>=(const Date &rhs) const {
  return !(*this < rhs);
}
bool Date::operator==(const Date &rhs) const {
  return year_ == rhs.year_ &&
	  month_ == rhs.month_ &&
	  day_ == rhs.day_;
}
bool Date::operator!=(const Date &rhs) const {
  return !(rhs == *this);
}


ostream &operator<<(ostream &os, const Date &date) {
  os << setfill('0') << setw(4) << date.year_ << "-" << setw(2) << date.month_ << "-" << setw(2) << date.day_;
  return os;
}
istream &operator>>(istream &is, Date &date) {
  string raw_date;
  is >> raw_date;
  stringstream ss(raw_date);

  int year, month, day;
  if (ss >> year && ss.peek() == '-' && ss.ignore(1) && ss >> month && ss.peek() == '-' && ss.ignore(1) && ss >> day
	  && ss.eof()) {
	date = Date(year, month, day);
  } else {
	throw domain_error("Wrong date format: " + raw_date);
  }
  return is;
}
bool Date::operator<(const Date &rhs) const {
  return tie(year_, month_, day_) < tie(rhs.year_, rhs.month_, rhs.day_);
}
string Date::GetData() const {
  stringstream os;
  os << setfill('0') << setw(4) << year_ << "-" << setw(2) << month_ << "-" << setw(2) << day_;
  string result;
  os >> result;
  return result;
}

Date ParseDate(istream &is) {
  string raw_date;
  is >> raw_date;
  stringstream ss(raw_date);
  Date date{};
  int year, month, day;
  if (ss >> year && ss.peek() == '-' && ss.ignore(1) && ss >> month && ss.peek() == '-' && ss.ignore(1) && ss >> day
	  && ss.eof()) {
	date = Date(year, month, day);
  } else {
	throw domain_error("Wrong date format: " + raw_date);
  }
  return date;
}
