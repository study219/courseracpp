// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#pragma once
#include "date.h"
#include <set>
#include <map>
#include <vector>
#include <functional>
#include <algorithm>

class Database {
 public:
  void Add(const Date &date, const string &event);
  void Print(ostream &os) const;
  string Last(const Date &date) const;
  int RemoveIf(const function<bool(const Date&, const string&)> &predicate);
  vector<pair<Date, string>> FindIf(const function<bool(const Date&, const string&)> &predicate) const;

 private:
  map<Date, vector<string>> db_;
  map<Date, set<string>> db_unique_;
};