// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "node.h"

bool EmptyNode::Evaluate(const Date& date, const string& event) const {
  return true;
}

template<typename T>
bool Comporator(const Comparison &cmp, const T &lhs, const T &rhs) {
  switch (cmp) {
	case Comparison::Less:
	  return lhs < rhs;
	case Comparison::LessOrEqual:
	  return lhs <= rhs;
	case Comparison::Greater:
	  return lhs > rhs;
	case Comparison::GreaterOrEqual:
	  return lhs >= rhs;
	case Comparison::Equal:
	  return lhs == rhs;
	case Comparison::NotEqual:
	  return lhs != rhs;
  }
  return false;
}

DateComparisonNode::DateComparisonNode(const Comparison &cmp, const Date &date) : cmp_(cmp), date_(date){}
bool DateComparisonNode::Evaluate(const Date &date, const string &event) const {
  return Comporator(cmp_,date,date_);
}

EventComparisonNode::EventComparisonNode(const Comparison &cmp, const string &event) : cmp_(cmp), event_(event){}
bool EventComparisonNode::Evaluate(const Date &date, const string &event) const {
  return Comporator(cmp_,event,event_);
}

LogicalOperationNode::LogicalOperationNode(const LogicalOperation &op,
										   const shared_ptr<Node> &left,
										   const shared_ptr<Node> &right) : op_(op), left_(left), right_(right){}
bool LogicalOperationNode::Evaluate(const Date &date, const string &event) const {
  switch (op_) {
	case LogicalOperation::And:
	  return left_->Evaluate(date, event) && right_->Evaluate(date, event);
	case LogicalOperation::Or:
	  return left_->Evaluate(date, event) || right_->Evaluate(date, event);
  }
  return false;
}
