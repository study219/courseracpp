// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <fstream>
#include <stdexcept>
#include <vector>

using namespace std;

// Реализуйте здесь
// * класс Matrix
// * оператор ввода для класса Matrix из потока istream
// * оператор вывода класса Matrix в поток ostream
// * оператор проверки на равенство двух объектов класса Matrix
// * оператор сложения двух объектов класса Matrix

class Matrix {
 public:
  Matrix() {
	num_rows_ = 0;
	num_cols_ = 0;
  }
  Matrix(int num_rows, int num_cols) {
	Reset(num_rows, num_cols);
  }
  void Reset(int num_rows, int num_cols) {
	if (num_rows < 0 || num_cols < 0) throw out_of_range("out_of_range");
	if (num_rows == 0 || num_cols == 0) {
	  num_rows = num_cols = 0;
	}

	num_rows_ = num_rows;
	num_cols_ = num_cols;
	data_.assign(num_rows, vector<int>(num_cols));
  }

  int &At(int row, int column) {
	return data_.at(row).at(column);
  }

  int At(int row, int column) const {
	return data_.at(row).at(column);
  }

  int GetNumRows() const {
	return num_rows_;
  }
  int GetNumColumns() const {
	return num_cols_;
  }
  friend ostream &operator<<(ostream &os, const Matrix &matrix) {
	os << matrix.num_rows_ << " " << matrix.num_cols_ << endl;
	for (int row = 0; row < matrix.GetNumRows(); ++row) {
	  for (int col = 0; col < matrix.GetNumColumns(); ++col) {
		if (col > 0) {
		  os << " ";
		}
		os << matrix.At(row, col);
	  }
	  os << endl;
	}
	return os;
  }

  friend istream &operator>>(istream &is, Matrix &matrix) {
	int rows, cols;
	is >> rows >> cols;
	matrix = Matrix(rows, cols);
	for (int row = 0; row < rows; ++row) {
	  for (int col = 0; col < cols; ++col) {
		is >> matrix.At(row, col);
	  }
	}
	return is;
  }
  bool operator==(const Matrix &rhs) const {
	if (num_rows_ != rhs.num_rows_) {
	  return false;
	}
	if (num_cols_ != rhs.num_cols_) {
	  return false;
	}

	for (int row = 0; row < rhs.GetNumRows(); ++row) {
	  for (int col = 0; col < rhs.GetNumColumns(); ++col) {
		if (this->At(row, col) != rhs.At(row, col)) {
		  return false;
		}
	  }
	}

	return true;
  }
  bool operator!=(const Matrix &rhs) const {
	return !(rhs == *this);
  }

 private:
  int num_rows_;
  int num_cols_;
  vector<vector<int>> data_;
};

Matrix operator+(const Matrix &lhs, const Matrix &rhs) {
  if (lhs.GetNumRows() != rhs.GetNumRows() && lhs.GetNumColumns() != rhs.GetNumColumns())
	throw invalid_argument("invalid_argument");

  Matrix result(lhs.GetNumRows(), lhs.GetNumColumns());
  for (int row = 0; row < result.GetNumRows(); ++row) {
	for (int col = 0; col < result.GetNumColumns(); ++col) {
	  result.At(row, col) = lhs.At(row, col) + rhs.At(row, col);
	}
  }
  return result;
}

int main() {
  Matrix one;
  Matrix two;

  cin >> one >> two;
  cout << (one == two) << endl;

  return 0;
}