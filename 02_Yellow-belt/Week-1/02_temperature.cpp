// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <vector>
#include <cstdint>
using namespace std;



int main() {
  int64_t days, temp_sum = 0;
  cin >> days;
  vector<int64_t> temperatures(days);
  for (auto& d: temperatures)
  {
	cin >> d;
	temp_sum += d;
  }
  int64_t mid_temp = temp_sum / days;
  vector<size_t> temp_days;
  for (size_t i = 0; i < temperatures.size(); ++i)
  {
	if (temperatures[i] > mid_temp)
	{
	  temp_days.push_back(i);
	}
  }

  cout << temp_days.size() << endl;
  for (auto& d: temp_days)
  {
	cout << d << " ";
  }

  return 0;
}