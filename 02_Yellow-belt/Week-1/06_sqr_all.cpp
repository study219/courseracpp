// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <map>
#include <vector>

using namespace std;

template<typename T> T Sqr(T v);

template<typename First, typename Second> auto Sqr(const pair<First, Second> &p);
template<typename T> auto Sqr(const vector<T> &v);
template<typename Key, typename Value> auto Sqr(const map<Key, Value> &m);

template<typename T>
T Sqr(T v) {
  return v * v;
}

template<typename First, typename Second>
auto Sqr(const pair<First, Second> &p) {
  return pair{Sqr(p.first), Sqr(p.second)};
}

template<typename T>
auto Sqr(const vector<T> &v) {
  vector<T> result = v;
  for (T &i: result) {
	i = Sqr(i);
  }
  return result;
}

template<typename Key, typename Value>
auto Sqr(const map<Key, Value> &m) {
  map<Key, Value> result = m;
  for (auto&[key, value]: result) {
	value = Sqr(value);
  }
  return result;
}

int main() {
  cout << Sqr(5);
  vector<int> v = {1, 2, 3};
  cout << "vector:";
  for (int x: Sqr(v)) {
	cout << ' ' << x;
  }
  cout << endl;

  map<int, pair<int, int>> map_of_pairs = {
	  {4, {2, 2}},
	  {7, {4, 3}}
  };
  cout << "map of pairs:" << endl;
  for (const auto &x: Sqr(map_of_pairs)) {
	cout << x.first << ' ' << x.second.first << ' ' << x.second.second << endl;
  }

  return 0;
}