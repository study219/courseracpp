// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <tuple>

using namespace std;

enum class Lang {
  DE, FR, IT
};

struct Region {

  string std_name;
  string parent_std_name;
  map<Lang, string> names;
  int64_t population;
};

auto GetTie(const Region &lhs){
  return tie(lhs.std_name, lhs.parent_std_name, lhs.names, lhs.population);
}

bool operator<(const Region &lhs, const Region &rhs) {
  return GetTie(lhs) < GetTie(rhs);
}

int FindMaxRepetitionCount(const vector<Region> &regions) {
  if (regions.empty()) {
	return 0;
  }

  int max_count = 1;
  map<Region, int> result;
  for (const auto &r: regions) {
	max_count = max(max_count, ++result[r]);
  }

  return max_count;
}

int main() {
  cout << FindMaxRepetitionCount({
									 {
										 "Moscow",
										 "Russia",
										 {{Lang::DE, "Moskau"}, {Lang::FR, "Moscou"}, {Lang::IT, "Mosca"}},
										 89
									 }, {
										 "Russia",
										 "Eurasia",
										 {{Lang::DE, "Russland"}, {Lang::FR, "Russie"}, {Lang::IT, "Russia"}},
										 89
									 }, {
										 "Moscow",
										 "Russia",
										 {{Lang::DE, "Moskau"}, {Lang::FR, "Moscou"}, {Lang::IT, "Mosca"}},
										 89
									 }, {
										 "Moscow",
										 "Russia",
										 {{Lang::DE, "Moskau"}, {Lang::FR, "Moscou"}, {Lang::IT, "Mosca"}},
										 89
									 }, {
										 "Russia",
										 "Eurasia",
										 {{Lang::DE, "Russland"}, {Lang::FR, "Russie"}, {Lang::IT, "Russia"}},
										 89
									 },
								 }) << endl;

  cout << FindMaxRepetitionCount({
									 {
										 "Moscow",
										 "Russia",
										 {{Lang::DE, "Moskau"}, {Lang::FR, "Moscou"}, {Lang::IT, "Mosca"}},
										 89
									 }, {
										 "Russia",
										 "Eurasia",
										 {{Lang::DE, "Russland"}, {Lang::FR, "Russie"}, {Lang::IT, "Russia"}},
										 89
									 }, {
										 "Moscow",
										 "Russia",
										 {{Lang::DE, "Moskau"}, {Lang::FR, "Moscou deux"}, {Lang::IT, "Mosca"}},
										 89
									 }, {"Moscow",
										 "Toulouse",
										 {{Lang::DE, "Moskau"}, {Lang::FR, "Moscou"}, {Lang::IT, "Mosca"}},
										 89
									 }, {
										 "Moscow",
										 "Russia",
										 {{Lang::DE, "Moskau"}, {Lang::FR, "Moscou"}, {Lang::IT, "Mosca"}},
										 31
									 },
								 }) << endl;
  return 0;
}