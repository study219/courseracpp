// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <map>
#include <string>
#include <tuple>
#include <vector>

using namespace std;

// Перечислимый тип для статуса задачи
enum class TaskStatus {
  NEW,          // новая
  IN_PROGRESS,  // в разработке
  TESTING,      // на тестировании
  DONE          // завершена
};

// Объявляем тип-синоним для map<TaskStatus, int>,
// позволяющего хранить количество задач каждого статуса
using TasksInfo = map<TaskStatus, int>;

class TeamTasks {
 public:
  // Получить статистику по статусам задач конкретного разработчика
  const TasksInfo &GetPersonTasksInfo(const string &person) const {
	return devs_.at(person);
  }

  // Добавить новую задачу (в статусе NEW) для конкретного разработчитка
  void AddNewTask(const string &person) {
	devs_[person][TaskStatus::NEW]++;
  }

  TaskStatus Next(TaskStatus status){
	return static_cast<TaskStatus>(static_cast<int>(status) + 1);
  }

  void NullZero(TasksInfo& task){
	vector<TaskStatus> remove;
	for (auto& [status, value] : task) {
	  if (value == 0){
		remove.push_back(status);
	  }
	}
	for (auto& status : remove) {
		task.erase(status);
	}

  }
  // Обновить статусы по данному количеству задач конкретного разработчика,
  // подробности см. ниже
  tuple<TasksInfo, TasksInfo> PerformPersonTasks(const string &person, int task_count) {
	if (!devs_.count(person)) {
	  return {};
	}
	TasksInfo updated_tasks, untouched_tasks;
	for (auto status = TaskStatus::NEW; status != TaskStatus::DONE && task_count; status = Next(status)) {
	  updated_tasks[Next(status)] = min(task_count, devs_[person][status]);
	  task_count -= updated_tasks[Next(status)];
	}

	for (auto status = TaskStatus::NEW; status != TaskStatus::DONE; status = Next(status)) {
	  untouched_tasks[status] = devs_[person][status] - updated_tasks[Next(status)];
	  devs_[person][status] += updated_tasks[status] - updated_tasks[Next(status)];
	}

	devs_[person][TaskStatus::DONE] += updated_tasks[TaskStatus::DONE];

	NullZero(updated_tasks);
	NullZero(untouched_tasks);
	NullZero(devs_[person]);

	return {updated_tasks, untouched_tasks};
  }
 private:
  map<string, TasksInfo> devs_;
};

void PrintTasksInfo(TasksInfo tasks_info) {
  cout << tasks_info[TaskStatus::NEW] << " new tasks" <<
	   ", " << tasks_info[TaskStatus::IN_PROGRESS] << " tasks in progress" <<
	   ", " << tasks_info[TaskStatus::TESTING] << " tasks are being tested" <<
	   ", " << tasks_info[TaskStatus::DONE] << " tasks are done" << endl;
}

int main() {

  TeamTasks tasks;
  for (int i = 0; i < 3; ++i) {
	tasks.AddNewTask("Ivan");
  }
  cout << "Ivan's tasks: ";
  PrintTasksInfo(tasks.GetPersonTasksInfo("Ivan"));

  TasksInfo updated_tasks, untouched_tasks;
  tie(updated_tasks, untouched_tasks) = tasks.PerformPersonTasks("Ivan", 2);
  cout << "Updated Ivan's tasks: ";
  PrintTasksInfo(updated_tasks);
  cout << "Ivan's tasks: ";
  PrintTasksInfo(tasks.GetPersonTasksInfo("Ivan"));
  cout << "Untouched Ivan's tasks: ";
  PrintTasksInfo(untouched_tasks);

  tie(updated_tasks, untouched_tasks) = tasks.PerformPersonTasks("Ivan", 2);
  cout << "Updated Ivan's tasks: ";
  PrintTasksInfo(updated_tasks);
  cout << "Ivan's tasks: ";
  PrintTasksInfo(tasks.GetPersonTasksInfo("Ivan"));
  cout << "Untouched Ivan's tasks: ";
  PrintTasksInfo(untouched_tasks);

  return 0;
}