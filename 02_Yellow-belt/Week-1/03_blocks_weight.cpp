// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <vector>

using namespace std;

class Block {
 public:
  Block() = default;
  Block(int w, int h, int d) {
	volume_ =  static_cast<uint64_t>(w) * h * d;
  }
  friend istream &operator>>(istream &is, Block &block) {
	int w, h, d;
	if(is >> w >> h >> d){
	  block = Block(w, h, d);
	}
	return is;
  }
  uint64_t GetVolume() const {
	return volume_;
  }
 private:
  uint64_t volume_;
};

int main() {
  int n, r;
  cin >> n >> r;
  uint64_t result_mass = 0;
  vector<Block> blocks(n);
  for (auto &block: blocks) {
	cin >> block;
	result_mass += block.GetVolume() * r;
  }
  cout << result_mass;

  return 0;
}