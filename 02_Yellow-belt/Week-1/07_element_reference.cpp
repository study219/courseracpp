// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <string>
#include <map>

using namespace std;

template<typename Key, typename Value>
Value &GetRefStrict(map<Key, Value>& map, Key key) {
  if (map.count(key) == 0) throw runtime_error("runtime_error");
  return map.at(key);
}


int main() {

  map<int, string> m = {{0, "value"}};
  string& item = GetRefStrict(m, 0);
  item = "newvalue";
  cout << m[0] << endl; // выведет newvalue

  return 0;
}
