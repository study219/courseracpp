cmake_minimum_required(VERSION 3.20)
project(Week6)

set(CMAKE_CXX_STANDARD 17)

add_executable(Week6 condition_parser.cpp condition_parser.h condition_parser_test.cpp database.cpp database.h database_test.cpp date.cpp date.h date_test.cpp event_set.cpp event_set.h main.cpp node.cpp node.h node_test.cpp test_runner.h token.cpp token.h) # condition_parser_test.cpp
