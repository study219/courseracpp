// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

using namespace std;

template <class T>
ostream& operator << (ostream& os, const vector<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
	if (!first) {
	  os << ", ";
	}
	first = false;
	os << x;
  }
  return os << "}";
}

template <class T>
ostream& operator << (ostream& os, const set<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
	if (!first) {
	  os << ", ";
	}
	first = false;
	os << x;
  }
  return os << "}";
}

template <class K, class V>
ostream& operator << (ostream& os, const map<K, V>& m) {
  os << "{";
  bool first = true;
  for (const auto& kv : m) {
	if (!first) {
	  os << ", ";
	}
	first = false;
	os << kv.first << ": " << kv.second;
  }
  return os << "}";
}

template<class T, class U>
void AssertEqual(const T& t, const U& u, const string& hint = {}) {
  if (t != u) {
	ostringstream os;
	os << "Assertion failed: " << t << " != " << u;
	if (!hint.empty()) {
	  os << " hint: " << hint;
	}
	throw runtime_error(os.str());
  }
}

void Assert(bool b, const string& hint) {
  AssertEqual(b, true, hint);
}

class TestRunner {
 public:
  template <class TestFunc>
  void RunTest(TestFunc func, const string& test_name) {
	try {
	  func();
	  cerr << test_name << " OK" << endl;
	} catch (exception& e) {
	  ++fail_count;
	  cerr << test_name << " fail: " << e.what() << endl;
	} catch (...) {
	  ++fail_count;
	  cerr << "Unknown exception caught" << endl;
	}
  }

  ~TestRunner() {
	if (fail_count > 0) {
	  cerr << fail_count << " unit tests failed. Terminate" << endl;
	  exit(1);
	}
  }

 private:
  int fail_count = 0;
};


class Person {
 public:
  void ChangeFirstName(int year, const string &first_name) {
	// добавить факт изменения имени на first_name в год year
	full_name_[year].first_name = first_name;
  }
  void ChangeLastName(int year, const string &last_name) {
	// добавить факт изменения фамилии на last_name в год year
	full_name_[year].last_name = last_name;
  }
  string GetFullName(int year) {
	// получить имя и фамилию по состоянию на конец года year
	if (full_name_.begin()->first > year) {
	  return "Incognito";
	} else {
	  string fn;
	  string ln;
	  for (const auto&[kYear, kFullname]: full_name_) {
		if (kYear <= year && !kFullname.first_name.empty()) {
		  fn = kFullname.first_name;
		}
		if (kYear <= year && !kFullname.last_name.empty()) {
		  ln = kFullname.last_name;
		}
	  }

	  if (fn.empty()) {
		return ln + " with unknown first name";
	  } else if (ln.empty()) {
		return fn + " with unknown last name";
	  } else {
		return fn + " " + ln;
	  }
	}

  }
 private:
  // приватные поля
  struct Names {
	string first_name;
	string last_name;
  };
  map<int, Names> full_name_;
};


void TestIncognito() {
  {
	Person person;
	person.ChangeFirstName(2000, "Ivan");
	AssertEqual(person.GetFullName(1991), "Incognito", "-1");
	AssertEqual(person.GetFullName(1), "Incognito", "-2");
  }

  {
	Person person;
	person.ChangeLastName(2000, "TestLastName");
	AssertEqual(person.GetFullName(1991), "Incognito", "-3");
	AssertEqual(person.GetFullName(1), "Incognito", "-4");
  }

  {
	Person person;
	person.ChangeFirstName(2000, "Ivan");
	person.ChangeLastName(2000, "TestLastName");
	AssertEqual(person.GetFullName(1991), "Incognito", "-3");
	AssertEqual(person.GetFullName(1999), "Incognito", "-3");
	AssertEqual(person.GetFullName(1), "Incognito", "-4");
  }
}

void TestUnknownFirstName() {
  {
	Person person;
	person.ChangeLastName(2000, "TestLastName2000");
	AssertEqual(person.GetFullName(2000), "TestLastName2000 with unknown first name", "-5");
	AssertEqual(person.GetFullName(2001), "TestLastName2000 with unknown first name", "-6");
	AssertEqual(person.GetFullName(1999), "Incognito", "-7");
  }

  {
	Person person;
	person.ChangeFirstName(2002, "TestFirstName2002");
	person.ChangeLastName(2000, "TestLastName2000");
	AssertEqual(person.GetFullName(2000), "TestLastName2000 with unknown first name", "-5");
	AssertEqual(person.GetFullName(2001), "TestLastName2000 with unknown first name", "-6");
	AssertEqual(person.GetFullName(1999), "Incognito", "-7");
  }
}

void TestUnknownLastName() {
  {
	Person person;
	person.ChangeFirstName(2000, "TestFirstName2000");
	AssertEqual(person.GetFullName(2000), "TestFirstName2000 with unknown last name", "-8");
	AssertEqual(person.GetFullName(2001), "TestFirstName2000 with unknown last name", "-9");
	AssertEqual(person.GetFullName(1999), "Incognito", "-10");
  }

  {
	Person person;
	person.ChangeFirstName(2000, "TestFirstName2000");
	person.ChangeLastName(2002, "TestLastName2002");
	AssertEqual(person.GetFullName(2000), "TestFirstName2000 with unknown last name", "-8");
	AssertEqual(person.GetFullName(2001), "TestFirstName2000 with unknown last name", "-9");
	AssertEqual(person.GetFullName(1999), "Incognito", "-10");
  }
}

void TestPerson() {
  {
	Person person;
	person.ChangeFirstName(2000, "TestFirstName2000");
	person.ChangeLastName(2002, "TestLastName2002");
	AssertEqual(person.GetFullName(2000), "TestFirstName2000 with unknown last name", "-8");
	AssertEqual(person.GetFullName(2001), "TestFirstName2000 with unknown last name", "-9");
	AssertEqual(person.GetFullName(1999), "Incognito", "-10");
	AssertEqual(person.GetFullName(2002), "TestFirstName2000 TestLastName2002", "-11");
	AssertEqual(person.GetFullName(2005), "TestFirstName2000 TestLastName2002", "-12");
	AssertEqual(person.GetFullName(3005), "TestFirstName2000 TestLastName2002", "-13");
	person.ChangeFirstName(1900, "TestFirstName1900");
	person.ChangeLastName(1900, "TestLastName1900");
	AssertEqual(person.GetFullName(1999), "TestFirstName1900 TestLastName1900", "-14");
	AssertEqual(person.GetFullName(2000), "TestFirstName2000 TestLastName1900", "-15");
	AssertEqual(person.GetFullName(2002), "TestFirstName2000 TestLastName2002", "-16");
  }
}

int main() {
  TestRunner runner;
  runner.RunTest(TestIncognito, "TestIncognito");
  runner.RunTest(TestUnknownFirstName, "TestUnknownFirstName");
  runner.RunTest(TestUnknownLastName, "TestUnknownLastName");
  runner.RunTest(TestPerson, "TestPerson");
  return 0;
}
