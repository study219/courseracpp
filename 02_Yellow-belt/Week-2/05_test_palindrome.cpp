#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

using namespace std;

template<class T>
ostream &operator<<(ostream &os, const vector<T> &s) {
  os << "{";
  bool first = true;
  for (const auto &x: s) {
	if (!first) {
	  os << ", ";
	}
	first = false;
	os << x;
  }
  return os << "}";
}

template<class T>
ostream &operator<<(ostream &os, const set<T> &s) {
  os << "{";
  bool first = true;
  for (const auto &x: s) {
	if (!first) {
	  os << ", ";
	}
	first = false;
	os << x;
  }
  return os << "}";
}

template<class K, class V>
ostream &operator<<(ostream &os, const map<K, V> &m) {
  os << "{";
  bool first = true;
  for (const auto &kv: m) {
	if (!first) {
	  os << ", ";
	}
	first = false;
	os << kv.first << ": " << kv.second;
  }
  return os << "}";
}

template<class T, class U>
void AssertEqual(const T &t, const U &u, const string &hint = {}) {
  if (t != u) {
	ostringstream os;
	os << "Assertion failed: " << t << " != " << u;
	if (!hint.empty()) {
	  os << " hint: " << hint;
	}
	throw runtime_error(os.str());
  }
}

void Assert(bool b, const string &hint) {
  AssertEqual(b, true, hint);
}

class TestRunner {
 public:
  template<class TestFunc>
  void RunTest(TestFunc func, const string &test_name) {
	try {
	  func();
	  cerr << test_name << " OK" << endl;
	} catch (exception &e) {
	  ++fail_count;
	  cerr << test_name << " fail: " << e.what() << endl;
	} catch (...) {
	  ++fail_count;
	  cerr << "Unknown exception caught" << endl;
	}
  }

  ~TestRunner() {
	if (fail_count > 0) {
	  cerr << fail_count << " unit tests failed. Terminate" << endl;
	  exit(1);
	}
  }

 private:
  int fail_count = 0;
};

bool IsPalindrom(const string &str) {
  if (str.size() <= 1) {
	return true;
  }
  for (int i = str.size() - 1, j = 0; j < str.size() / 2; --i, ++j) {
	if (str[i] != str[j]) {
	  return false;
	}
  }
  return true;
}

void TestAll() {
  Assert(IsPalindrom(""), "empty");
  Assert(IsPalindrom("a"), "1 symbol");
  Assert(IsPalindrom(" "), "1 symbol");
  Assert(IsPalindrom("madam"), "madam");
  Assert(IsPalindrom(" madam "), " madam ");
  Assert(IsPalindrom("level"), "level");
  Assert(IsPalindrom("  level  "), "  level  ");
  Assert(IsPalindrom("wasitacaroracatisaw"), "wasitacaroracatisaw");
  Assert(IsPalindrom("aa"), "aa");
  Assert(IsPalindrom("1"), "1");
  Assert(IsPalindrom("11"), "11");
  Assert(IsPalindrom("%%"), "%%");
  Assert(IsPalindrom("  "), "  ");
  Assert(IsPalindrom("bb"), "bb");
  Assert(!IsPalindrom("abc"), "abc");
  Assert(!IsPalindrom(" aba"), " aba");
  Assert(!IsPalindrom("aba "), "aba ");
  Assert(!IsPalindrom("abcd"), "abcd");
  Assert(!IsPalindrom("abcde"), "abcde");
  Assert(!IsPalindrom(" abcde"), " abcde");
}

int main() {
  TestRunner runner;
  runner.RunTest(TestAll, "TestAll");
  // добавьте сюда свои тесты
  return 0;
}
