// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <string>
#include <iostream>
#include <vector>
#include <map>

using namespace std;

enum class QueryType {
  NewBus,
  BusesForStop,
  StopsForBus,
  AllBuses
};

struct Query {
  QueryType type;
  string bus;
  string stop;
  vector<string> stops;
};

istream &operator>>(istream &is, Query &q) {
  string operation_code;
  is >> operation_code;
  if (operation_code == "NEW_BUS") {
	q.type = QueryType::NewBus;
	is >> q.bus;
	int stop_count;
	is >> stop_count;
	q.stops.resize(stop_count);
	for (string &stop: q.stops) {
	  is >> stop;
	}
  } else if (operation_code == "BUSES_FOR_STOP") {
	q.type = QueryType::BusesForStop;
	is >> q.stop;
  } else if (operation_code == "STOPS_FOR_BUS") {
	q.type = QueryType::StopsForBus;
	is >> q.bus;
  } else if (operation_code == "ALL_BUSES") {
	q.type = QueryType::AllBuses;
  }

  // Реализуйте эту функцию
  return is;
}

struct BusesForStopResponse {
  // Наполните полями эту структуру
  vector<string> bus_to_stops;
};

ostream &operator<<(ostream &os, const BusesForStopResponse &r) {
  // Реализуйте эту функцию
  if (r.bus_to_stops.empty()) {
	os << "No stop";
  } else {
	for (const string &bus: r.bus_to_stops) {
	  os << bus << " ";
	}
  }
  return os;
}

struct StopsForBusResponse {
  // Наполните полями эту структуру
  string name_bus;
  vector<string> buses_to_stops;
  map<string, vector<string>> stops_to_bus;
};

ostream &operator<<(ostream &os, const StopsForBusResponse &r) {
  // Реализуйте эту функцию
  if (r.buses_to_stops.empty()) {
	return os << "No bus";
  }

  for (const string &s: r.buses_to_stops) {
	os << "Stop " << s << ": ";
	if (r.stops_to_bus.at(s).size() == 1) {
	  os << "no interchange";
	  os << endl;
	} else {
	  for (const string &w: r.stops_to_bus.at(s)) {
		if (w != r.name_bus) {
		  os << w << " ";
		}
	  }
	  os << endl;
	}
  }
  return os;
}

struct AllBusesResponse {
  // Наполните полями эту структуру
  map<string, vector<string>> buses_to_stops;
};

ostream &operator<<(ostream &os, const AllBusesResponse &r) {
  // Реализуйте эту функцию
  if (r.buses_to_stops.empty()) {
	return os << "No buses";
  }

  for (const auto &item: r.buses_to_stops) {
	os << "Bus " << item.first << ": ";
	for (const string &s: item.second) {
	  os << s << " ";
	}
	os << endl;
  }
  return os;
}

class BusManager {
 public:
  void AddBus(const string &bus, const vector<string> &stops) {
	// Реализуйте этот метод
	buses_to_stops_[bus] = stops;
	for (const auto &s: stops) {
	  stops_to_buses_[s].push_back(bus);
	}
  }

  BusesForStopResponse GetBusesForStop(const string &stop) const {
	// Реализуйте этот метод
	if (stops_to_buses_.count(stop) == 0) {
	  return {};
	}

	return {stops_to_buses_.at(stop)};
  }

  StopsForBusResponse GetStopsForBus(const string &bus) const {
	// Реализуйте этот метод
	if (buses_to_stops_.count(bus) == 0) {
	  return {};
	}
	return {bus, buses_to_stops_.at(bus), stops_to_buses_};
  }

  AllBusesResponse GetAllBuses() const {
	// Реализуйте этот метод
	if (buses_to_stops_.empty()) {
	  return {};
	}
	return {buses_to_stops_};
  }
 private:
  map<string, vector<string>> buses_to_stops_, stops_to_buses_;
};

// Не меняя тела функции main, реализуйте функции и классы выше

int main() {
  int query_count;
  Query q;

  cin >> query_count;

  BusManager bm;
  for (int i = 0; i < query_count; ++i) {
	cin >> q;
	switch (q.type) {
	  case QueryType::NewBus: bm.AddBus(q.bus, q.stops);
		break;
	  case QueryType::BusesForStop: cout << bm.GetBusesForStop(q.stop) << endl;
		break;
	  case QueryType::StopsForBus: cout << bm.GetStopsForBus(q.bus) << endl;
		break;
	  case QueryType::AllBuses: cout << bm.GetAllBuses() << endl;
		break;
	}
  }

  return 0;
}
