// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#include "01_sum_reverse_sort.h"
#include <algorithm>

int Sum(int x, int y){
  return x+y;
}
string Reverse(string s){
  reverse(begin(s), end(s));
  return s;
}
void Sort(vector<int>& nums){
  sort(nums.begin(),  nums.end());
}