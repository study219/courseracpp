#include "responses.h"

ostream &operator<<(ostream &os, const BusesForStopResponse &r) {
  // Реализуйте эту функцию
  if (r.bus_to_stops.empty()) {
	os << "No stop";
  } else {
	for (const string &bus: r.bus_to_stops) {
	  os << bus << " ";
	}
  }
  return os;
}

ostream &operator<<(ostream &os, const StopsForBusResponse &r) {
  // Реализуйте эту функцию
  if (r.buses_to_stops.empty()) {
	return os << "No bus";
  }

  for (const string &s: r.buses_to_stops) {
	os << "Stop " << s << ": ";
	if (r.stops_to_bus.at(s).size() == 1) {
	  os << "no interchange";
	  os << endl;
	} else {
	  for (const string &w: r.stops_to_bus.at(s)) {
		if (w != r.name_bus) {
		  os << w << " ";
		}
	  }
	  os << endl;
	}
  }
  return os;
}

ostream &operator<<(ostream &os, const AllBusesResponse &r) {
  // Реализуйте эту функцию
  if (r.buses_to_stops.empty()) {
	return os << "No buses";
  }

  for (const auto &item: r.buses_to_stops) {
	os << "Bus " << item.first << ": ";
	for (const string &s: item.second) {
	  os << s << " ";
	}
	os << endl;
  }
  return os;
}