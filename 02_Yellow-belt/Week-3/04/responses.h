#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <map>

using namespace std;

struct BusesForStopResponse {
  // Наполните полями эту структуру
  vector<string> bus_to_stops;
};

struct StopsForBusResponse {
  // Наполните полями эту структуру
  string name_bus;
  vector<string> buses_to_stops;
  map<string, vector<string>> stops_to_bus;
};

struct AllBusesResponse {
  // Наполните полями эту структуру
  map<string, vector<string>> buses_to_stops;
};

ostream &operator<<(ostream &os, const BusesForStopResponse &r);
ostream &operator<<(ostream &os, const StopsForBusResponse &r);
ostream &operator<<(ostream &os, const AllBusesResponse &r);