#include "bus_manager.h"

void BusManager::AddBus(const string &bus, const vector<string> &stops) {
  // Реализуйте этот метод
  buses_to_stops_[bus] = stops;
  for (const auto &s: stops) {
	stops_to_buses_[s].push_back(bus);
  }
}

BusesForStopResponse BusManager::GetBusesForStop(const string &stop) const {
  // Реализуйте этот метод
  if (stops_to_buses_.count(stop) == 0) {
	return {};
  }

  return {stops_to_buses_.at(stop)};
}
StopsForBusResponse BusManager::GetStopsForBus(const string &bus) const {
  // Реализуйте этот метод
  if (buses_to_stops_.count(bus) == 0) {
	return {};
  }
  return {bus, buses_to_stops_.at(bus), stops_to_buses_};
}
AllBusesResponse BusManager::GetAllBuses() const {
  // Реализуйте этот метод
  if (buses_to_stops_.empty()) {
	return {};
  }
  return {buses_to_stops_};
}
