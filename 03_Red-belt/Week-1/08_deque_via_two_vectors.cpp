// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <vector>

using namespace std;

template<class T>
class Deque {
 public:
  Deque() = default;
  bool Empty() const {
	return front_.empty() && back_.empty();
  }
  size_t Size() const {
	return front_.size() + back_.size();
  }


  const T& operator [] (size_t i) const
  {
	return i < front_.size() ? front_[front_.size() - i - 1] : back_[i - front_.size()];
  }

  T& operator [] (size_t i)
  {
	return i < front_.size() ? front_[front_.size() - i - 1] : back_[i - front_.size()];
  }

  const T& At(size_t i) const
  {
	CheckRange(i);
	return (*this)[i];
  }

  T& At(size_t i)
  {
	CheckRange(i);
	return (*this)[i];
  }

  void PushFront(T value){
	front_.push_back(value);
  }

  void PushBack(T value){
	back_.push_back(value);
  }

  T& Front(){
	return front_.empty() ? back_.front() : front_.back();
  }

  T& Back(){
	return back_.empty() ? front_.front() : back_.back();
  }

  const T& Front() const{
	return front_.empty() ? back_.front() : front_.back();
  }

  const T& Back() const{
	return back_.empty() ? front_.front() : back_.back();
  }

 private:
  void CheckRange(size_t& i) const {
	if (i >= Size()) {
	  throw out_of_range("Index is out of range");
	}
  }
  vector<T> front_;
  vector<T> back_;
};

int main() {

  return 0;
}