// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "test_runner.h"
#include <vector>

using namespace std;

// Реализуйте здесь шаблонный класс Table
template<class T>
class Table{
 public:
  Table(const size_t& row, const size_t& colums){
	Resize(row, colums);
  }

  void Resize(const size_t& row, const size_t& colums) {
	row_ = row;
	colums_ = colums;
	if (row == 0 || colums == 0){
	  row_ = colums_ = 0;
	}
	data_.resize(row_);
	for (auto& d: data_) {
		d.resize(colums_);
	}
  }

  pair<size_t, size_t> Size() const {
	return {row_, colums_};
  }

  vector<T> &operator[](const size_t& index){
	return data_[index];
  }

  const vector<T> &operator[](const size_t& index ) const{
	return data_[index];
  }

  size_t row_;
  size_t colums_;
  vector<vector<T>> data_;
};

void TestTable() {
  Table<int> t(1, 1);
  ASSERT_EQUAL(t.Size().first, 1u);
  ASSERT_EQUAL(t.Size().second, 1u);
  t[0][0] = 42;
  ASSERT_EQUAL(t[0][0], 42);
  t.Resize(3, 4);
  ASSERT_EQUAL(t.Size().first, 3u);
  ASSERT_EQUAL(t.Size().second, 4u);
}

int main() {
  TestRunner tr;
  RUN_TEST(tr, TestTable);
  return 0;
}
