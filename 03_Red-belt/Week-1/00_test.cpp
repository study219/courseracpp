// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;


template <typename It>
struct IteratorRange {
  It first, last;

  IteratorRange(It begin, It end)
	  : first(begin)
	  , last(end)
  {
  }
};

template<typename T>
auto MakeRange(T& container) {
  return IteratorRange(container.begin(), container.end());
}


int main() {
  string s = "Hello, world!";
  auto rng = MakeRange(s);


  return 0;
}