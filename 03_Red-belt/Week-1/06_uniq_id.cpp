// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <string>
#include <vector>
using namespace std;

#define MACRO_1(temp) _var_##temp
#define MACRO(temp) MACRO_1(temp)
#define UNIQ_ID MACRO(__LINE__)


int main() {
  int UNIQ_ID = 0;
  string UNIQ_ID = "hello";
  vector<string> UNIQ_ID = {"hello", "world"};
  vector<int> UNIQ_ID = {1, 2, 3, 4};
}