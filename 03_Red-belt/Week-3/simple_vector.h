#pragma once

#include <cstdlib>
#include <cstring>

// Реализуйте шаблон SimpleVector
template <typename T>
class SimpleVector {
 public:
  SimpleVector() : data_(nullptr), size_(0), capacity_(0) {}
  explicit SimpleVector(size_t size) : data_(new T[size]),size_(size), capacity_(size) {}

  ~SimpleVector(){
	delete[] data_;
  }

  T& operator[](size_t index){
	return data_[index];
  }

  T* begin(){
	return data_;
  }
  T* end(){
	return data_+size_;
  }

  const T* begin() const{
	return data_;
  }
  const T* end() const{
	return data_+size_;
  }

  size_t Size() const{
	return size_;
  }
  size_t Capacity() const{
	return capacity_;
  }
  void PushBack(const T& value){
	if (size_ == 0){
	  data_ = new T[1];
	  size_ = capacity_ = 1;
	  data_[0] = value;
	} else {
	  if (size_ == capacity_){
		capacity_*=2;
		T* tmp = new T[capacity_];
		memmove(tmp, data_, sizeof(T) * size_);
		delete [] data_;
		data_ = tmp;
	  }
	  data_[size_++] = value;
	}
  }

 private:
  // Добавьте поля для хранения данных вектора
  T* data_;
  size_t size_;
  size_t capacity_;
};