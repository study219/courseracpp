// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "test_runner.h"

#include <algorithm>
#include <iostream>
#include <string>
#include <queue>
#include <stdexcept>
#include <set>
using namespace std;

template<class T>
class ObjectPool {
 public:
  ObjectPool() : allocated_(), deallocated_() {}
  T *Allocate() {
	if (deallocated_.empty()) {
	  auto alloc = new T;
	  allocated_.insert(alloc);
	  return alloc;
	} else {
	  auto alloc = deallocated_.front();
	  allocated_.insert(alloc);
	  deallocated_.pop();
	  return alloc;
	}
  }
  T *TryAllocate() {
	if (deallocated_.empty()) {
	  return nullptr;
	} else {
	  auto alloc = deallocated_.front();
	  allocated_.insert(alloc);
	  deallocated_.pop();
	  return alloc;
	}
  }

  void Deallocate(T *object) {
	if (allocated_.find(object) == allocated_.end()) {
	  throw invalid_argument("invalid_argument");
	}
	allocated_.erase(object);
	deallocated_.push(object);
  }

  ~ObjectPool() {
	for (auto &v: allocated_) {
	  delete v;
	}

	while (!deallocated_.empty()) {
	  delete deallocated_.front();
	  deallocated_.pop();
	}

  }

 private:
  // Добавьте сюда поля
  set<T *> allocated_;
  queue<T *> deallocated_;
};

void TestObjectPool() {
  ObjectPool<string> pool;

  auto p1 = pool.Allocate();
  auto p2 = pool.Allocate();
  auto p3 = pool.Allocate();

  *p1 = "first";
  *p2 = "second";
  *p3 = "third";

  pool.Deallocate(p2);
  ASSERT_EQUAL(*pool.Allocate(), "second");

  pool.Deallocate(p3);
  pool.Deallocate(p1);
  ASSERT_EQUAL(*pool.Allocate(), "third");
  ASSERT_EQUAL(*pool.Allocate(), "first");

  pool.Deallocate(p1);
}

int main() {
  TestRunner tr;
  RUN_TEST(tr, TestObjectPool);
  return 0;
}
