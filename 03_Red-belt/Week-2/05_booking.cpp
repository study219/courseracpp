// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <string>
#include <cstdint>
#include <map>
#include <queue>

using namespace std;

struct Clients {
  int64_t time;
  string hotel_name;
  int client_id;
  int room_count;
};

class HotelBooking {
 public:
  HotelBooking() : hotels_(), hotel_room_booked_(), clients_count_() {}
  void Add(const int64_t &time, const string &hotel_name, const int &client_id, const int &room_count) {
	while (!hotels_.empty() && hotels_.front().time <= time - 86400) //
	{
	  auto front_it = hotels_.front();
	  --clients_count_[front_it.hotel_name][front_it.client_id];
	  if (clients_count_[front_it.hotel_name][front_it.client_id] == 0) {
		clients_count_[front_it.hotel_name].erase(front_it.client_id);
	  }
	  hotel_room_booked_[front_it.hotel_name] -= front_it.room_count;
	  hotels_.pop();
	}

	++clients_count_[hotel_name][client_id];
	hotels_.push({time, hotel_name, client_id, room_count});
	hotel_room_booked_[hotel_name] += room_count;
  }
  size_t Client(const string &hotel_name)  {
	return clients_count_[hotel_name].size();
  }
  int Rooms(const string &hotel_name)  {
	return hotel_room_booked_[hotel_name];
  }
 private:
  queue<Clients> hotels_;
  map<string, int> hotel_room_booked_;
  map<string, map<int, int>> clients_count_;
};

int main() {
  ios::sync_with_stdio(false);
  cin.tie(nullptr);

  HotelBooking hb;

  int q;
  cin >> q;

  while (q) {
	string command;
	cin >> command;
	if (command == "BOOK") {
	  int64_t time;
	  int client_id, room_count;
	  string hotel_name;
	  cin >> time;
	  cin >> hotel_name;
	  cin >> client_id >> room_count;
	  hb.Add(time, hotel_name, client_id, room_count);
	} else {
	  string hotel_name;
	  cin >> hotel_name;
	  if (command == "CLIENTS") {
		cout << hb.Client(hotel_name) << endl;
	  } else if (command == "ROOMS") {
		cout << hb.Rooms(hotel_name) << endl;
	  }
	}
	--q;
  }

  return 0;
}