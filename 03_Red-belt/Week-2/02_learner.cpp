// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <set>

using namespace std;

class Learner {
 private:
  set<string> dict_;

 public:
  int Learn(const vector<string>& words) {
	int new_words = 0;
	for (const auto& word : words) {
	  if (dict_.insert(word).second) {
		++new_words;
	  }
	}
	return new_words;
  }

  vector<string> KnownWords() {
	return {dict_.begin(), dict_.end()};
  }
};
/*
int main() {
  Learner learner;
  string line;
  while (getline(cin, line)) {
	vector<string> words;
	stringstream ss(line);
	string word;
	while (ss >> word) {
	  words.push_back(word);
	}
	cout << learner.Learn(words) << "\n";
  }
  cout << "=== known words ===\n";
  for (auto word : learner.KnownWords()) {
	cout << word << "\n";
  }
}*/