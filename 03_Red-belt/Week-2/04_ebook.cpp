// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iomanip>
#include <iostream>
#include <vector>
#include <utility>
#include <numeric>

using namespace std;

class ReadingManager {
 public:
  ReadingManager()
	  : user_to_readed_page_(max_user_count_ + 1, 0),
		page_to_readed_users_count_(max_page_count_ + 1, 0),
		users_count_(0) {}

  void Read(const int &user_id, const int &page_count) {
	if (user_to_readed_page_[user_id] == 0) {
	  ++users_count_;
	} else {
	  --page_to_readed_users_count_[user_to_readed_page_[user_id]];
	}
	user_to_readed_page_[user_id] = page_count;
	++page_to_readed_users_count_[page_count];
  }

  double Cheer(const int &user_id) const {
	if (user_to_readed_page_[user_id] == 0) {
	  return 0;
	}

	if (users_count_ == 1) {
	  return 1.0;
	}
	const auto &user_page = user_to_readed_page_.at(user_id);
	int readed_users =
		accumulate(page_to_readed_users_count_.begin(), page_to_readed_users_count_.begin() + user_page, 0);

	return (readed_users * 1.0 / static_cast<double >(users_count_ - 1));
  }

 private:
  static const int max_user_count_ = 100'000;
  static const int max_page_count_ = 1'000;

  vector<int> user_to_readed_page_;
  vector<int> page_to_readed_users_count_;
  size_t users_count_;
};

int main() {
  // Для ускорения чтения данных отключается синхронизация
  // cin и cout с stdio,
  // а также выполняется отвязка cin от cout
  ios::sync_with_stdio(false);
  cin.tie(nullptr);

  ReadingManager manager;

  int query_count;
  cin >> query_count;

  for (int query_id = 0; query_id < query_count; ++query_id) {
	string query_type;
	cin >> query_type;
	int user_id;
	cin >> user_id;

	if (query_type == "READ") {
	  int page_count;
	  cin >> page_count;
	  manager.Read(user_id, page_count);
	} else if (query_type == "CHEER") {
	  cout << setprecision(6) << manager.Cheer(user_id) << "\n";
	}
  }

  return 0;
}