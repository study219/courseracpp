// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <algorithm>
#include <vector>
#include <set>

using namespace std;

class Student{
 public:
  Student(const string &name, int score_math, int score_info, int score_rus, int score_eng)
	  : name_(name),
		score_math_(score_math),
		score_info_(score_info),
		score_rus_(score_rus),
		score_eng_(score_eng),
		score_all_(score_math+score_info+score_rus+score_eng) {}
  string name_;

  friend ostream &operator<<(ostream &os, const Student &student) {
	os << "name_: " << student.name_ << " score_math_: " << student.score_math_ << " score_info_: "
	   << student.score_info_ << " score_all_: " << student.score_all_;
	return os;
  }
  int score_math_;
  int score_info_;
  int score_rus_;
  int score_eng_;
  int score_all_;
};

bool Comparator(const Student& lhs,  const Student& rhs){
  if (lhs.score_all_ == rhs.score_all_) {
	return (lhs.score_math_ + lhs.score_info_) > (rhs.score_math_ + rhs.score_info_);
  }
  return lhs.score_all_ > rhs.score_all_;
}


int main() {

  vector<Student> students{{"a", 25,25,25,25},
						   {"b", 13,14,25,25}, // равны
						   {"c", 13,15,24,25}, // равны
						   {"d", 25,25,13,14}, // равны
						   {"e", 14,15,25,25},};

  for (auto& s : students) {
	cout << s << endl;
  }

  cout << endl;
  //sort(students.begin(),  students.end(), Comparator);

  std::partial_sort(students.begin(), students.begin() + 3, students.end(),Comparator);
  for (auto& s : students) {
	cout << s << endl;
  }

  return 0;
}