// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include<iostream>
#include<list>
#include<algorithm>
#include<vector>

using namespace std;

int main() {

  list<int> athlets;
  //вектор для хранения итераторов вставки в лист
  vector<list<int>::iterator> it_pos(100'000 + 1, athlets.end());

  int q;
  cin >> q;
  while (q--) {
	int number, before_number;
	cin >> number >> before_number;
	it_pos[number] = athlets.insert(it_pos[before_number], number);
  }

  for (const int &a: athlets) {
	cout << a << endl;
  }

  return 0;
}