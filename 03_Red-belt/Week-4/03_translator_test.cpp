#include "test_runner.h"
#include <string>
#include <vector>
#include <string_view>
#include <list>
#include <iterator>

using namespace std;

class Words{
 public:
  Words(){}
  Words(string s, string t):source(s), target(t) {}
  string source;
  string target;
};
class Translator {
 public:
  void Add(string_view source, string_view target) {
	list<Words>::iterator id;
	if (!source_map.count(source)) {
	  words.push_front({ string{ source }, string{ target } });
	  Changes(words.begin());
	}
	else {
	  id = id_map.at(source);
	  id->source = source;
	  id->target = target;
	  Changes(id);
	}
  }
  string_view TranslateForward(string_view source) const {
	if (source_map.count(source)) {
	  return source_map.at(source);
	}
	else { return ""; }
  }
  string_view TranslateBackward(string_view target) const {
	if (target_map.count(target)) {
	  return target_map.at(target);
	}
	else { return ""; }
  }

 private:
  void Changes(list<Words>::iterator id) {
	id_map[id->source] = id;
	source_map[id->source] = id->target;
	target_map[id->target] = id->source;
  }
  list<Words> words;
  map< string_view, string_view> source_map;
  map< string_view, string_view> target_map;
  map< string_view, list<Words>::iterator> id_map;

};

void TestSimple() {
  Translator translator;
  translator.Add(string("a"), string("b"));
  translator.Add(string("c"), string("a"));
  translator.Add(string("c"), string("b"));

  ASSERT_EQUAL(translator.TranslateForward("okno"), "window");
  ASSERT_EQUAL(translator.TranslateBackward("table"), "stol");
  ASSERT_EQUAL(translator.TranslateBackward("stol"), "");
}

int main() {
  TestRunner tr;

  RUN_TEST(tr, TestSimple);


  return 0;
}
