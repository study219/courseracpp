// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <array>
#include <string_view>

using namespace std;

string ConcatenateStringViews(
	string_view lhs, string_view rhs) {
  string s;
  s.reserve(lhs.size() + rhs.size());
  return (s += lhs) += rhs;
}

int main() {

  array<char, 3> a = {'a', 'b', 'c'};

  cout << string_view(&a[0],a.size()) << endl;


  return 0;
}