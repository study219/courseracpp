// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#pragma once

#include <stdexcept>
#include <array>

using namespace std;

template <typename T, size_t N>
class StackVector {
 public:
  explicit StackVector(size_t a_size = 0){
	if(a_size > N) throw invalid_argument("invalid_argument");
	size_ = a_size;
	capacity_ = N;
  }

  T& operator[](size_t index){
	return const_cast<T&>(*this[index]);
  }
  const T& operator[](size_t index) const{
	return data_[index];
  }

  auto begin(){
	return data_.begin();
  }
  auto end(){
	return data_.begin()+size_;
  }
  auto begin() const{
	return data_.begin();
  }
  auto end() const{
	return data_.begin()+size_;
  }

  size_t Size() const{
	return size_;
  }
  size_t Capacity() const{
	return capacity_;
  }

  void PushBack(const T& value){
	if (size_ == capacity_) throw overflow_error("overflow_error");
	data_[size_++] = value;

  }
  T PopBack(){
	if (size_ == 0) throw underflow_error("underflow_error");
	return data_[--size_];
  }

 private:
  array<T, N> data_;
  size_t  size_;
  size_t  capacity_;
};
