#include "stats.h"

HttpRequest ParseRequest(string_view line) {
  size_t fist_not_space = line.find_first_not_of(' ');
  if (fist_not_space){
	line.remove_prefix(fist_not_space);
  }

  HttpRequest result;
  for (auto *sv: {&result.method, &result.uri, &result.protocol}) {
	size_t space = line.find(' ');
	*sv = line.substr(0, space);
	if (space == std::string_view::npos) {
	  break;
	} else {
	  line.remove_prefix(space + 1);
	}
  }
  return result;
}
void Stats::AddMethod(string_view method) {
  if (method == "GET" || method == "POST" || method == "PUT" || method == "DELETE") {
	method_[method]++;
  } else {
	method_["UNKNOWN"]++;
  }
}
void Stats::AddUri(string_view uri) {
  if (uri == "/" || uri == "/order" || uri == "/product" || uri == "/basket" || uri == "/help") {
	uri_[uri]++;
  } else {
	uri_["unknown"]++;
  }
}
const map<string_view, int> &Stats::GetMethodStats() const {
  return method_;
}
const map<string_view, int> &Stats::GetUriStats() const {
  return uri_;
}

Stats::Stats() : method_(), uri_() {
  method_["GET"] = 0;
  method_["POST"] = 0;
  method_["PUT"] = 0;
  method_["DELETE"] = 0;
  method_["UNKNOWN"] = 0;

  uri_["unknown"] = 0;
  uri_["/"] = 0;
  uri_["/order"] = 0;
  uri_["/product"] = 0;
  uri_["/basket"] = 0;
  uri_["/help"] = 0;
}
