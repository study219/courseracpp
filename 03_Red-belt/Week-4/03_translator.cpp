// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "test_runner.h"
#include <string>
#include <list>

using namespace std;

class Translator {
 public:
  Translator()
	  : data_(), trans_forward_(), trans_backward_() {
  }
  void Add(string_view source, string_view target) {
	const string_view kS = GetUniqueView(source);
	const string_view kT = GetUniqueView(target);
	trans_forward_[kS] = kT;
	trans_backward_[kT] = kS;
  }
  string_view TranslateForward(string_view source) const {
	return Translate(trans_forward_, source);
  }
  string_view TranslateBackward(string_view target) const {
	return Translate(trans_backward_, target);
  }

 private:
  // ???
  list<string> data_;
  map<string_view, string_view> trans_forward_;
  map<string_view, string_view> trans_backward_;

  static string_view Translate(const map<string_view, string_view> &trans_map, string_view sv) {
	const auto kIt = trans_map.find(sv);
	if (kIt != trans_map.end()) {
	  return kIt->second;
	}
	return {};
  }

  string_view GetUniqueView(string_view sv) {
	for (const auto* trans_map: {&trans_forward_, &trans_backward_}) { // интересненько
	  const auto kIt = trans_map->find(sv);
	  if (kIt != trans_map->end()) {
		return kIt->first;
	  }
	}
	return data_.emplace_front(sv);
  }

};

void TestSimple() {
  Translator translator;
  translator.Add(string("a"), string("b"));
  translator.Add(string("c"), string("a"));
  translator.Add(string("c"), string("b"));

  translator.Add(string("okno"), string("window"));
  translator.Add(string("stol"), string("table"));

  ASSERT_EQUAL(translator.TranslateForward("okno"), "window");
  ASSERT_EQUAL(translator.TranslateBackward("table"), "stol");
  ASSERT_EQUAL(translator.TranslateBackward("stol"), "");
}

int main() {
  TestRunner tr;
  RUN_TEST(tr, TestSimple);
  return 0;
}