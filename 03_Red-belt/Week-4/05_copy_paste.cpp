// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <string>
#include <list>
#include "test_runner.h"
using namespace std;

class Editor {
 public:
  // Реализуйте конструктор по умолчанию и объявленные методы
  Editor() : data_(), pos_(data_.begin()), buffer_() {}
  void Left() {
	if (pos_ != data_.begin()) {
	  pos_--;
	}
  }
  void Right() {
	if (pos_ != data_.end()) {
	  pos_++;
	}
  }
  void Insert(char token) {
	data_.insert(pos_, token);
  }
  void Cut(size_t tokens = 1) {
	size_t dis = distance(pos_, data_.end());
	auto end_pos = pos_;
	advance(end_pos, min(dis, tokens));

	buffer_.assign(pos_, end_pos);
	pos_ = data_.erase(pos_, end_pos);
  }
  void Copy(size_t tokens = 1) {
	buffer_.clear();
	size_t dis = distance(pos_, data_.end());
	auto end_pos = pos_;
	advance(end_pos, min(dis, tokens));
	buffer_.assign(pos_, end_pos);
  }
  void Paste() {
	data_.insert(pos_, buffer_.begin(), buffer_.end());
  }
  string GetText() const {
	return {data_.begin(), data_.end()};
  }

 private:
  list<char> data_;
  list<char>::iterator pos_;
  list<char> buffer_;
};

void TypeText(Editor &editor, const string &text) {
  for (char c: text) {
	editor.Insert(c);
  }
}

void TestEditing() {
  {
	Editor editor;

	const size_t text_len = 12;
	const size_t first_part_len = 7;
	TypeText(editor, "hello, world");
	for (size_t i = 0; i < text_len; ++i) {
	  editor.Left();
	}
	editor.Cut(first_part_len);
	for (size_t i = 0; i < text_len - first_part_len; ++i) {
	  editor.Right();
	}
	TypeText(editor, ", ");
	editor.Paste();
	editor.Left();
	editor.Left();
	editor.Cut(3);

	ASSERT_EQUAL(editor.GetText(), "world, hello");
  }
  {
	Editor editor;

	TypeText(editor, "misprnit");
	editor.Left();
	editor.Left();
	editor.Left();
	editor.Cut(1);
	editor.Right();
	editor.Paste();

	ASSERT_EQUAL(editor.GetText(), "misprint");
  }
}

void TestReverse() {
  Editor editor;

  const string text = "esreveR";
  for (char c: text) {
	editor.Insert(c);
	editor.Left();
  }

  ASSERT_EQUAL(editor.GetText(), "Reverse");
}

void TestNoText() {
  Editor editor;
  ASSERT_EQUAL(editor.GetText(), "");

  editor.Left();
  editor.Left();
  editor.Right();
  editor.Right();
  editor.Copy(0);
  editor.Cut(0);
  editor.Paste();

  ASSERT_EQUAL(editor.GetText(), "");
}

void TestEmptyBuffer() {
  Editor editor;

  editor.Paste();
  TypeText(editor, "example");
  editor.Left();
  editor.Left();
  editor.Paste();
  editor.Right();
  editor.Paste();
  editor.Copy(0);
  editor.Paste();
  editor.Left();
  editor.Cut(0);
  editor.Paste();

  ASSERT_EQUAL(editor.GetText(), "example");
}

int main() {
  TestRunner tr;
  RUN_TEST(tr, TestEditing);
  RUN_TEST(tr, TestReverse);
  RUN_TEST(tr, TestNoText);
  RUN_TEST(tr, TestEmptyBuffer);
  return 0;
}