// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "test_runner.h"
#include "profile.h"

#include <map>
#include <string>
#include <string_view>
#include <vector>
#include <future>
using namespace std;

template<typename Iterator>
class Page {
 public:
  Page(const Iterator &f, const Iterator &l) : begin_(f), end_(l), size_(l - f) {}
  Iterator begin() const {
	return begin_;
  }
  Iterator end() const {
	return end_;
  }
  size_t size() const {
	return size_;
  }
 private:
  Iterator begin_;
  Iterator end_;
  size_t size_;
};

template<typename Iterator>
class Paginator {
 public:
  Paginator(Iterator begin, Iterator end, size_t page_size) {

	size_t item_count = distance(begin, end);
	while (item_count) {
	  auto page_end = min(page_size, item_count);
	  pages_.push_back({begin, begin + page_end});

	  item_count -= page_end;
	  begin = next(begin, page_end);
	}

  }
  auto begin() const {
	return pages_.begin();
  }
  auto end() const {
	return pages_.end();
  }
  auto size() const {
	return pages_.size();
  }
 private:
  vector<Page<Iterator>> pages_;
};

template<typename C>
auto Paginate(C &c, size_t page_size) {
  return Paginator{c.begin(), c.end(), page_size};
}

struct Stats {
  map<string, int> word_frequences;

  void operator+=(const Stats &other) {
	for (auto&[key, value]: other.word_frequences) {
	  word_frequences[key] += value;
	}
  }
};

Stats CountWords(vector<string> lines, const set<string> &key_words) {
  Stats stat;

  for (auto &str: lines) {
	string_view line(str);

	size_t fist_not_space = line.find_first_not_of(' ');
	if (fist_not_space) {
	  line.remove_prefix(fist_not_space);
	}

	size_t space = 0;
	while (space != std::string_view::npos) {
	  space = line.find(' ');
	  string key(line.substr(0, space));
	  if (key_words.count(key) > 0){
		stat.word_frequences[key]++;
	  }
	  line.remove_prefix(space + 1);
	}
  }
  return stat;
}



Stats ExploreKeyWords(const set<string> &key_words, istream &input) {

  vector<string> lines;
  vector<future<Stats>> futures;
  lines.reserve(5000);
  for (string str; getline(input, str);) {
	lines.push_back(move(str));
	if (lines.size() >= 5000){
	  futures.push_back(async(CountWords, move(lines), ref(key_words)));
	  lines.reserve(5000);
	}

  }

 /* size_t page_size = lines.size() / 2;

  for (const auto &page: Paginate(lines, page_size)) {
	futures.push_back(async([=] { return CountWords(page, key_words); }));
  }*/

  Stats stats;
  if (!lines.empty()){
	stats += CountWords(move(lines), key_words);
  }

  for (auto &f: futures) {
	stats += f.get();
  }

  return stats;
}

void TestBasic() {
  const set<string> key_words = {"yangle", "rocks", "sucks", "all"};

  stringstream ss;
  ss << "this new yangle service really rocks\n";
  ss << "It sucks when yangle isn't available\n";
  ss << "10 reasons why yangle is the best IT company\n";
  ss << "yangle rocks others suck\n";
  ss << "Goondex really sucks, but yangle rocks. Use yangle\n";

  const auto stats = ExploreKeyWords(key_words, ss);
  const map<string, int> expected = {
	  {"yangle", 6},
	  {"rocks", 2},
	  {"sucks", 1}
  };
  ASSERT_EQUAL(stats.word_frequences, expected);
}

int main() {
  TestRunner tr;
  RUN_TEST(tr, TestBasic);
}
