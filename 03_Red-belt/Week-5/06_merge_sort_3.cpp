// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "test_runner.h"
#include <algorithm>
#include <memory>
#include <vector>

using namespace std;

template <typename RandomIt>
void MergeSort(RandomIt range_begin, RandomIt range_end) {
  if (range_end - range_begin < 2){
	return;
  }
  vector<typename RandomIt::value_type> result(make_move_iterator(range_begin), make_move_iterator(range_end));
  auto fist_part = result.begin() + result.size() / 3;
  auto second_part = result.begin() + result.size() / 3 * 2;
  MergeSort(result.begin(), fist_part);
  MergeSort(fist_part, second_part);
  MergeSort(second_part, result.end());
  vector<typename RandomIt::value_type> temp;
  merge(make_move_iterator(result.begin()),make_move_iterator(fist_part),make_move_iterator(fist_part),make_move_iterator(second_part), back_inserter(temp));
  merge(make_move_iterator(temp.begin()),make_move_iterator(temp.end()),make_move_iterator(second_part),make_move_iterator(result.end()), range_begin);
}

void TestIntVector() {
  vector<int> numbers = {6, 1, 3, 9, 1, 9, 8, 12, 1};
  MergeSort(begin(numbers), end(numbers));
  ASSERT(is_sorted(begin(numbers), end(numbers)));
}

int main() {
  TestRunner tr;
  RUN_TEST(tr, TestIntVector);
  return 0;
}
