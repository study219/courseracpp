// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "test_runner.h"

#include <vector>
#include <algorithm>

using namespace std;

// Объявляем Sentence<Token> для произвольного типа Token
// синонимом vector<Token>.
// Благодаря этому в качестве возвращаемого значения
// функции можно указать не малопонятный вектор векторов,
// а вектор предложений — vector<Sentence<Token>>.
template<typename Token>
using Sentence = vector<Token>;

// Класс Token имеет метод bool IsEndSentencePunctuation() const
template<typename Token>
vector<Sentence<Token>> SplitIntoSentences(vector<Token> tokens) {
  // Напишите реализацию функции, не копируя объекты типа Token
  vector<Sentence<Token>> result;
  auto begin_it = tokens.begin();
  const auto kEndIt = tokens.end();
  while (begin_it != kEndIt) {
	auto end_sentence_it = adjacent_find(begin_it,
										 kEndIt,
										 [](const auto &lhs, const auto &rhs) {
										   return lhs.IsEndSentencePunctuation() && !rhs.IsEndSentencePunctuation();
										 });
	if (end_sentence_it != kEndIt) {
	  end_sentence_it = next(end_sentence_it);
	}

	result.push_back(Sentence<Token>(make_move_iterator(begin_it), make_move_iterator(end_sentence_it)));
	begin_it = end_sentence_it;
  }

  return result;
}

struct TestToken {
  string data;
  bool is_end_sentence_punctuation = false;

  bool IsEndSentencePunctuation() const {
	return is_end_sentence_punctuation;
  }
  bool operator==(const TestToken &other) const {
	return data == other.data && is_end_sentence_punctuation == other.is_end_sentence_punctuation;
  }
};

ostream &operator<<(ostream &stream, const TestToken &token) {
  return stream << token.data;
}

// Тест содержит копирования объектов класса TestToken.
// Для проверки отсутствия копирований в функции SplitIntoSentences
// необходимо написать отдельный тест.
void TestSplitting() {
  ASSERT_EQUAL(
	  SplitIntoSentences(vector<TestToken>({{"Split"}, {"into"}, {"sentences"}, {"!"}})),
	  vector<Sentence<TestToken>>({
									  {{"Split"}, {"into"}, {"sentences"}, {"!"}}
								  })
  );

  ASSERT_EQUAL(
	  SplitIntoSentences(vector<TestToken>({{"Split"}, {"into"}, {"sentences"}, {"!", true}})),
	  vector<Sentence<TestToken>>({
									  {{"Split"}, {"into"}, {"sentences"}, {"!", true}}
								  })
  );

  ASSERT_EQUAL(
	  SplitIntoSentences(vector<TestToken>({{"Split"}, {"into"}, {"sentences"}, {"!", true}, {"!", true}, {"Without"},
											{"copies"}, {".", true}})),
	  vector<Sentence<TestToken>>({
									  {{"Split"}, {"into"}, {"sentences"}, {"!", true}, {"!", true}},
									  {{"Without"}, {"copies"}, {".", true}},
								  })
  );
}

int main() {
  TestRunner tr;
  RUN_TEST(tr, TestSplitting);
  return 0;
}
