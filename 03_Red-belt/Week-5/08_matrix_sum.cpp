// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "test_runner.h"
#include <vector>
#include <numeric>
#include <future>

using namespace std;

template<typename Iterator>
class Page {
 public:
  Page(const Iterator &f, const Iterator &l) : begin_(f), end_(l), size_(l - f) {}
  Iterator begin() const {
	return begin_;
  }
  Iterator end() const {
	return end_;
  }
  size_t size() const {
	return size_;
  }
 private:
  Iterator begin_;
  Iterator end_;
  size_t size_;
};

template<typename Iterator>
class Paginator {
 public:
  Paginator(Iterator begin, Iterator end, size_t page_size) {

	size_t item_count = distance(begin, end);
	while (item_count) {
	  auto page_end = min(page_size, item_count);
	  pages_.push_back({begin, begin + page_end});

	  item_count -= page_end;
	  begin = next(begin, page_end);
	}

  }
  auto begin() const {
	return pages_.begin();
  }
  auto end() const {
	return pages_.end();
  }
  auto size() const {
	return pages_.size();
  }
 private:
  vector<Page<Iterator>> pages_;
};

template<typename C>
auto Paginate(C &c, size_t page_size) {
  return Paginator{c.begin(), c.end(), page_size};
}

template<typename C>
int64_t CalcSum(const C& matrix) {
  int64_t result = 0;
  for (auto &word: matrix) {
	result += accumulate(word.begin(), word.end(), 0);
  }
  return result;
}

int64_t CalculateMatrixSum(const vector<vector<int>> &matrix) {
  // Реализуйте эту функцию

  vector<future<int64_t>> futures;
  for (const auto &x: Paginate(matrix, 2000)) {
	futures.push_back(async([=]{ return CalcSum(x);}));
  }
  int64_t result = 0;
  for(auto& f : futures){
	result+= f.get();
  }

  return result;
}

void TestCalculateMatrixSum() {
  const vector<vector<int>> matrix = {
	  {1, 2, 3, 4},
	  {5, 6, 7, 8},
	  {9, 10, 11, 12},
	  {13, 14, 15, 16}
  };
  ASSERT_EQUAL(CalculateMatrixSum(matrix), 136);
}

int main() {
  TestRunner tr;
  RUN_TEST(tr, TestCalculateMatrixSum);
}
