// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <array>
#include <string_view>
#include <set>
#include <string>

using namespace std;

using Id =  typename std::set<pair<string, int>>::iterator;

int main() {

  set<pair<string, int>> data;
  Id id = data.insert({"abc", 1}).first;

  id->second = 1;
  cout << id->second << endl;


  return 0;
}