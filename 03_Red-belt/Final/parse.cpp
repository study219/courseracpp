// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#include "parse.h"

string_view Strip(string_view s) {
  while (!s.empty() && isspace(s.front())) {
    s.remove_prefix(1);
  }
  while (!s.empty() && isspace(s.back())) {
    s.remove_suffix(1);
  }
  return s;
}

vector<string_view> SplitBy(string_view s, char sep) {
  vector<string_view> result;
  while (!s.empty()) {
    size_t pos = s.find(sep);
    result.push_back(s.substr(0, pos));
    s.remove_prefix(pos != s.npos ? pos + 1 : s.size());
  }
  return result;
}
void LeftStrip(string_view &s) {
  while (!s.empty() && isspace(s.front())) {
	s.remove_prefix(1);
  }
}
string_view ReadWord(string_view &s) {
  LeftStrip(s);
  size_t pos = s.find(' ');
  auto result = s.substr(0, pos);
  s.remove_prefix(pos != s.npos ? pos : s.size());
  return result;
}
vector<string_view> SplitIntoWordsView(string_view s) {
  vector<string_view> result;
  for (string_view word = ReadWord(s); !word.empty() ; word = ReadWord(s)) {
	result.push_back(word);
  }
  return result;
}


