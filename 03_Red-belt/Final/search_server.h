// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#pragma once

#include <istream>
#include <ostream>
#include <set>
#include <list>
#include <vector>
#include <map>
#include <string>
#include <string_view>
using namespace std;

struct Rate {
  size_t docid;
  size_t hitcount;
};

class InvertedIndex {
 public:
  InvertedIndex() = default;
  explicit InvertedIndex(istream &document_input);
  vector<Rate>& GetWordRating(string_view word);

  const string &GetDocument(size_t id) const;
  size_t GetDocumentsSize() const;

 private:
  vector<string> docs_;
  map<string_view , vector<Rate>> index_;
};

class SearchServer {
 public:
  SearchServer() = default;
  explicit SearchServer(istream &document_input);
  void UpdateDocumentBase(istream &document_input);
  void AddQueriesStream(istream &doc_id_lhs, ostream &doc_id_rhs);

 private:
  InvertedIndex index_;
};
