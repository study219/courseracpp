// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#include "search_server.h"
#include "iterator_range.h"
#include "parse.h"

#include <algorithm>
#include <iterator>
#include <sstream>
#include <iostream>
#include <numeric>

vector<string> SplitIntoWords(const string &line) {
  istringstream words_input(line);
  return {istream_iterator<string>(words_input), istream_iterator<string>()};
}

SearchServer::SearchServer(istream &document_input) {
  UpdateDocumentBase(document_input);
}

void SearchServer::UpdateDocumentBase(istream &document_input) {
  index_ = InvertedIndex(document_input);
}

void SearchServer::AddQueriesStream(
	istream &query_input, ostream &search_results_output
) {
  const size_t kDocsSize = index_.GetDocumentsSize();

  vector<size_t> docids_to_hitcount(kDocsSize);
  vector<int> docids(kDocsSize);

  for (string current_query; getline(query_input, current_query);) {
	//const auto kWords = SplitIntoWords(current_query);
	const auto kWords = SplitIntoWordsView(current_query);

	docids_to_hitcount.assign(kDocsSize, 0);
	iota(docids.begin(), docids.end(), 0);

	for (const auto &word: kWords) {
	  for (auto &x: index_.GetWordRating(word)) {
		docids_to_hitcount[x.docid] += x.hitcount;
	  }
	}

	partial_sort(docids.begin(), Head(docids, 5).end(), docids.end(),
				 [&docids_to_hitcount](int doc_id_lhs, int doc_id_rhs){
				   return make_pair(docids_to_hitcount[doc_id_lhs], -doc_id_lhs) > make_pair(docids_to_hitcount[doc_id_rhs], -doc_id_rhs); // минус потому что нужно вывести в обратном порядке docid
	});

	search_results_output << current_query << ':';
	for ( int& doc_id: Head(docids, 5)) {
	  if (docids_to_hitcount[doc_id] == 0) break;
	  search_results_output << " {"
							<< "docid: " << doc_id << ", "
							<< "hitcount: " << docids_to_hitcount[doc_id] << '}';
	}
	search_results_output << endl;
  }
}

InvertedIndex::InvertedIndex(istream &document_input) : docs_() {
  docs_.reserve(50'000);

  for (string current_document; getline(document_input, current_document);) {
	docs_.push_back(move(current_document));

	const size_t kDocid = docs_.size() - 1;
	//for (const auto &word: SplitIntoWords(docs_.back())) {
	for (const auto &word: SplitIntoWordsView(docs_.back())) {
	  auto &word_rating = index_[word];
	  if (!word_rating.empty() && word_rating.back().docid == kDocid) {
		word_rating.back().hitcount++;
	  } else {
		word_rating.push_back({kDocid, 1});
	  }
	}
  }

}

const string &InvertedIndex::GetDocument(size_t id) const {
  return docs_[id];
}
size_t InvertedIndex::GetDocumentsSize() const {
  return docs_.size();
}
vector<Rate> &InvertedIndex::GetWordRating(string_view word) {
  static vector<Rate> empty;
  if (auto it = index_.find(word); it != index_.end()) {
	return it->second;
  } else {
	return empty;
  }
}
