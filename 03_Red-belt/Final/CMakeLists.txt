cmake_minimum_required(VERSION 3.20)
project(Coursera)

set(CMAKE_CXX_STANDARD 17)
set(targetApp final)

add_executable(${targetApp} main.cpp parse.cpp search_server.cpp)
target_include_directories(${targetApp} PRIVATE ${PROJECT_SOURCE_DIR}/)


