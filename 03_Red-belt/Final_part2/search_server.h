// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#pragma once
#include "synchronized.h"

#include <istream>
#include <ostream>
#include <set>
#include <list>
#include <vector>
#include <map>
#include <string>
#include <future>
#include <deque>
#include <string_view>
using namespace std;

class InvertedIndex {
 public:
  struct Rate {
	size_t docid, hitcount;
  };
  InvertedIndex() = default;
  explicit InvertedIndex(istream &document_input);
  const vector<Rate> &GetWordRating(const string_view &word) const;

  const vector<string> &GetDocuments() const {
	return docs;
  }

 private:
  map<string_view , vector<Rate>> index;
  vector<string> docs;
};

class SearchServer {
 public:
  SearchServer() = default;
  explicit SearchServer(istream &document_input) : index_(InvertedIndex(document_input)) {
  }
  void UpdateDocumentBase(istream &document_input);
  void AddQueriesStream(istream &query_input, ostream &search_results_output);

 private:
  Synchronized<InvertedIndex> index_;
  vector<future<void>> async_tasks_;
};