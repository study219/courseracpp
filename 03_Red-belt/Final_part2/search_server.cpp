// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "search_server.h"
#include "iterator_range.h"

#include <algorithm>
#include <iterator>
#include <sstream>
#include <iostream>
#include <vector>
#include <numeric>
#include "parse.h"

vector<string> SplitIntoWords(const string &line) {
  istringstream words_input(line);
  return {istream_iterator<string>(words_input), istream_iterator<string>()};
}

void LeftStrip(string_view &s) {
  while (!s.empty() && isspace(s.front())) {
	s.remove_prefix(1);
  }
}
string_view ReadWord(string_view &s) {
  LeftStrip(s);
  size_t pos = s.find(' ');
  auto result = s.substr(0, pos);
  s.remove_prefix(pos != s.npos ? pos : s.size());
  return result;
}
vector<string_view> SplitIntoWordsView(string_view s) {
  vector<string_view> result;
  for (string_view word = ReadWord(s); !word.empty() ; word = ReadWord(s)) {
	result.push_back(word);
  }
  return result;
}

InvertedIndex::InvertedIndex(istream &document_input) {
  docs.reserve(50'000);
  for (string current_document; getline(document_input, current_document);) {
	docs.push_back(move(current_document));
	size_t docid = docs.size() - 1;
	for (const auto &word: SplitIntoWordsView(docs.back())) {
	  auto &docids = index[word];
	  if (!docids.empty() && docids.back().docid == docid) {
		++docids.back().hitcount;
	  } else {
		docids.push_back({docid, 1});
	  }
	}
  }
}

void UpdateIndex(istream &document_input, Synchronized<InvertedIndex> &index) {
  InvertedIndex new_index(document_input);
  swap(index.GetAccess().ref_to_value, new_index);
}

void SearchServer::UpdateDocumentBase(istream &document_input) {
  async_tasks_.push_back(async(UpdateIndex, ref(document_input), ref(index_)));
}

void AddQueriesStreamMulti(istream &query_input, ostream &search_results_output, Synchronized<InvertedIndex> &index_handle) {
  //const auto& documents = index.GetDocuments();
  vector<size_t> docid_count;
  vector<int64_t> search_results;

  for (string current_query; getline(query_input, current_query);) {
	const auto kWords = SplitIntoWordsView(current_query);

	//docid_count.assign(docid_count.size(), 0);
	{
	  auto access = index_handle.GetAccess();

	  const size_t kDocCount = access.ref_to_value.GetDocuments().size();
	  docid_count.assign(kDocCount, 0);
	  search_results.resize(kDocCount);

	  auto &index = access.ref_to_value;
	  for (const auto &word: kWords) {
		for (const auto&[kDocid, kHitCount]: index.GetWordRating(word)) {
		  docid_count[kDocid] += kHitCount;
		}
	  }
	}

	iota(begin(search_results), end(search_results), 0);

	partial_sort(
		begin(search_results),
		Head(search_results, 5).end(),
		end(search_results),
		[&docid_count](int64_t lhs, int64_t rhs) {
		  return make_pair(docid_count[lhs], -lhs) > make_pair(docid_count[rhs], -rhs);
		}
	);

	search_results_output << current_query << ':';
	for (auto docid: Head(search_results, 5)) {
	  if (docid_count[docid] == 0) break;
	  search_results_output << " {"
							<< "docid: " << docid << ", "
							<< "hitcount: " << docid_count[docid] << '}';
	}
	search_results_output << endl;
  }
}

void SearchServer::AddQueriesStream(istream &query_input, ostream &search_results_output) {
  async_tasks_.push_back(async(AddQueriesStreamMulti, ref(query_input), ref(search_results_output), ref(index_)));
}

const vector<InvertedIndex::Rate> &InvertedIndex::GetWordRating(const string_view &word) const {
  static const vector<Rate> kEmpty;
  if (auto it = index.find(word); it != index.end()) {
	return it->second;
  } else {
	return kEmpty;
  }
}