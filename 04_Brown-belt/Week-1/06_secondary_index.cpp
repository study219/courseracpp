// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "test_runner.h"

#include <iostream>
#include <map>
#include <string>
#include <unordered_map>

using namespace std;

struct Record {
  bool operator<(const Record &rhs) const {
	return id < rhs.id;
  }
  bool operator==(const Record &rhs) const {
	return id == rhs.id &&
		title == rhs.title &&
		user == rhs.user &&
		timestamp == rhs.timestamp &&
		karma == rhs.karma;
  }

  string id;
  string title;
  string user;
  int timestamp;
  int karma;
};

// Реализуйте этот класс
class Database {
 private:
  struct Data {
	Record record;
	multimap<int, const Record *>::iterator time_iter;
	multimap<int, const Record *>::iterator karma_iter;
	multimap<string, const Record *>::iterator user_iter;
  };

  unordered_map<string, Data> data_;
  multimap<int, const Record *> data_by_timestamp_;
  multimap<int, const Record *> data_by_karma_;
  multimap<string, const Record *> data_by_user_;
 public:
  bool Put(const Record &record) {

	auto[db_it, success_insert] = data_.insert({record.id, {record}});
	if (success_insert) {
	  auto &entry = db_it->second;
	  const Record *ptr = &entry.record;
	  entry.time_iter = data_by_timestamp_.insert({record.timestamp, ptr});
	  entry.karma_iter = data_by_karma_.insert({record.karma, ptr});
	  entry.user_iter = data_by_user_.insert({record.user, ptr});
	}
	return success_insert;
  }
  const Record *GetById(const string &id) const {
	if (data_.count(id) > 0) {
	  return &data_.at(id).record;
	}
	return nullptr;
  }
  bool Erase(const string &id) {
	auto it = data_.find(id);
	if (it != data_.end()) {
	  data_by_timestamp_.erase(it->second.time_iter);
	  data_by_karma_.erase(it->second.karma_iter);
	  data_by_user_.erase(it->second.user_iter);
	  data_.erase(it);
	  return true;
	}
	return false;
  }

  template<typename Callback>
  void RangeByTimestamp(int low, int high, Callback callback) const {
	auto it_low = data_by_timestamp_.lower_bound(low);
	auto it_high = data_by_timestamp_.upper_bound(high);
	while (it_low != it_high) {
	  if (!callback(*it_low->second)) {
		break;
	  }
	  ++it_low;
	}
  }

  template<typename Callback>
  void RangeByKarma(int low, int high, Callback callback) const {
	auto it_low = data_by_karma_.lower_bound(low);
	auto it_high = data_by_karma_.upper_bound(high);
	while (it_low != it_high) {
	  if (!callback(*it_low->second)) {
		break;
	  }
	  ++it_low;
	}
  }

  template<typename Callback>
  void AllByUser(const string &user, Callback callback) const {
	auto[it_low, it_high] = data_by_user_.equal_range(user);
	while (it_low != it_high) {
	  if (!callback(*it_low->second)) {
		break;
	  }
	  ++it_low;
	}
  }
};

void TestRangeBoundaries() {
  const int good_karma = 1000;
  const int bad_karma = -10;

  Database db;
  db.Put({"id1", "Hello there", "master", 1536107260, good_karma});
  db.Put({"id2", "O>>-<", "general2", 1536107260, bad_karma});

  int count = 0;
  db.RangeByKarma(bad_karma, good_karma, [&count](const Record &) {
	++count;
	return true;
  });

  ASSERT_EQUAL(2, count);
}

void TestSameUser() {
  Database db;
  db.Put({"id1", "Don't sell", "master", 1536107260, 1000});
  db.Put({"id2", "Rethink life", "master", 1536107260, 2000});

  int count = 0;
  db.AllByUser("master", [&count](const Record &) {
	++count;
	return true;
  });

  ASSERT_EQUAL(2, count);
}

void TestReplacement() {
  const string final_body = "Feeling sad";

  Database db;
  db.Put({"id", "Have a hand", "not-master", 1536107260, 10});
  db.Erase("id");
  db.Put({"id", final_body, "not-master", 1536107260, -10});

  auto record = db.GetById("id");
  ASSERT(record != nullptr);
  ASSERT_EQUAL(final_body, record->title);
}

int main() {
  TestRunner tr;
  RUN_TEST(tr, TestRangeBoundaries);
  RUN_TEST(tr, TestSameUser);
  RUN_TEST(tr, TestReplacement);
  return 0;
}
