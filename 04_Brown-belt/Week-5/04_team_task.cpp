// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include<iostream>
#include<map>
#include <vector>

using namespace std;

// Перечислимый тип для статуса задачи
enum class TaskStatus {
  NEW,          // новая
  IN_PROGRESS,  // в разработке
  TESTING,      // на тестировании
  DONE          // завершена
};

using TasksInfo = map<TaskStatus, int>;

class TeamTasks {
 public:
  // Получить статистику по статусам задач конкретного разработчика
  const TasksInfo &GetPersonTasksInfo(const string &person) const {
	return team_.at(person);
  }

  // Добавить новую задачу (в статусе NEW) для конкретного разработчитка
  void AddNewTask(const string &person) {
	team_[person][TaskStatus::NEW]++;
  }

  // Обновить статусы по данному количеству задач конкретного разработчика,
  // подробности см. ниже
  tuple<TasksInfo, TasksInfo> PerformPersonTasks(const string &person, int task_count) {

	TasksInfo &person_tasks = team_[person];

	TasksInfo updated_tasks, untouched_tasks;
	for (TaskStatus status = TaskStatus::NEW; status != TaskStatus::DONE; status = NextStatus(status)) {

	  updated_tasks[NextStatus(status)] = min(person_tasks[status], task_count);
	  task_count -= updated_tasks[NextStatus(status)];
	}

	for (TaskStatus status = TaskStatus::NEW; status != TaskStatus::DONE; status = NextStatus(status)) {
	  untouched_tasks[status] = person_tasks[status] - updated_tasks[NextStatus(status)];
	  person_tasks[status] += updated_tasks[status] - updated_tasks[NextStatus(status)];
	}

	person_tasks[TaskStatus::DONE] += updated_tasks[TaskStatus::DONE];

	RemoveZeros(updated_tasks);
	RemoveZeros(untouched_tasks);
	RemoveZeros(person_tasks);

	return {updated_tasks, untouched_tasks};
  }

 private:
  TaskStatus NextStatus(TaskStatus task_status) {
	return static_cast<TaskStatus>(static_cast<int>(task_status) + 1);
  }

  void RemoveZeros(TasksInfo &tasks_info) {

	vector<TaskStatus> item_to_delete;
	for (const auto&[status, count]: tasks_info) {
	  if (count == 0) {
		item_to_delete.push_back(status);
	  }
	}

	for(const auto& status : item_to_delete){
	  tasks_info.erase(status);
	}
  }

  map<string, TasksInfo> team_;
};

void PrintTasksInfo(TasksInfo tasks_info) {
  cout << tasks_info[TaskStatus::NEW] << " new tasks" <<
	   ", " << tasks_info[TaskStatus::IN_PROGRESS] << " tasks in progress" <<
	   ", " << tasks_info[TaskStatus::TESTING] << " tasks are being tested" <<
	   ", " << tasks_info[TaskStatus::DONE] << " tasks are done" << endl;
}

int main() {
  TeamTasks tasks;
  for (int i = 0; i < 3; ++i) {
	tasks.AddNewTask("Ivan");
  }

  cout << "Ivan's tasks: ";
  PrintTasksInfo(tasks.GetPersonTasksInfo("Ivan"));

  TasksInfo updated_tasks, untouched_tasks;

  tie(updated_tasks, untouched_tasks) =
	  tasks.PerformPersonTasks("Ivan", 2);
  cout << "Updated Ivan's tasks: ";
  PrintTasksInfo(updated_tasks);
  cout << "Untouched Ivan's tasks: ";
  PrintTasksInfo(untouched_tasks);

  tie(updated_tasks, untouched_tasks) =
	  tasks.PerformPersonTasks("Ivan", 2);
  cout << "Updated Ivan's tasks: ";
  PrintTasksInfo(updated_tasks);
  cout << "Untouched Ivan's tasks: ";
  PrintTasksInfo(untouched_tasks);

  return 0;
}
