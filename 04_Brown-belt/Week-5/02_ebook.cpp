// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#include <iomanip>
#include <iostream>
#include <vector>
#include <utility>
#include <numeric>

using namespace std;

class ReadingManager {
 public:
  ReadingManager()
	  : user_id_to_page_readed_(MAX_USER_COUNT_ + 1, 0),
		page_to_users_num_readed_(1'001, 0),
		user_count_(0) {}

  void Read(int user_id, int page_count) {
	if (user_id_to_page_readed_[user_id] == 0) {
	  user_count_++;
	} else {
	  int readed_page = user_id_to_page_readed_[user_id];
	  page_to_users_num_readed_[readed_page]--;
	}
	user_id_to_page_readed_[user_id] = page_count;
	page_to_users_num_readed_[page_count]++;
  }

  double Cheer(int user_id) const {
	const auto& readed_page = user_id_to_page_readed_[user_id];
	if (readed_page == 0){
	  return 0;
	}
	if (user_count_ == 1){
	  return 1.0;
	}

	int readed_users = accumulate(begin(page_to_users_num_readed_), begin(page_to_users_num_readed_) + readed_page, 0);

	return (readed_users * 1.0 / static_cast<double>(user_count_ - 1));
  }

 private:
  // Статическое поле не принадлежит какому-то конкретному
  // объекту класса. По сути это глобальная переменная,
  // в данном случае константная.
  // Будь она публичной, к ней можно было бы обратиться снаружи
  // следующим образом: ReadingManager::MAX_USER_COUNT.
  static const int MAX_USER_COUNT_ = 100'000;

  vector<int> user_id_to_page_readed_;
  vector<int> page_to_users_num_readed_;
  size_t user_count_ = 0;

};

int main() {
  // Для ускорения чтения данных отключается синхронизация
  // cin и cout с stdio,
  // а также выполняется отвязка cin от cout
  ios::sync_with_stdio(false);
  cin.tie(nullptr);

  ReadingManager manager;

  int query_count;
  cin >> query_count;

  for (int query_id = 0; query_id < query_count; ++query_id) {
	string query_type;
	cin >> query_type;
	int user_id;
	cin >> user_id;

	if (query_type == "READ") {
	  int page_count;
	  cin >> page_count;
	  manager.Read(user_id, page_count);
	} else if (query_type == "CHEER") {
	  cout << setprecision(6) << manager.Cheer(user_id) << "\n";
	}
  }

  return 0;
}