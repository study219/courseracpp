// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <algorithm>
#include <iostream>
#include <string>
#include <string_view>
#include <vector>

using namespace std;

vector<string> ParseDomains(istream &in = cin) {
  int count;
  in >> count;
  vector<string> result(count);
  for (string &domain: result) {
	in >> domain;
  }
  return result;
}

bool IsSubDomain(const string &lhs, const string &rhs) {

  const size_t kLhsSize = lhs.size();
  const size_t kRhsSize = rhs.size();

  if (kLhsSize == kRhsSize) {
	return lhs == rhs;
  } else if (kLhsSize < kRhsSize){
	return false;
  } else {
	string_view sv_lhs(lhs.data(), kRhsSize);
	string_view sv_rhs(rhs);
	return sv_lhs == sv_rhs && lhs[kRhsSize] == '.';
  }
}

int main() {


  vector<string> banned_domains = ParseDomains();
  vector<string> domains_to_check = ParseDomains();
  for (string& domain : banned_domains) {
	reverse(begin(domain), end(domain));
  }
  sort(begin(banned_domains), end(banned_domains));

  size_t insert_pos = 0;
  for (string& domain : banned_domains) {
	if (insert_pos == 0 || !IsSubDomain(domain, banned_domains[insert_pos - 1])) {
	  swap(banned_domains[insert_pos++], domain);
	}
  }
  banned_domains.resize(insert_pos);

  for (string &domain : domains_to_check) {
	reverse(begin(domain), end(domain));
	if (const auto it = upper_bound(begin(banned_domains), end(banned_domains), domain);
		it != begin(banned_domains) && IsSubDomain(domain, *prev(it))) {
	  cout << "Bad" << endl;
	} else {
	  cout << "Good" << endl;
	}
  }

}