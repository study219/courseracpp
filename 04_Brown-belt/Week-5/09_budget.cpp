// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <ctime>
#include <numeric>
#include <tuple>

using namespace std;

class Date {
 public:
  Date() = default;
  Date(int year, int month, int day) : year_(year - 1900), month_(month - 1), day_(day) {}
  bool operator==(const Date &rhs) const {
	return year_ == rhs.year_ &&
		month_ == rhs.month_ &&
		day_ == rhs.day_;
  }
  bool operator!=(const Date &rhs) const {
	return !(rhs == *this);
  }
  bool operator<(const Date &rhs) const {
	return tie(year_, month_, day_) < tie(rhs.year_, rhs.month_, rhs.day_);
  }
  bool operator>(const Date &rhs) const {
	return rhs < *this;
  }
  bool operator<=(const Date &rhs) const {
	return !(rhs < *this);
  }
  bool operator>=(const Date &rhs) const {
	return !(*this < rhs);
  }
  friend ostream &operator<<(ostream &os, const Date &date) {
	os << "year_: " << date.year_ << " month_: " << date.month_ << " day_: " << date.day_;
	return os;
  }

  friend istream &operator>>(istream &is, Date &date) {
	is >> date.year_;
	is.ignore(1);
	is >> date.month_;
	is.ignore(1);
	is >> date.day_;

	date.year_ -= 1900;
	date.month_ -= 1;
	return is;
  }
  int GetYear() const {
	return year_;
  }
  int GetMonth() const {
	return month_;
  }
  int GetDay() const {
	return day_;
  }
 private:
  int year_;
  int month_;
  int day_;
};

int DateDistance(const Date &date_from, const Date &date_to) {
  struct std::tm a = {0, 0, 0, date_from.GetDay(), date_from.GetMonth(), date_from.GetYear()}; /* June 24, 2004 */
  struct std::tm b = {0, 0, 0, date_to.GetDay(), date_to.GetMonth(), date_to.GetYear()};
  std::time_t x = std::mktime(&a);
  std::time_t y = std::mktime(&b);
 // if (x != (std::time_t)(-1) && y != (std::time_t)(-1)) {
	double difference = std::difftime(y, x) / (60 * 60 * 24);
	return static_cast<int>(difference);
 // }
//  return -1;
}

const Date kStartDate = Date(2000, 01, 01);
const Date kEndDate = Date(2100, 01, 01);
const size_t kDayCount = DateDistance(kStartDate, kEndDate);

struct Budget {
  double earn;
  double spend;
  Budget& operator+(const Budget &other){
	earn +=other.earn;
	spend +=other.spend;
	return *this;
  }
};


int main() {
  // Show time/date using default formatting
  ios::sync_with_stdio(false);
  cin.tie(nullptr);
  cout.precision(25);
  vector<Budget> budget(kDayCount, {.0, .0});
  int q;
  cin >> q;

  while (q) {
	string command;
	Date date_from{}, date_to{};
	cin >> command >> date_from >> date_to;
	auto start_index = DateDistance(kStartDate, date_from);
	auto finish_index = DateDistance(kStartDate, date_to);

	if (command == "Earn") {
	  double value;
	  cin >> value;

	  value /= DateDistance(date_from, date_to) + 1;

	  for (size_t j = start_index; j <= finish_index; ++j) {
		budget[j].earn += value;
	  }

	} else if (command == "PayTax") {
	  double percentage;
	  cin >> percentage;
	  percentage = (100 - percentage) / 100;
	  for (size_t j = start_index; j <= finish_index; ++j) {
		budget[j].earn *= percentage;
	  }

	} else if (command == "Spend") {
	  double value;
	  cin >> value;

	  value /= DateDistance(date_from, date_to) + 1;

	  for (size_t j = start_index; j <= finish_index; ++j) {
		budget[j].spend += value;
	  }

	} else if (command == "ComputeIncome") {

	  Budget
		  compute_budget = accumulate(budget.begin() + start_index, budget.begin() + finish_index + 1, Budget{0., 0.});
	  cout << compute_budget.earn - compute_budget.spend << endl;

	}
	--q;
  }

  return 0;
}