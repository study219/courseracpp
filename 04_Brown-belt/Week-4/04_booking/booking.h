// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#pragma once

namespace RAII {

template<typename T>
class Booking {
 public:
  Booking(T *data, int counter) : data_(data), counter_(counter) {}

  Booking(const Booking<T> &) = delete;
  Booking(Booking<T> &&other)  noexcept : data_(other.data_), counter_(other.counter_) {
	other.data_ = nullptr;
  }

  Booking<T> &operator=(Booking<T> &) = delete;

  Booking<T> &operator=(Booking<T> &&other)  noexcept {
	delete data_;
	data_ = other.data_;
	other.data_ = nullptr;
	return *this;
  }

  ~Booking() {
	if (data_) {
	  data_->CancelOrComplete(*this);
	}

  }

 private:
  T *data_;
  int counter_;
};

}