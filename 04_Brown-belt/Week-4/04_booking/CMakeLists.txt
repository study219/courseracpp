cmake_minimum_required(VERSION 3.20)
project(Coursera)

set(CMAKE_CXX_STANDARD 17)
set(targetApp Week4)

add_executable(${targetApp} test.cpp)
target_include_directories(${targetApp} PRIVATE ${PROJECT_SOURCE_DIR}/)


