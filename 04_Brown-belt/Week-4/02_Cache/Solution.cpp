#include "Common.h"
#include <mutex>
#include <list>
#include <utility>
#include <unordered_map>
using namespace std;

class LruCache : public ICache {
 public:
  LruCache(
	  shared_ptr<IBooksUnpacker> books_unpacker,
	  const Settings &settings
  ) : books_unpacker_(std::move(books_unpacker)), settings_(settings) {
	// реализуйте метод

  }

  BookPtr GetBook(const string &book_name) override {
	lock_guard l(mtx_);
	if (cache_.count(book_name)) {
	  cached_book_names_.remove(book_name);
	  cached_book_names_.push_back(book_name);
	  return cache_[book_name];
	} else {
	  cached_book_names_.push_back(book_name);
	  cache_[book_name] = books_unpacker_->UnpackBook(book_name);
	  cache_size_ += cache_[book_name]->GetContent().size();
	  while (cache_size_ > settings_.max_memory) {
		string book_name_remove = cached_book_names_.front();
		cached_book_names_.pop_front();
		cache_size_ -= cache_[book_name_remove]->GetContent().size();
		cache_.erase(book_name_remove);
	  }
	  return cache_[book_name];
	}

  }
 private:
  mutex mtx_;
  shared_ptr<IBooksUnpacker> books_unpacker_;
  const Settings &settings_;
  list<string> cached_book_names_;
  unordered_map<string, BookPtr> cache_{};
  size_t cache_size_ = 0;
};

unique_ptr<ICache> MakeCache(
	shared_ptr<IBooksUnpacker> books_unpacker,
	const ICache::Settings &settings
) {
  // реализуйте функцию
  return make_unique<LruCache>(move(books_unpacker), settings);
}
