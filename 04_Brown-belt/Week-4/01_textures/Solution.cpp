#include "Common.h"

using namespace std;

// Этот файл сдаётся на проверку
// Здесь напишите реализацию необходимых классов-потомков `IShape`
class FigureProxy : public IShape {
 public:
  std::unique_ptr<IShape> Clone() const final {
	return CloneChild();
  }
  void SetPosition(Point point) final {
	point_ = point;
  }

  Point GetPosition() const final {
	return point_;
  }

  void SetSize(Size size) final {
	size_ = size;
  }

  Size GetSize() const final {
	return size_;
  }

  void SetTexture(std::shared_ptr<ITexture> ptr) final {
	i_texture_ = ptr;
  }

  ITexture *GetTexture() const final {
	return i_texture_.get();
  }

  void Draw(Image &image) const final {

	for (int y = point_.y, y_image = 0; y < point_.y + size_.height && y < image.size(); y++, y_image++) {
	  for (int x = point_.x, x_image = 0; x < point_.x + size_.width && x < image[y].size(); x++, x_image++) {
		if (i_texture_) {
		  const Size &texture_size = i_texture_->GetSize();
		  if (IsPointInShape({x_image, y_image}) && y_image < texture_size.height
			  && x_image < texture_size.width) {
			image[y][x] = i_texture_->GetImage()[y_image][x_image];
			continue;
		  }
		}
		if (IsPointInShape({x_image, y_image})) {
		  image[y][x] = '.';
		}
	  }
	}
  }

  virtual bool IsPointInShape(Point) const = 0;
  virtual std::unique_ptr<IShape> CloneChild() const =0;

 protected:
  Point point_;
  Size size_;
  std::shared_ptr<ITexture> i_texture_;
};

class Rectangle : public FigureProxy {
 public:

  std::unique_ptr<IShape> CloneChild() const override {
	return std::make_unique<Rectangle>(*this);
  }

  bool IsPointInShape(Point point) const override {
	return true;
  }

};

class Ellipse : public FigureProxy {
 public:

  std::unique_ptr<IShape> CloneChild() const override {
	return std::make_unique<Ellipse>(*this);
  }

  bool IsPointInShape(Point point) const override {
	return IsPointInEllipse(point, FigureProxy::GetSize());
  }
};

// Напишите реализацию функции
unique_ptr<IShape> MakeShape(ShapeType shape_type) {
  switch (shape_type) {

	case ShapeType::Rectangle: return make_unique<Rectangle>();
	  break;
	case ShapeType::Ellipse: return make_unique<Ellipse>();
	  break;
  }
  return {};
}