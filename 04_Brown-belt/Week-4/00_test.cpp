// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <algorithm>
#include <utility>
#include <vector>
#include <numeric>

using namespace std;

struct SomeStruct {
  string a;
  int b;


  operator int() {
	return b;
  }

  SomeStruct(int b) : b(b) {}
  SomeStruct(string a, int b) : a(std::move(a)), b(b) {}
};

int operator+(const SomeStruct& l, const SomeStruct& r) {
  return l.b + r.b ;
}

int main() {

  vector<SomeStruct> v_structs;
  vector<int> v_sum;

  //Просуммировать b  в v_sum;
  partial_sum(v_structs.begin(),
			  v_structs.end(),
			  v_sum.begin());

  return 0;
}