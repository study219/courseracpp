#include "parser.h"
#include <istream>

using namespace std;

pair<string_view, optional<string_view>> SplitTwoStrict(string_view s, string_view delimiter) {
  const size_t kPos = s.find(delimiter);
  if (kPos == std::string_view::npos) {
	return {s, nullopt};
  } else {
	return {s.substr(0, kPos), s.substr(kPos + delimiter.length())};
  }
}

pair<string_view, string_view> SplitTwo(string_view s, string_view delimiter) {
  const auto[kLhs, kRhsOpt] = SplitTwoStrict(s, delimiter);
  return {kLhs, kRhsOpt.value_or("")};
}


string_view ReadToken(string_view &s, string_view delimiter) {
  const auto[kLhs, kRhs] = SplitTwo(s, delimiter);
  s = kRhs;
  return kLhs;
}

size_t ReadRequestCount(istream &stream) {
  size_t number;
  stream >> number;
  string eof;
  getline(stream, eof);
  return number;
}

double ConvertToDouble(std::string_view str) {
  // use std::from_chars when available to git rid of string copy
  size_t pos;
  const double kResult = stod(string(str), &pos);

  if (pos != str.length()) {
	str = str.substr(0, pos);
  }
  /*if (pos != str.length()) {
    std::stringstream error;
    error << "string " << str << " contains " << (str.length() - pos) << " trailing chars";
    throw invalid_argument(error.str());
  }*/
  return kResult;
}

