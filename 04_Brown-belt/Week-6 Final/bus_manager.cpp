#include "bus_manager.h"
#include "parser.h"
#include <iomanip>
#include <cmath>
#include <unordered_set>
#include <sstream>
#include <algorithm>

using namespace std;

double GetRouteLength(const Coordinate &lhs, const Coordinate &rhs) {
  return acos(
	  sin(lhs.lat_rad) * sin(rhs.lat_rad)
		  + cos(lhs.lat_rad) * cos(rhs.lat_rad) *
			  cos(abs(lhs.lon_rad - rhs.lon_rad))
  ) * 6371000;
}

Coordinate Coordinate::FromString(string_view &str) {
  const double kLatitude = ConvertToDouble(ReadToken(str, ", "));
  const double kLongitude = ConvertToDouble(ReadToken(str));

  return {kLatitude, kLongitude};
}

Coordinate::Coordinate(double latitude, double longitude)
	: lat_rad(latitude * 3.1415926535 / 180),
	  lon_rad(longitude * 3.1415926535 / 180) {
}

void BusManager::SetStopCoordinates(const string &stop_name,
									const Coordinate &coordinate,
									const optional<std::vector<StopToLength>> &stop_to_length) {
  stops_coords_[stop_name] = coordinate;
  stop_to_buses_[stop_name];

  if (stop_to_length != nullopt) {

	for (const auto &value: stop_to_length.value()) {
	  computed_length_[{stop_name, value.stop_name}].inputed_length = value.length;
	  if (computed_length_.find({value.stop_name, stop_name}) == computed_length_.end()) {
		computed_length_[{value.stop_name, stop_name}].inputed_length = value.length;
	  }
	}
  }

}
void BusManager::SetBusRoute(const string &bus_name, const Route &route) {
  bus_route_[bus_name] = route;
  for (auto &stop_name: bus_route_[bus_name].GetUniqueStops()) {
	stop_to_buses_[stop_name].insert(bus_name);
  }
}

void BusManager::ComputeBusesRoutes() {
  for (auto&[bus_name, route]: bus_route_) {

	for (size_t i = 1; i < route.GetStopsCount(); ++i) {
	  auto& route_stops = route.GetStops();

	  const auto &lhs_stop_name = route_stops[i - 1];
	  const auto &rhs_stop_name = route_stops[i];

	  const auto &lhs_coords = stops_coords_[lhs_stop_name];
	  const auto &rhs_coords = stops_coords_[rhs_stop_name];

	  auto &computed_length = computed_length_[{lhs_stop_name, rhs_stop_name}];
	  if (computed_length.linear_length == 0.0) {
		computed_length_[{rhs_stop_name, lhs_stop_name}].linear_length = computed_length.linear_length = GetRouteLength(lhs_coords, rhs_coords);
	  }

	  auto& route_lengths = route.GetLengthRef();
	  route_lengths+=computed_length;

	}

  }
}

std::string BusManager::GetComputedBusRoute(const string &bus_name) const {
  stringstream stream;
  stream << "Bus " << bus_name << ": ";
  if (const auto kIt = bus_route_.find(bus_name); kIt != bus_route_.end()) {
	stream << kIt->second;
  } else {
	stream << "not found";
  }
  return stream.str();
}
std::string BusManager::GetStopBuses(string_view stop_name) const {
  stringstream stream;
  stream << "Stop " << stop_name << ":";
  if (const auto kIt = stop_to_buses_.find(stop_name); kIt != stop_to_buses_.end()) {
	if (kIt->second.empty()) {
	  stream << " no buses";
	} else {
	  stream << " buses";
	  for (const auto &bus_name: kIt->second) {
		stream << " " << bus_name;
	  }
	}
  } else {
	stream << " not found";
  }
  return stream.str();
}
Json::Node BusManager::GetStopBusesJson(std::string_view stop_name) const {
  map<string, Json::Node> result;
  if (const auto kIt = stop_to_buses_.find(stop_name); kIt != stop_to_buses_.end()) {
	vector<Json::Node> array;
	array.reserve(kIt->second.size());
	for (const auto &bus_name: kIt->second) {
	  array.emplace_back((string(bus_name)));
	}
	result["buses"] = move(array);
  } else {
	result["error_message"] = Json::Node(string("not found"));
  }

  return result;
}
Json::Node BusManager::GetComputedBusRouteJson(const string &bus_name) const {
  map<string, Json::Node> result;
  if (const auto kIt = bus_route_.find(bus_name); kIt != bus_route_.end()) {

	result["route_length"] = kIt->second.GetLength().inputed_length;
	result["curvature"] = kIt->second.GetCurvature();
	result["stop_count"] = int(kIt->second.GetStopsCount());
	result["unique_stop_count"] = int(kIt->second.GetUniqueStopsCount());

  } else {
	result["error_message"] = Json::Node(string("not found"));
  }
  return result;
}

Route Route::FromString(string_view str) {
  Route r;
  string_view delimiter = str.find('>') != string_view::npos ? " > " : " - ";
  r.is_circle_ = delimiter == " > ";

  while (!str.empty()) {
	auto sv = ReadToken(str, delimiter);
	r.stops_.emplace_back(sv);


  }
  r.unique_stops_.insert(r.stops_.begin(),  r.stops_.end());

  if (!r.is_circle_){
	vector<string> stops_copy(r.stops_.begin(),  r.stops_.end() - 1);
	reverse(stops_copy.begin(), stops_copy.end());
	r.stops_.insert(r.stops_.end(), stops_copy.begin(),  stops_copy.end());
  }

  return r;
}

Route Route::FromJson(const Json::Node &node) {
  const auto& json_map = node.AsMap();
  const auto& stops = json_map.at("stops").AsArray();
  Route r;
  r.is_circle_ = json_map.at("is_roundtrip").AsString() == "true";
  r.stops_.resize(stops.size());

  for (size_t i = 0; i < r.stops_.size(); ++i) {
	r.stops_[i] = stops.at(i).AsString();
  }

  r.unique_stops_.insert(r.stops_.begin(),  r.stops_.end());

  if (!r.is_circle_){
	vector<string> stops_copy(r.stops_.begin(),  r.stops_.end() - 1);
	reverse(stops_copy.begin(), stops_copy.end());
	r.stops_.insert(r.stops_.end(), stops_copy.begin(),  stops_copy.end());
  }

  return r;
}

ostream &operator<<(ostream &os, const Route &route){
  os << route.GetStopsCount() << " stops on route, " << route.GetUniqueStopsCount() << " unique stops, "
	 << setprecision(6) << route.GetLength().inputed_length << " route length, "
	 << route.GetCurvature() << " curvature";
  return os;
}
size_t Route::GetStopsCount() const{
  return stops_.size();
}
size_t Route::GetUniqueStopsCount() const{
  return unique_stops_.size();
}
Length &Route::GetLengthRef() {
  return length_;
}
bool Route::IsCircle() const {
  return is_circle_;
}
double Route::GetCurvature() const {
  return length_.inputed_length / length_.linear_length;
}
const Length &Route::GetLength() const {
  return length_;
}
std::vector<std::string> &Route::GetStops() {
  return stops_;
}
std::unordered_set<std::string> &Route::GetUniqueStops() {
  return unique_stops_;
}


Length &Length::operator+=(Length &other) {
  this->inputed_length += other.inputed_length;
  this->linear_length += other.linear_length;
  return *this;
}
