// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#pragma once
#include "test_runner.h"
#include "requester.h"

void SimpleTest() {
  using namespace std;
  stringstream stream;
  stream << "13\n"
			"Stop Tolstopaltsevo: 55.611087, 37.20829, 3900m to Marushkino\n"
			"Stop Marushkino: 55.595884, 37.209755, 9900m to Rasskazovka\n"
			"Bus 256: Biryulyovo Zapadnoye > Biryusinka > Universam > Biryulyovo Tovarnaya > Biryulyovo Passazhirskaya > Biryulyovo Zapadnoye\n"
			"Bus 750: Tolstopaltsevo - Marushkino - Rasskazovka\n"
			"Stop Rasskazovka: 55.632761, 37.333324\n"
			"Stop Biryulyovo Zapadnoye: 55.574371, 37.6517, 7500m to Rossoshanskaya ulitsa, 1800m to Biryusinka, 2400m to Universam\n"
			"Stop Biryusinka: 55.581065, 37.64839, 750m to Universam\n"
			"Stop Universam: 55.587655, 37.645687, 5600m to Rossoshanskaya ulitsa, 900m to Biryulyovo Tovarnaya\n"
			"Stop Biryulyovo Tovarnaya: 55.592028, 37.653656, 1300m to Biryulyovo Passazhirskaya\n"
			"Stop Biryulyovo Passazhirskaya: 55.580999, 37.659164, 1200m to Biryulyovo Zapadnoye\n"
			"Bus 828: Biryulyovo Zapadnoye > Universam > Rossoshanskaya ulitsa > Biryulyovo Zapadnoye\n"
			"Stop Rossoshanskaya ulitsa: 55.595579, 37.605757\n"
			"Stop Prazhskaya: 55.611678, 37.603831\n"
			"6\n"
			"Bus 256\n"
			"Bus 750\n"
			"Bus 751\n"
			"Stop Samara\n"
			"Stop Prazhskaya\n"
			"Stop Biryulyovo Zapadnoye\n";

  stringstream excepted;
  excepted << "Bus 256: 6 stops on route, 5 unique stops, 5950 route length, 1.36124 curvature\n"
			  "Bus 750: 5 stops on route, 3 unique stops, 27600 route length, 1.31808 curvature\n"
			  "Bus 751: not found\n"
			  "Stop Samara: not found\n"
			  "Stop Prazhskaya: no buses\n"
			  "Stop Biryulyovo Zapadnoye: buses 256 828\n";

  const auto kRequestsIn = InputRequester().ReadRequest(stream);
  const auto kRequestsOut = OutputRequester().ReadRequest(stream);

  BusManager bus_manager;
  ProcessInputRequests(bus_manager, kRequestsIn);
  stringstream result;
  PrintResponses(ProcessOutputRequests(bus_manager, kRequestsOut), result);

  ASSERT_EQUAL(excepted.str(), result.str());
}

void SimpleTest2() {
  using namespace std;
  stringstream stream;
  stream << "3\n"
			"Stop A: 0.5, -1.0, 100000m to B\n"
			"Stop B: 0.0, -1.1, 10000m to A\n"
			"Bus 256: B - A\n"
			"4\n"
			"Bus 256\n"
			"Stop A\n"
			"Stop B\n"
			"Stop C\n";

  stringstream excepted;
  excepted << "Bus 256: 3 stops on route, 2 unique stops, 110000 route length, 0.970044 curvature\n"
			  "Stop A: buses 256\n"
			  "Stop B: buses 256\n"
			  "Stop C: not found\n";

  const auto kRequestsIn = InputRequester().ReadRequest(stream);
  const auto kRequestsOut = OutputRequester().ReadRequest(stream);

  BusManager bus_manager;
  ProcessInputRequests(bus_manager, kRequestsIn);
  stringstream result;
  PrintResponses(ProcessOutputRequests(bus_manager, kRequestsOut), result);

  ASSERT_EQUAL(excepted.str(), result.str());
}

void SimpleTest3() {
  using namespace std;
  stringstream stream;
  stream << "3\n"
			"Stop A b c: 0.5, -1.0, 100000m to B b c a\n"
			"Stop B b c a: 0.0, -1.1, 10000m to A b c\n"
			"Bus 256 az: B b c a - A b c\n"
			"8\n"
			"Bus 256\n"
			"Bus 256 az\n"
			"Stop A b c\n"
			"Stop B b c a\n"
			"Stop A\n"
			"Stop B\n"
			"Stop C\n"
			"Stop C asd\n";

  stringstream excepted;
  excepted << "Bus 256: not found\n"
			  "Bus 256 az: 3 stops on route, 2 unique stops, 110000 route length, 0.970044 curvature\n"
			  "Stop A b c: buses 256 az\n"
			  "Stop B b c a: buses 256 az\n"
			  "Stop A: not found\n"
			  "Stop B: not found\n"
			  "Stop C: not found\n"
			  "Stop C asd: not found\n";

  const auto kRequestsIn = InputRequester().ReadRequest(stream);
  const auto kRequestsOut = OutputRequester().ReadRequest(stream);

  BusManager bus_manager;
  ProcessInputRequests(bus_manager, kRequestsIn);
  stringstream result;
  PrintResponses(ProcessOutputRequests(bus_manager, kRequestsOut), result);

  ASSERT_EQUAL(excepted.str(), result.str());
}

void TestTypes() {
  using namespace std;
  ASSERT_EQUAL(static_cast<int>(InputRequester().ConvertRequestTypeFromString("Stop").value()),
			   static_cast<int>(Request::Type::ADD_STOP_COORDINATE));
  ASSERT_EQUAL(static_cast<int>(InputRequester().ConvertRequestTypeFromString("Bus").value()),
			   static_cast<int>(Request::Type::ADD_BUS_ROUTE));
  ASSERT_EQUAL(static_cast<int>(OutputRequester().ConvertRequestTypeFromString("Stop").value()),
			   static_cast<int>(Request::Type::SHOW_STOP_BUSES));
  ASSERT_EQUAL(static_cast<int>(OutputRequester().ConvertRequestTypeFromString("Bus").value()),
			   static_cast<int>(Request::Type::SHOW_BUS_ROUTE));
}

void TestAll() {
  TestRunner tr;
  RUN_TEST(tr, SimpleTest);
  RUN_TEST(tr, SimpleTest2);
  RUN_TEST(tr, SimpleTest3);
  RUN_TEST(tr, TestTypes);
}