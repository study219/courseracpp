// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "requester.h"
#include "tests.h"
#include "json.h"
#include <fstream>
#include <sstream>
using namespace std;

int main() {
 // TestAll();

  ifstream in_file("json.txt");
  stringstream ss;
  if (in_file) {
	copy(istreambuf_iterator<char>(in_file),
		 istreambuf_iterator<char>(),
		 ostreambuf_iterator<char>(ss));
  }

  Json::Document document = Json::Load(ss);
 // cout << document.GetRoot().AsMap().at("base_requests").AsArray()[0].AsMap().size();

  const auto kRequestsIn = InputRequester().ReadRequestJson(document.GetRoot().AsMap().at("base_requests"));
  const auto kRequestsOut = OutputRequester().ReadRequestJson(document.GetRoot().AsMap().at("stat_requests"));
  BusManager bus_manager;
  ProcessInputRequests(bus_manager, kRequestsIn);
  auto tmp = ProcessOutputRequestsJson(bus_manager, kRequestsOut);
 // PrintResponses(ProcessOutputRequests(bus_manager, kRequestsOut));
  /*const auto kRequestsIn = InputRequester().ReadRequest();
  const auto kRequestsOut = OutputRequester().ReadRequest();
  BusManager bus_manager;
  ProcessInputRequests(bus_manager, kRequestsIn);
  PrintResponses(ProcessOutputRequests(bus_manager, kRequestsOut));*/

  return 0;
}

