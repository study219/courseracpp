#include "requester.h"

using namespace std;

RequestPtr Request::Create(Request::Type type) {
  switch (type) {
	case Request::Type::ADD_STOP_COORDINATE: return std::make_unique<AddStopCoordinateRequest>();
	case Type::ADD_BUS_ROUTE: return std::make_unique<AddBusRouteRequest>();
	case Type::SHOW_BUS_ROUTE: return std::make_unique<ShowBusRouteRequest>();
	case Type::SHOW_STOP_BUSES: return std::make_unique<ShowStopBusesRequest>();
	default: return nullptr;
  }
}

ShowBusRouteRequest::ShowBusRouteRequest() : ReadRequest(Type::SHOW_BUS_ROUTE) {}

void ShowBusRouteRequest::ParseFrom(std::string_view input) {
  //bus_name_ = ;
  bus_name = ReadToken(input, "\n");
}

Json::Node ShowBusRouteRequest::Process(const BusManager &manager) const {
  //manager получаем Bus X: R stops on route, U unique stops, L route length
  return manager.GetComputedBusRouteJson(bus_name);
}
void ShowBusRouteRequest::ParseFromJson(const Json::Node &node) {
  const auto& json_map = node.AsMap();
  bus_name = json_map.at("name").AsString();
  request_id = static_cast<uint64_t>(json_map.at("id").AsDouble());
}

ShowStopBusesRequest::ShowStopBusesRequest() : ReadRequest(Type::SHOW_STOP_BUSES) {}

void ShowStopBusesRequest::ParseFrom(std::string_view input) {
  stop_name = ReadToken(input, "\n");
}

Json::Node ShowStopBusesRequest::Process(const BusManager &manager) const {
  return manager.GetStopBusesJson(stop_name);
}
void ShowStopBusesRequest::ParseFromJson(const Json::Node &node) {
  const auto& json_map = node.AsMap();
  stop_name = json_map.at("name").AsString();
  request_id = static_cast<uint64_t>(json_map.at("id").AsDouble());
}

AddStopCoordinateRequest::AddStopCoordinateRequest() : ModifyRequest(Type::ADD_STOP_COORDINATE) {}

void AddStopCoordinateRequest::ParseFrom(std::string_view input) {
  //coordinate парсим
  stop_name = ReadToken(input, ": ");
  coordinate = Coordinate::FromString(input);

  while (!input.empty()) {
	StopToLength buffer;
	buffer.length = ConvertToDouble(ReadToken(input, "m to "));
	buffer.stop_name = ReadToken(input, ", ");
	inputed_length.push_back(move(buffer));
  }

}
void AddStopCoordinateRequest::Process(BusManager &manager) const {
  //добавляем координаты к остановке
  manager.SetStopCoordinates(stop_name, coordinate, inputed_length);
}
void AddStopCoordinateRequest::ParseFromJson(const Json::Node &node) {
  const auto& json_map = node.AsMap();
  stop_name = json_map.at("name").AsString();
  coordinate = Coordinate(json_map.at("latitude").AsDouble(), json_map.at("longitude").AsDouble());


  const auto& inputed_length_map = json_map.at("road_distances").AsMap();
  for (const auto& [kStopName, kLength]: inputed_length_map) {
	inputed_length.push_back({kStopName, kLength.AsDouble()});
  }

}

AddBusRouteRequest::AddBusRouteRequest() : ModifyRequest(Type::ADD_BUS_ROUTE) {}

void AddBusRouteRequest::ParseFrom(std::string_view input) {
  bus_name = ReadToken(input, ": ");
  route = Route::FromString(input);
}

void AddBusRouteRequest::Process(BusManager &manager) const {
  manager.SetBusRoute(bus_name, route);
}
void AddBusRouteRequest::ParseFromJson(const Json::Node &node) {
  const auto& json_map = node.AsMap();
  bus_name = json_map.at("name").AsString();

  const auto& stops = json_map.at("stops");
  route = Route::FromJson(json_map);
}

Requester::Requester(const std::unordered_map<std::string_view, Request::Type> &string_to_type)
	: string_to_type_(string_to_type) {}

std::optional<Request::Type> Requester::ConvertRequestTypeFromString(std::string_view type_str) {
  if (const auto kIt = string_to_type_.find(type_str);
	  kIt != string_to_type_.end()) {
	return kIt->second;
  } else {
	return std::nullopt;
  }
}

RequestPtr Requester::ParseRequest(std::string_view request_str) {
  const auto kRequestType = ConvertRequestTypeFromString(ReadToken(request_str));
  if (!kRequestType) {
	return nullptr;
  }
  RequestPtr request = Request::Create(*kRequestType);
  if (request) {
	request->ParseFrom(request_str);
  }
  return request;
}

std::vector<RequestPtr> Requester::ReadRequest(std::istream &in_stream) {
  size_t request_count = ReadRequestCount(in_stream);

  std::vector<RequestPtr> requests;
  requests.reserve(request_count);

  for (size_t i = 0; i < request_count; ++i) {
	std::string request_str;
	getline(in_stream, request_str);
	if (auto request = ParseRequest(request_str)) {
	  requests.push_back(move(request));
	}
  }

  return requests;
}

RequestPtr Requester::ParseRequestJson(const Json::Node& node) {
  const auto& json_map = node.AsMap();

  const auto kRequestType = ConvertRequestTypeFromString(json_map.at("type").AsString());
  if (!kRequestType) {
	return nullptr;
  }
  RequestPtr request = Request::Create(*kRequestType);
  if (request) {
	request->ParseFromJson(node);
  }
  return request;
}

std::vector<RequestPtr> Requester::ReadRequestJson(const Json::Node& node) {
  const auto& json_array = node.AsArray();
  size_t request_count = json_array.size();

  std::vector<RequestPtr> requests;
  requests.reserve(request_count);

  for (size_t i = 0; i < request_count; ++i) {
	if (auto request = ParseRequestJson(json_array[i])) {
	  requests.push_back(move(request));
	}
  }

  return requests;
}

InputRequester::InputRequester(const std::unordered_map<std::string_view, Request::Type> &string_to_type)
	: Requester(string_to_type) {}

OutputRequester::OutputRequester(const std::unordered_map<std::string_view, Request::Type> &string_to_type)
	: Requester(string_to_type) {}

void ProcessInputRequests(BusManager &manager, const vector<std::unique_ptr<Request>> &requests) {
  for (const auto &request_holder: requests) {
	const auto &request = dynamic_cast<const ModifyRequest &>(*request_holder);
	request.Process(manager);
  }
  manager.ComputeBusesRoutes();
}

vector<string> ProcessOutputRequests(const BusManager &manager, const vector<std::unique_ptr<Request>> &requests) {

  vector<string> responses;
  responses.reserve(requests.size());

  for (auto &request_holder: requests) {
	const auto &request = dynamic_cast<const ReadRequest<string> &>(*request_holder);
	responses.push_back(request.Process(manager));

  }
  return responses;
}

void PrintResponses(const vector<std::string> &responses, ostream &stream) {
  for (const auto &response: responses) {
	stream << response << endl;
  }
}
std::vector<Json::Node> ProcessOutputRequestsJson(const BusManager &manager, const vector<RequestPtr> &requests) {
  vector<Json::Node> responses;
  responses.reserve(requests.size());

  for (auto &request_holder: requests) {
	const auto &request = dynamic_cast<const ReadRequest<Json::Node> &>(*request_holder);
	Json::Node processing = request.Process(manager);
	auto test = processing.AsMap();
	test["request_id"] = Json::Node(int(request.request_id));

	responses.emplace_back(move(test));

  }
  return responses;
}


