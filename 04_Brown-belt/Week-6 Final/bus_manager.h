#pragma once

#include <iostream>
#include <string_view>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <set>
#include <utility>
#include <list>
#include <optional>
#include <vector>

#include "json.h"

struct pair_hash {
  std::size_t operator()(const std::pair<std::string, std::string> &p) const {
	auto h_1 = hasher(p.first);
	auto h_2 = hasher(p.second);

	return h_1 ^ h_2;
  }
  std::hash<std::string> hasher;
};

struct Length {
  double inputed_length = 0.0;
  double linear_length = 0.0;

  Length& operator+=(Length& other);
};

struct StopToLength {
  std::string stop_name;
  double length;
};

using ComputedLength = std::unordered_map<std::pair<std::string, std::string>, Length, pair_hash>;

struct Coordinate {
  double lat_rad;
  double lon_rad;
  Coordinate() = default;
  Coordinate(double latitude, double longitude);
  static Coordinate FromString(std::string_view &str);
};

class Route {
 public:
  static Route FromString(std::string_view str);
  static Route FromJson(const Json::Node& node);
  size_t GetStopsCount() const;
  size_t GetUniqueStopsCount() const;
  Length &GetLengthRef();
  const Length& GetLength() const;
  bool IsCircle() const;
  double GetCurvature() const;
  std::vector<std::string>& GetStops();
  std::unordered_set<std::string>& GetUniqueStops();
  friend std::ostream &operator<<(std::ostream &os, const Route &route);
 private:
  bool is_circle_;
  std::vector<std::string> stops_;
  std::unordered_set<std::string> unique_stops_;
  Length length_;
};

double GetRouteLength(const Coordinate &lhs, const Coordinate &rhs);

class BusManager {
 public:
  void SetStopCoordinates(const std::string &stop_name,
						  const Coordinate &coordinate,
						  const std::optional<std::vector<StopToLength>> &stop_to_length = std::nullopt);
  void SetBusRoute(const std::string &bus_name, const Route &route);

  std::string GetComputedBusRoute(const std::string &bus_name) const;
  std::string GetStopBuses(std::string_view stop_name) const;
  Json::Node GetComputedBusRouteJson(const std::string &bus_name) const;
  Json::Node GetStopBusesJson(std::string_view stop_name) const;

  void ComputeBusesRoutes();
 private:

  ComputedLength computed_length_;
  std::unordered_map<std::string, Coordinate> stops_coords_;
  std::unordered_map<std::string, Route> bus_route_;
  std::unordered_map<std::string_view, std::set<std::string_view>> stop_to_buses_;
};