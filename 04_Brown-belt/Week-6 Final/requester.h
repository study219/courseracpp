#pragma once
#include "bus_manager.h"
#include "parser.h"
#include "json.h"

#include <memory>
#include <string_view>
#include <string>
#include <unordered_map>
#include <optional>
#include <iostream>
#include <vector>

struct Request;

using RequestPtr = std::unique_ptr<Request>;

struct Request {
  enum class Type {
	ADD_STOP_COORDINATE,
	ADD_BUS_ROUTE,
	SHOW_BUS_ROUTE,
	SHOW_STOP_BUSES
  };

  explicit Request(Type type) : type_(type) {}
  static RequestPtr Create(Type type);
  virtual void ParseFrom(std::string_view input) = 0;
  virtual void ParseFromJson(const Json::Node &node) = 0;
  virtual ~Request() = default;

  const Type type_;
};

template<typename ResultType>
struct ReadRequest : Request {
  using Request::Request;
  virtual ResultType Process(const BusManager &manager) const = 0;

  uint64_t request_id;
};

struct ModifyRequest : Request {
  using Request::Request;
  virtual void Process(BusManager &manager) const = 0;
};

struct ShowBusRouteRequest : ReadRequest<Json::Node> {
  ShowBusRouteRequest();
  void ParseFrom(std::string_view input) override;
  void ParseFromJson(const Json::Node &node) override;
  Json::Node Process(const BusManager &manager) const override;

  std::string bus_name;

};

struct ShowStopBusesRequest : ReadRequest<Json::Node> {
  ShowStopBusesRequest();
  void ParseFrom(std::string_view input) override;
  void ParseFromJson(const Json::Node &node) override;
  Json::Node Process(const BusManager &manager) const override;

  std::string stop_name;
};

struct AddStopCoordinateRequest : ModifyRequest {
  AddStopCoordinateRequest();
  void ParseFrom(std::string_view input) override;
  void ParseFromJson(const Json::Node &node) override;
  void Process(BusManager &manager) const override;

  std::string stop_name;
  Coordinate coordinate;


  std::vector<StopToLength> inputed_length;
};

struct AddBusRouteRequest : ModifyRequest {
  AddBusRouteRequest();
  void ParseFrom(std::string_view input) override;
  void ParseFromJson(const Json::Node &node) override;
  void Process(BusManager &manager) const override;

  std::string bus_name;
  Route route;
};

const std::unordered_map<std::string_view, Request::Type> kStringToInputType =
	{
		{"Stop", Request::Type::ADD_STOP_COORDINATE},
		{"Bus", Request::Type::ADD_BUS_ROUTE}
	};

const std::unordered_map<std::string_view, Request::Type> kStringToOutputType =
	{
		{"Stop", Request::Type::SHOW_STOP_BUSES},
		{"Bus", Request::Type::SHOW_BUS_ROUTE}
	};

class Requester {
 private:
  const std::unordered_map<std::string_view, Request::Type>& string_to_type_;
 public:
  explicit Requester(const std::unordered_map<std::string_view, Request::Type> &string_to_type);
  std::optional<Request::Type> ConvertRequestTypeFromString(std::string_view type_str);
  RequestPtr ParseRequest(std::string_view request_str);
  RequestPtr ParseRequestJson(const Json::Node& node);
  std::vector<RequestPtr> ReadRequest(std::istream &in_stream = std::cin);
  std::vector<RequestPtr> ReadRequestJson(const Json::Node& node);
};

class InputRequester : public Requester {
 public:
  explicit InputRequester(const std::unordered_map<std::string_view,
												   Request::Type> &string_to_type = kStringToInputType);
 private:

};

class OutputRequester : public Requester {
 public:
  explicit OutputRequester(const std::unordered_map<std::string_view,
													Request::Type> &string_to_type = kStringToOutputType);
};

//std::vector<std::pair<std::string, std::optional<Route>>> ProcessRequests(BusManager& manager, const std::vector<RequestPtr> &requests);


void ProcessInputRequests(BusManager& manager, const std::vector<RequestPtr> &requests);
std::vector<std::string> ProcessOutputRequests(const BusManager& manager, const std::vector<RequestPtr> &requests);
std::vector<Json::Node> ProcessOutputRequestsJson(const BusManager& manager, const std::vector<RequestPtr> &requests);
void PrintResponses(const std::vector<std::string> &responses, std::ostream &stream = std::cout);