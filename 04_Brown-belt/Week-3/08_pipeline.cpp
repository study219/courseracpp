// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "test_runner.h"
#include <functional>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>
#include <ostream>

using namespace std;

struct Email {

  string from;
  string to;
  string body;
  friend istream &operator>>(istream &in, Email &email) {
	getline(in, email.from);
	getline(in, email.to);
	getline(in, email.body);
	return in;
  }
};

class Worker {
 public:
  virtual ~Worker() = default;
  virtual void Process(unique_ptr<Email> email) = 0;
  virtual void Run() {
	// только первому worker-у в пайплайне нужно это имплементировать
	throw logic_error("Unimplemented");
  }

 protected:
  // реализации должны вызывать PassOn, чтобы передать объект дальше
  // по цепочке обработчиков
  void PassOn(unique_ptr<Email> email) const {
	if (next_) {
	  next_->Process(move(email));
	}
  }

 public:
  void SetNext(unique_ptr<Worker> next) {
	next_ = move(next);
  }

 //private:
  unique_ptr<Worker> next_;
};

class Reader : public Worker {
 public:
  explicit Reader(istream &in) : in_(in) {}
  void Process(unique_ptr<Email> email) override {

  }
  void Run() override {
	Email email;
	while (in_ >> email) {
	  PassOn(make_unique<Email>(move(email)));
	}
  }
 private:
  // реализуйте класс
  istream &in_;
};

class Filter : public Worker {
 public:
  using Function = function<bool(const Email &)>;
  explicit Filter(Function function) : function_(std::move(function)) {}
 public:
  // реализуйте класс
  void Process(unique_ptr<Email> email) override {
	if (function_(*email)) {
	  PassOn(move(email));
	}
  }
  Function function_;
};

class Copier : public Worker {
 public:
  // реализуйте класс
  explicit Copier(string to) : to_(std::move(to)) {}
  void Process(unique_ptr<Email> email) override {
	if (email->to != to_){
	  auto copy = make_unique<Email>(*email);
	  copy->to = to_;
	  PassOn(move(email));
	  PassOn(move(copy));
	} else {
	  PassOn(move(email));
	}

  }

  string to_;
};

class Sender : public Worker {
 public:
  // реализуйте класс
  explicit Sender(ostream &out) : out_(out) {}
  void Process(unique_ptr<Email> email) override {
	out_ << email->from << endl << email->to << endl << email->body << endl;
	PassOn(move(email));
  }

  ostream &out_;
};

// реализуйте класс
class PipelineBuilder {
 public:
  // добавляет в качестве первого обработчика Reader
  explicit PipelineBuilder(istream &in){
	worker_ = make_unique<Reader>(in);
	prev_ = worker_.get();
	//workers_.push_back(move(make_unique<Reader>(in)));
  }

  // добавляет новый обработчик Filter
  PipelineBuilder &FilterBy(Filter::Function filter){
	//workers_.push_back(move(make_unique<Filter>(filter)));
	/*auto ptr = worker_->next_.get();
	while (ptr->next_){
	  ptr = ptr->next_.get();
	}
	ptr->SetNext( make_unique<Filter>(filter));*/
	prev_->SetNext(make_unique<Filter>(filter));
	prev_ = prev_->next_.get();
	return *this;
  }

  // добавляет новый обработчик Copier
  PipelineBuilder &CopyTo(string recipient){
	//workers_.push_back(move(make_unique<Copier>(move(recipient))));
	prev_->SetNext(make_unique<Copier>(move(recipient)));
	prev_ = prev_->next_.get();
	return *this;
  }

  // добавляет новый обработчик Sender
  PipelineBuilder &Send(ostream &out){
	//workers_.push_back(move(make_unique<Sender>(out)));
	prev_->SetNext(move(make_unique<Sender>(out)));
	prev_ = prev_->next_.get();
	return *this;
  }

  // возвращает готовую цепочку обработчиков
  unique_ptr<Worker> Build(){
	/*for (size_t i = workers_.size() - 1; i > 0 ; --i) {
	  workers_[i - 1]->SetNext(move(workers_[i]));
	}
	return move(workers_[0]);*/
	return move(worker_);
  }
 private:
  Worker* prev_;
  unique_ptr<Worker> worker_;
  //vector<unique_ptr<Worker>> workers_;
};

void TestSanity() {
  string input = (
	  "erich@example.com\n"
	  "richard@example.com\n"
	  "Hello there\n"

	  "erich@example.com\n"
	  "ralph@example.com\n"
	  "Are you sure you pressed the right button?\n"

	  "ralph@example.com\n"
	  "erich@example.com\n"
	  "I do not make mistakes of that kind\n"
  );
  istringstream inStream(input);
  ostringstream outStream;

  PipelineBuilder builder(inStream);
  builder.FilterBy([](const Email &email) {
	return email.from == "erich@example.com";
  });
  builder.CopyTo("richard@example.com");
  builder.Send(outStream);
  auto pipeline = builder.Build();

  pipeline->Run();

  string expectedOutput = (
	  "erich@example.com\n"
	  "richard@example.com\n"
	  "Hello there\n"

	  "erich@example.com\n"
	  "ralph@example.com\n"
	  "Are you sure you pressed the right button?\n"

	  "erich@example.com\n"
	  "richard@example.com\n"
	  "Are you sure you pressed the right button?\n"
  );

  ASSERT_EQUAL(expectedOutput, outStream.str());
}

int main() {
  TestRunner tr;
  RUN_TEST(tr, TestSanity);
  return 0;
}
