// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "Common.h"
#include "test_runner.h"

#include <sstream>

using namespace std;

namespace expr {
class Value : public Expression {
 public:
  explicit Value(int value) : value_(value) {}
  int Evaluate() const override {
	return value_;
  }
  string ToString() const override {
	return to_string(value_);
  }
 private:
  int value_;
};

class BinaryOp : public Expression {
 public:
  BinaryOp(ExpressionPtr left, ExpressionPtr right) : left_(move(left)), right_(move(right)) {}
  int Evaluate() const final {
	return EvaluateOp(left_->Evaluate(), right_->Evaluate());
  }

  string ToString() const final {
	return string("(" + left_->ToString() + ")" + GetOp() + "(" + right_->ToString() + ")");
  }

 private:
  virtual char GetOp() const = 0;
  virtual int EvaluateOp(int left, int right) const = 0;

 private:
  ExpressionPtr left_;
  ExpressionPtr right_;
};

class Sum : public BinaryOp {
 public:
  using BinaryOp::BinaryOp; //
 private:
  char GetOp() const override {
	return '+';
  }
  int EvaluateOp(int left, int right) const override {
	return left + right;
  }
};

class Product : public BinaryOp {
 public:
  using BinaryOp::BinaryOp;
 private:
  char GetOp() const override {
	return '*';
  }
  int EvaluateOp(int left, int right) const override {
	return left * right;
  }
};
}

ExpressionPtr Value(int value) {
  return make_unique<expr::Value>(value);
}
ExpressionPtr Sum(ExpressionPtr left, ExpressionPtr right) {
  return make_unique<expr::Sum>(move(left), move(right));
}
ExpressionPtr Product(ExpressionPtr left, ExpressionPtr right) {
  return make_unique<expr::Product>(move(left), move(right));
}

string Print(const Expression *e) {
  if (!e) {
	return "Null expression provided";
  }
  stringstream output;
  output << e->ToString() << " = " << e->Evaluate();
  return output.str();
}

void Test() {
  ExpressionPtr e1 = Product(Value(2), Sum(Value(3), Value(4)));
  ASSERT_EQUAL(Print(e1.get()), "(2)*((3)+(4)) = 14");

  ExpressionPtr e2 = Sum(move(e1), Value(5));
  ASSERT_EQUAL(Print(e2.get()), "((2)*((3)+(4)))+(5) = 19");

  ASSERT_EQUAL(Print(e1.get()), "Null expression provided");
}

int main() {
  TestRunner tr;
  RUN_TEST(tr, Test);
  return 0;
}