// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <algorithm>
#include <iostream>
#include <vector>
#include <string>
#include <numeric>
#include <optional>

using namespace std;

template<typename Iterator>
class IteratorRange {
 public:
  IteratorRange(Iterator begin, Iterator end)
	  : first(begin), last(end) {
  }

  Iterator begin() const {
	return first;
  }

  Iterator end() const {
	return last;
  }

 private:
  Iterator first, last;
};

template<typename Collection>
auto Head(Collection &v, size_t top) {
  return IteratorRange{v.begin(), next(v.begin(), min(top, v.size()))};
}

struct Person {
  string name;
  int age, income;
  bool is_male;
  operator int() const { return income; }

  Person &operator+(const Person &rhs) {
	income += rhs.income;
	return *this;
  }
};

vector<Person> ReadPeople(istream &input) {
  int count;
  input >> count;

  vector<Person> result(count);
  for (Person &p: result) {
	char gender;
	input >> p.name >> p.age >> p.income >> gender;
	p.is_male = gender == 'M';
  }

  return result;
}

struct PersonStats {
  vector<Person> sorted_persons;
  vector<int> top_wealthy_persons_sum;
  optional<string> popular_man_name;
  optional<string> popular_woman_name;

  explicit PersonStats(vector<Person> v) : sorted_persons(move(v)), top_wealthy_persons_sum(sorted_persons.size()) {

	sort(
		begin(sorted_persons), end(sorted_persons), [](const Person &lhs, const Person &rhs) {
		  return lhs.income > rhs.income;
		}
	);

	partial_sum(sorted_persons.begin(),
				sorted_persons.end(),
				top_wealthy_persons_sum.begin());

	IteratorRange mans{
		begin(sorted_persons),
		partition(begin(sorted_persons), end(sorted_persons), [](const Person &p) {
		  return p.is_male;
		})
	};
	IteratorRange womens{mans.end(), sorted_persons.end()};

	popular_man_name = FindMostName(mans);
	popular_woman_name = FindMostName(womens);

	sort(begin(sorted_persons), end(sorted_persons), [](const Person &lhs, const Person &rhs) {
	  return lhs.age < rhs.age;
	});

  }

  template<class T>
  optional<string> FindMostName(IteratorRange<T> range) {
	if (range.begin() == range.end()) {
	  return std::nullopt;
	} else {
	  sort(range.begin(), range.end(), [](const Person &lhs, const Person &rhs) {
		return lhs.name < rhs.name;
	  });
	  const string *most_popular_name = &range.begin()->name;
	  int count = 1;
	  for (auto i = range.begin(); i != range.end();) {
		auto same_name_end = find_if_not(i, range.end(), [i](const Person &p) {
		  return p.name == i->name;
		});
		auto cur_name_count = std::distance(i, same_name_end);
		if (cur_name_count > count) {
		  count = cur_name_count;
		  most_popular_name = &i->name;
		}
		i = same_name_end;
	  }
	  return *most_popular_name;
	}
  }


};

int main() {

  const PersonStats kPeopleStats(ReadPeople(cin));

  for (string command; cin >> command;) {
	if (command == "AGE") {
	  int adult_age;
	  cin >> adult_age;

	  auto adult_begin = lower_bound(
		  begin(kPeopleStats.sorted_persons),
		  end(kPeopleStats.sorted_persons),
		  adult_age,
		  [](const Person &lhs, int age) {
			return lhs.age < age;
		  }
	  );

	  cout << "There are " << std::distance(adult_begin, end(kPeopleStats.sorted_persons))
		   << " adult people for maturity age " << adult_age << '\n';

	} else if (command == "WEALTHY") {
	  int count;
	  cin >> count;
	  cout << "Top-" << count << " people have total income " << kPeopleStats.top_wealthy_persons_sum[count - 1]
		   << '\n';

	} else if (command == "POPULAR_NAME") {
	  char gender;
	  cin >> gender;

	  const optional<string> &name = gender == 'M' ? kPeopleStats.popular_man_name : kPeopleStats.popular_woman_name;

	  if (name) {
		cout << "Most popular name among people of gender " << gender << " is "
			 << name.value() << '\n';

	  } else {
		cout << "No people of gender " << gender << '\n';
	  }
	}
  }
}
