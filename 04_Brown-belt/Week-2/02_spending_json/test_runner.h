// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#pragma once

#include <sstream>
#include <stdexcept>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <vector>

using namespace std;

template <class T>
ostream& operator << (ostream& os, const vector<T>& s);
template <class T>
ostream& operator << (ostream& os, const set<T>& s);
template <class K, class V>
ostream& operator << (ostream& os, const map<K, V>& m);
template<class F, class S>
ostream &operator<<(ostream &os, const pair<F, S> &s);
template<class T, class U>
void AssertEqual(const T& t, const U& u, const string& hint);
inline void Assert(bool b, const string& hint);

class TestRunner {
 public:
  template<class TestFunc>
  void RunTest(TestFunc func, const string &test_name);
  ~TestRunner();
 private:
  int fail_count_ = 0;
};

template<class F, class S>
ostream &operator<<(ostream &os, const pair<F, S> &s) {
  os << s.first << ' ' << s.second;
  return os;
}


template <class T>
ostream& operator << (ostream& os, const vector<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
	if (!first) {
	  os << ", ";
	}
	first = false;
	os << x;
  }
  return os << "}";
}

template <class T>
ostream& operator << (ostream& os, const set<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
	if (!first) {
	  os << ", ";
	}
	first = false;
	os << x;
  }
  return os << "}";
}

template <class K, class V>
ostream& operator << (ostream& os, const map<K, V>& m) {
  os << "{";
  bool first = true;
  for (const auto& kv : m) {
	if (!first) {
	  os << ", ";
	}
	first = false;
	os << kv.first << ": " << kv.second;
  }
  return os << "}";
}

template<class T, class U>
void AssertEqual(const T& t, const U& u, const string& hint) {
  if (!(t == u)) {
	ostringstream os;
	os << "Assertion failed: " << t << " != " << u;
	if (!hint.empty()) {
	  os << " hint: " << hint;
	}
	throw runtime_error(os.str());
  }
}

inline void Assert(bool b, const string& hint) {
  AssertEqual(b, true, hint);
}

template<class TestFunc>
void TestRunner::RunTest(TestFunc func, const string &test_name) {
  try {
	func();
	cerr << test_name << " OK" << endl;
  }
  catch (runtime_error &e) {
	++fail_count_;
	cerr << test_name << " fail: " << e.what() << endl;
  }
}

TestRunner::~TestRunner() {
  if (fail_count_ > 0) {
	cerr << fail_count_ << " test failed. Terminate";
	exit(1);
  }
}

#define ASSERT_EQUAL(x, y) {            \
  ostringstream __assert_equal_private_os;                     \
  __assert_equal_private_os << #x << " != " << #y << ", "      \
    << __FILE__ << ":" << __LINE__;     \
  AssertEqual(x, y, __assert_equal_private_os.str());          \
}

#define ASSERT(x) {                     \
  ostringstream __assert_equal_private_os;                     \
  __assert_equal_private_os << #x << " is false, "             \
    << __FILE__ << ":" << __LINE__;     \
  Assert(x, __assert_equal_private_os.str());                  \
}

#define RUN_TEST(tr, func) \
  tr.RunTest(func, #func)