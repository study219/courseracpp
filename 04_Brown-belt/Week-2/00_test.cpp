// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include<iostream>
#include <map>

using namespace std;

int main() {
multimap<int,int> data;
data.insert({0,3});
data.insert({1,4});
data.insert({1,5});
data.insert({2,6});
data.insert({2,7});

auto it_1 = data.lower_bound(1);
auto it_2 = data.upper_bound(2);

  while (it_1 != it_2){
	cout <<it_1 ->second << endl;
	it_1 ++;
  }


  return 0;
}