// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "ini.h"

#include <iostream>

size_t Ini::Document::SectionCount() const {
  return sections_.size();
}
const Ini::Section &Ini::Document::GetSection(const std::string &name) const {
  return sections_.at(name);
}
Ini::Section &Ini::Document::AddSection(std::string name) {
  return sections_.insert({name,{}}).first->second;
}



Ini::Document Ini::Load(std::istream &input) {
  Document doc;
  Ini::Section *section = nullptr;
  for (string line; getline(input, line);){
	if (line.empty()) continue;

	if (line[0] == '[' && line.back() == ']'){
	  section = &doc.AddSection(line.substr(1,line.size()-2));
	  continue;
	}

	if (section){
	  auto it = line.find('=');
	  section->insert({line.substr(0,it), line.substr(it+1)});
	}
  }


  return doc;
}
