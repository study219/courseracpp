// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#pragma once
#include <string>
#include <unordered_map>

namespace Ini {
using namespace std;

using Section = unordered_map<string, string>;

class Document {
 public:
  Section &AddSection(string name);
  const Section &GetSection(const string &name) const;
  size_t SectionCount() const;

 private:
  unordered_map<string, Section> sections_;
};

Document Load(istream &input);
}