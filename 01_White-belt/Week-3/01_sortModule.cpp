// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <vector>
#include <algorithm>


using namespace std;

bool Myfunction (int i,int j) { return (abs(i) < abs(j)); }


int main()
{
	int n;
	cin >> n;
	vector<int> v(n);
	for (auto& num: v)
	{
		cin >> num;
	}
	sort(v.begin(),  v.end(), Myfunction);
	for (const auto& num: v)
	{
		cout << num << " ";
	}

	return 0;
}
