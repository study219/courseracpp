// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <vector>
#include <algorithm>


using namespace std;

class Weather {
 public:
  Weather(const string& new_state, int new_temperature) {
	state = new_state;
	temperature = new_temperature;
	cout << 'c';
  }
  ~Weather() {
	cout << 'd';
  }
 private:
  string state;
  int temperature;
};

Weather GetMayWeather(int day) {
  if (day == 9) {
	cout << 'b';
	return {"clear", 10};
  } else {
	cout << 'b';
	return {"cloudy", 10};
  }
}

void f(int x) {
  cout << 'a';
  if (x % 2 == 1) {
	Weather weather("rain", 5);
	cout << 'b';
  }
  Weather weather2("fog", 4);
  cout << 'e';
}

int main() {
  f(11);
  return 0;
}