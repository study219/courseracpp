// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class SortedStrings {
 public:
  void AddString(const string &s) {
	// добавить строку s в набор
	sorted_strings_.push_back(s);
	sort(sorted_strings_.begin(), sorted_strings_.end());
  }
  vector<string> GetSortedStrings() {
	// получить набор из всех добавленных строк в отсортированном порядке
	return sorted_strings_;
  }
 private:
  // приватные поля
  vector<string> sorted_strings_;
};

void PrintSortedStrings(SortedStrings &strings) {
  for (const string &s: strings.GetSortedStrings()) {
	cout << s << " ";
  }
  cout << endl;
}

int main() {
  SortedStrings strings;

  strings.AddString("first");
  strings.AddString("third");
  strings.AddString("second");
  PrintSortedStrings(strings);

  strings.AddString("second");
  PrintSortedStrings(strings);

  return 0;
}