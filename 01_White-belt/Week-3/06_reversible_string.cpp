// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>


using namespace std;

class ReversibleString{
 public:
  ReversibleString()= default;
  explicit ReversibleString(const string& s) {
	str_ = std::move(s);
  }
  void Reverse(){
	reverse(str_.begin(), str_.end());
  }
  string ToString() const{
	return str_;
  }
 private:
  string str_;
};


int main() {
  ReversibleString s("live");
  s.Reverse();
  cout << s.ToString() << endl;

  s.Reverse();
  const ReversibleString& s_ref = s;
  string tmp = s_ref.ToString();
  cout << tmp << endl;

  ReversibleString empty;
  cout << '"' << empty.ToString() << '"' << endl;

  return 0;
}
