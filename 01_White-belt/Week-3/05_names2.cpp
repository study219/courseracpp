// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <map>

using namespace std;

class Person {
 public:
  void ChangeFirstName(int year, const string &first_name) {
	// добавить факт изменения имени на first_name в год year
	full_name_[year].first_name = first_name;
  }
  void ChangeLastName(int year, const string &last_name) {
	// добавить факт изменения фамилии на last_name в год year
	full_name_[year].last_name = last_name;
  }
  string GetFullName(int year) {
	// получить имя и фамилию по состоянию на конец года year
	//return last_name.lower_bound(year)->second + " " + first_name.lower_bound(year)->second;
	if (full_name_.begin()->first > year) {
	  return "Incognito";
	} else {
	  string fn;
	  string ln;
	  for (const auto&[kYear, kFullname]: full_name_) {
		if (kYear <= year && !kFullname.first_name.empty()) {
		  fn = kFullname.first_name;
		}
		if (kYear <= year && !kFullname.last_name.empty()) {
		  ln = kFullname.last_name;
		}
	  }

	  if (fn.empty()) {
		return ln + " with unknown first name";
	  } else if (ln.empty()) {
		return fn + " with unknown last name";
	  } else {
		return fn + " " + ln;
	  }
	}

  }
  string GetFullNameWithHistory(int year) {
	if (full_name_.begin()->first > year) {
	  return "Incognito";
	} else {
	  vector<string> fn;
	  vector<string> ln;
	  for (const auto&[kYear, kFullname]: full_name_) {
		if (kYear <= year && !kFullname.first_name.empty()) {
		  if (fn.empty()) {
			fn.push_back(kFullname.first_name);
		  } else if (fn.back() != kFullname.first_name) {
			fn.push_back(kFullname.first_name);
		  }
		}
		if (kYear <= year && !kFullname.last_name.empty()) {
		  if (ln.empty()) {
			ln.push_back(kFullname.last_name);
		  } else if (ln.back() != kFullname.last_name) {
			ln.push_back(kFullname.last_name);
		  }
		}

	  }
	  reverse(fn.begin(),  fn.end());
	  reverse(ln.begin(),  ln.end());

	  if (fn.empty()) {
		return GetString(ln) + " with unknown first name";
	  } else if (ln.empty()) {
		return GetString(fn) + " with unknown last name";
	  } else {
		return GetString(fn) + " " + GetString(ln);
	  }
	}

  }
 private:
  string GetString(vector<string> &str) {
	if (str.size() == 1) {
	  return str.back();
	} else if (str.size() == 2) {
	  return str.front() + " (" + str.back() + ")";
	} else {
	  string result = str.front() + " (";
	  for (size_t i = 1; i < str.size() - 1; ++i) {
		result += str[i] + ", ";
	  }
	  result += str.back() + ")";
	  return result;
	}

  }
  // приватные поля
  struct Names {
	string first_name;
	string last_name;
  };
  map<int, Names> full_name_;
};

int main() {
  Person person;

  person.ChangeFirstName(1965, "Polina");
  person.ChangeLastName(1967, "Sergeeva");
  for (int year : {1900, 1965, 1990}) {
	cout << person.GetFullNameWithHistory(year) << endl;
  }

  person.ChangeFirstName(1970, "Appolinaria");
  for (int year : {1969, 1970}) {
	cout << person.GetFullNameWithHistory(year) << endl;
  }

  person.ChangeLastName(1968, "Volkova");
  for (int year : {1969, 1970}) {
	cout << person.GetFullNameWithHistory(year) << endl;
  }

  person.ChangeFirstName(1990, "Polina");
  person.ChangeLastName(1990, "Volkova-Sergeeva");
  cout << person.GetFullNameWithHistory(1990) << endl;

  person.ChangeFirstName(1966, "Pauline");
  cout << person.GetFullNameWithHistory(1966) << endl;

  person.ChangeLastName(1960, "Sergeeva");
  for (int year : {1960, 1967}) {
	cout << person.GetFullNameWithHistory(year) << endl;
  }

  person.ChangeLastName(1961, "Ivanova");
  cout << person.GetFullNameWithHistory(1967) << endl;

  return 0;
}
