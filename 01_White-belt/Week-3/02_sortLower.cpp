// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <vector>
#include <algorithm>
#include <cctype>

using namespace std;

bool Myfunction (string i,string j) {
	std::transform(i.begin(), i.end(), i.begin(),
			[](unsigned char c){ return std::tolower(c); });
	std::transform(j.begin(), j.end(), j.begin(),
			[](unsigned char c){ return std::tolower(c); });
	return (i < j);
}


int main()
{
	int n;
	cin >> n;
	vector<string> v(n);
	for (auto& num: v)
	{
		cin >> num;
	}
	sort(v.begin(),  v.end(), Myfunction);
	for (const auto& num: v)
	{
		cout << num << " ";
	}

	return 0;
}
