// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <cmath>

int main() {
  double a, b, c;
  std::cin >> a >> b >> c;

  if (a != 0) {
    double d = b * b - 4 * a * c;
    if (d < 0) return 0;
    if (d == 0) {
      std::cout << (-b / (2 * a)) << std::endl;
    }
    if (d > 0) {
      std::cout << ((-b + sqrt(d)) / (2 * a)) << " " << ((-b - sqrt(d)) / (2 * a)) << std::endl;
    }
  }

  if (a == 0) {
    if (b != 0) {
      std::cout << -(c / b) << std::endl;
    }
  }

  return 0;
}
