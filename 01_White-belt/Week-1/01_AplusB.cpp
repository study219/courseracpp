// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>

int main() {
  int a,b;
  std::cin >> a >> b;
  std::cout << a + b << std::endl;
  return 0;
}
