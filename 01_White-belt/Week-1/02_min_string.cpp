// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <string>

int main() {
  std::string s1, s2, s3, min_string;
  std::cin >> s1 >> s2 >> s3;

  if (s1 < s2) {
    min_string = s1;
  } else {
    min_string = s2;
  }

  if (s3 < min_string) {
    min_string = s3;
  }

  std::cout << min_string;

  return 0;
}
