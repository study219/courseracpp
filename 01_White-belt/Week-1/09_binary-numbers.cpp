// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <vector>

int main() {
  int n;
  std::cin >> n;
  std::vector<int> result;
  while (n > 0) {
    result.push_back(n % 2);
    n /= 2;
  }

  for (int i = result.size() - 1; i >= 0; --i){
    std::cout << result[i];
  }

  return 0;
}
