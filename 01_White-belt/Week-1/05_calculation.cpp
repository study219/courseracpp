// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>

int main() {
  double n, a, b, x, y;
  std::cin >> n >> a >> b >> x >> y;
  double discount = 0;
  if (n > b) {
    discount = y;
  } else if (n > a) {
    discount = x;
  }
  std::cout << (n - (n/100 * discount));
  return 0;
}
