// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <string>

int main() {
  std::string in;
  std::cin >> in;
  int result = -2;

  for (size_t i = 0; i < in.size(); i++) {
    if (in[i] == 'f'){
      if(result == -1){
        std::cout << i;
        return 0;
      }
      result++;
    }
  }

  std::cout << result;
  return 0;
}
