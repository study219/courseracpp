// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <numeric>
using namespace std;

class Rational {
 public:
  Rational() {
	// Реализуйте конструктор по умолчанию
	numerator_ = 0;
	denominator_ = 1;
  }

  Rational(int numerator, int denominator) {
	// Реализуйте конструктор
	int nod = gcd(numerator, denominator);
	numerator_ = numerator / nod;
	denominator_ = denominator / nod;
	if (denominator_ < 0) {
	  denominator_ *= -1;
	  numerator_ *= -1;
	}

  }

  int Numerator() const {
	// Реализуйте этот метод
	return numerator_;
  }

  int Denominator() const {
	// Реализуйте этот метод
	return denominator_;
  }

  friend bool operator==(const Rational &lhs, const Rational &rhs) {
	return lhs.numerator_ == rhs.numerator_ &&
		lhs.denominator_ == rhs.denominator_;
  }
  friend bool operator!=(const Rational &lhs, const Rational &rhs) {
	return !(rhs == lhs);
  }
  friend bool operator<(const Rational &lhs, const Rational &rhs) {
	if (lhs.numerator_ < rhs.numerator_)
	  return true;
	if (rhs.numerator_ < lhs.numerator_)
	  return false;
	return lhs.denominator_ < rhs.denominator_;
  }
  friend bool operator>(const Rational &lhs, const Rational &rhs) {
	return rhs < lhs;
  }
  friend bool operator<=(const Rational &lhs, const Rational &rhs) {
	return !(rhs < lhs);
  }
  friend bool operator>=(const Rational &lhs, const Rational &rhs) {
	return !(lhs < rhs);
  }

 private:
  // Добавьте поля
  int numerator_, denominator_;
};

Rational operator+(const Rational &lhs, const Rational &rhs) {

  if (lhs.Denominator() == rhs.Denominator()) {
	return {lhs.Numerator() + rhs.Numerator(), lhs.Denominator()};
  } else {
	int nok = (lhs.Denominator() * rhs.Denominator()) / gcd(lhs.Denominator(), rhs.Denominator());
	int lhs_num = nok / lhs.Denominator() * lhs.Numerator();
	int rhs_num = nok / rhs.Denominator() * rhs.Numerator();
	return {lhs_num + rhs_num, nok};
  }

}

Rational operator-(const Rational &lhs, const Rational &rhs) {

  if (lhs.Denominator() == rhs.Denominator()) {
	return {lhs.Numerator() - rhs.Numerator(), lhs.Denominator()};
  } else {
	int nok = (lhs.Denominator() * rhs.Denominator()) / gcd(lhs.Denominator(), rhs.Denominator());
	int lhs_num = nok / lhs.Denominator() * lhs.Numerator();
	int rhs_num = nok / rhs.Denominator() * rhs.Numerator();
	return {lhs_num - rhs_num, nok};
  }

}

int main() {
  {
	Rational r1(4, 6);
	Rational r2(2, 3);
	bool equal = r1 == r2;
	if (!equal) {
	  cout << "4/6 != 2/3" << endl;
	  return 1;
	}
  }

  {
	Rational a(2, 3);
	Rational b(4, 3);
	Rational c = a + b;
	bool equal = c == Rational(2, 1);
	if (!equal) {
	  cout << "2/3 + 4/3 != 2" << endl;
	  return 2;
	}
  }

  {
	Rational a(5, 7);
	Rational b(2, 9);
	Rational c = a - b;
	bool equal = c == Rational(31, 63);
	if (!equal) {
	  cout << "5/7 - 2/9 != 31/63" << endl;
	  return 3;
	}
  }

  cout << "OK" << endl;
  return 0;
}