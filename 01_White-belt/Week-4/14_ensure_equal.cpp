// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <string>
#include <exception>

using namespace std;

void EnsureEqual(const string& left, const string& right){
  if (left != right){
	throw runtime_error(left + " != " + right);
  }
}

int main() {
  try {
	EnsureEqual("C++ White", "C++ White");
	EnsureEqual("C++ White", "C++ Yellow");
  } catch (runtime_error& e) {
	cout << e.what() << endl;
  }
  return 0;
}