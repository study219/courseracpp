// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <numeric>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <exception>

using namespace std;

class Rational {
 public:
  Rational() {
	// Реализуйте конструктор по умолчанию
	numerator_ = 0;
	denominator_ = 1;
  }

  Rational(int numerator, int denominator) {

	if (denominator == 0) {
	  throw invalid_argument("Invalid argument");
	}
	// Реализуйте конструктор
	int nod = gcd(numerator, denominator);
	numerator_ = numerator / nod;
	denominator_ = denominator / nod;
	if (denominator_ < 0) {
	  denominator_ *= -1;
	  numerator_ *= -1;
	}

  }

  int Numerator() const {
	// Реализуйте этот метод
	return numerator_;
  }

  int Denominator() const {
	// Реализуйте этот метод
	return denominator_;
  }

  friend bool operator==(const Rational &lhs, const Rational &rhs) {
	return lhs.numerator_ == rhs.numerator_ &&
		lhs.denominator_ == rhs.denominator_;
  }
  friend bool operator!=(const Rational &lhs, const Rational &rhs) {
	return !(rhs == lhs);
  }
  friend bool operator<(const Rational &lhs, const Rational &rhs) {
	return lhs.Numerator() / (double)lhs.Denominator() < rhs.Numerator() / (double)rhs.Denominator();

  }
  friend bool operator>(const Rational &lhs, const Rational &rhs) {
	return rhs < lhs;
  }
  friend bool operator<=(const Rational &lhs, const Rational &rhs) {
	return !(rhs < lhs);
  }
  friend bool operator>=(const Rational &lhs, const Rational &rhs) {
	return !(lhs < rhs);
  }
  friend ostream &operator<<(ostream &os, const Rational &rational) {
	os << rational.numerator_ << "/" << rational.denominator_;
	return os;
  }
  friend istream &operator>>(istream &os, Rational &rational) {

	int numerator, denominator;
	char c;
	if (os >> numerator && os >> c && os >> denominator && c == '/') {
	  rational = Rational(numerator, denominator);
	}

	return os;
  }

 private:
  // Добавьте поля
  int numerator_, denominator_;
};

Rational operator+(const Rational &lhs, const Rational &rhs) {

  if (lhs.Denominator() == rhs.Denominator()) {
	return {lhs.Numerator() + rhs.Numerator(), lhs.Denominator()};
  } else {
	int nok = (lhs.Denominator() * rhs.Denominator()) / gcd(lhs.Denominator(), rhs.Denominator());
	int lhs_num = nok / lhs.Denominator() * lhs.Numerator();
	int rhs_num = nok / rhs.Denominator() * rhs.Numerator();
	return {lhs_num + rhs_num, nok};
  }

}

Rational operator-(const Rational &lhs, const Rational &rhs) {

  if (lhs.Denominator() == rhs.Denominator()) {
	return {lhs.Numerator() - rhs.Numerator(), lhs.Denominator()};
  } else {
	int nok = (lhs.Denominator() * rhs.Denominator()) / gcd(lhs.Denominator(), rhs.Denominator());
	int lhs_num = nok / lhs.Denominator() * lhs.Numerator();
	int rhs_num = nok / rhs.Denominator() * rhs.Numerator();
	return {lhs_num - rhs_num, nok};
  }

}

Rational operator*(const Rational &lhs, const Rational &rhs) {
  return {lhs.Numerator() * rhs.Numerator(), lhs.Denominator() * rhs.Denominator()};
}

Rational operator/(const Rational &lhs, const Rational &rhs) {
  if (rhs.Numerator() == 0) {
	throw domain_error("Division by zero");
  }
  return {lhs.Numerator() * rhs.Denominator(), lhs.Denominator() * rhs.Numerator()};
}

int main() {

  try {
	Rational a, b;
	char op;
	cin >> a >> op >> b;

	switch (op) {
	  case '+': cout << (a + b) << endl;
		break;
	  case '-': cout << (a - b) << endl;
		break;
	  case '*': cout << (a * b) << endl;
		break;
	  case '/': cout << (a / b) << endl;
		break;
	  default:
		break;
	}
  } catch (exception &ex) {
	cout << ex.what() << endl;
  }

  return 0;
}