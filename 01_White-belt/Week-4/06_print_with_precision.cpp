// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <vector>

using namespace std;

int main() {
  ifstream input("input.txt");

  double a;

  cout << fixed << setprecision(3);

  while (input >> a) {
	cout << a << endl;
  }

  return 0;
}