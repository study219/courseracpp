// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>

using namespace std;
/*
struct Image {
  double quality;
  double freshness;
  double rating;
};

struct Params {
  double a;
  double b;
  double c;
};*/

class FunctionPart {
 public:
  FunctionPart(char new_operation, double new_value) {
	operation_ = new_operation;
	value_ = new_value;
  }
  double Apply(double source_value) const {
	if (operation_ == '+') {
	  return source_value + value_;
	} else if (operation_ == '-') {
	  return source_value - value_;
	} else if (operation_ == '*') {
	  return source_value * value_;
	} else {
	  return source_value / value_;
	}

  }
  void Invert() {
	if (operation_ == '+') {
	  operation_ = '-';
	} else if (operation_ == '-'){
	  operation_ = '+';
	} else if (operation_ == '*'){
	  operation_ = '/';
	} else if (operation_ == '/') {
	  operation_ = '*';
	}
  }
 private:
  char operation_;
  double value_;
};

class Function {
 public:
  void AddPart(char operation, double value) {
	parts_.emplace_back(operation, value);
  }
  double Apply(double value) const {
	for (const FunctionPart &part: parts_) {
	  value = part.Apply(value);
	}
	return value;
  }
  void Invert() {
	for (FunctionPart &part: parts_) {
	  part.Invert();
	}
	reverse(begin(parts_), end(parts_));
  }
 private:
  vector<FunctionPart> parts_;
};
/*
Function MakeWeightFunction(const Params &params,
							const Image &image) {
  Function function;
  function.AddPart('*', params.a);
  function.AddPart('-', image.freshness * params.a + params.b);
  function.AddPart('+', image.rating * params.c);
  return function;
}

double ComputeImageWeight(const Params &params, const Image &image) {
  Function function = MakeWeightFunction(params, image);
  return function.Apply(image.quality);
}

double ComputeQualityByWeight(const Params &params,
							  const Image &image,
							  double weight) {
  Function function = MakeWeightFunction(params, image);
  function.Invert();
  return function.Apply(weight);
}

int main() {
  Image image = {10, 2, 6};
  Params params = {4, 2, 6};
  cout << ComputeImageWeight(params, image) << endl;
  cout << ComputeQualityByWeight(params, image, 52) << endl;
  return 0;
}*/