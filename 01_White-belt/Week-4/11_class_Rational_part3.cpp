// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <numeric>
using namespace std;

class Rational {
 public:
  Rational() {
	// Реализуйте конструктор по умолчанию
	numerator_ = 0;
	denominator_ = 1;
  }

  Rational(int numerator, int denominator) {
	// Реализуйте конструктор
	int nod = gcd(numerator, denominator);
	numerator_ = numerator / nod;
	denominator_ = denominator / nod;
	if (denominator_ < 0) {
	  denominator_ *= -1;
	  numerator_ *= -1;
	}

  }

  int Numerator() const {
	// Реализуйте этот метод
	return numerator_;
  }

  int Denominator() const {
	// Реализуйте этот метод
	return denominator_;
  }

  friend bool operator==(const Rational &lhs, const Rational &rhs) {
	return lhs.numerator_ == rhs.numerator_ &&
		lhs.denominator_ == rhs.denominator_;
  }
  friend bool operator!=(const Rational &lhs, const Rational &rhs) {
	return !(rhs == lhs);
  }
  friend bool operator<(const Rational &lhs, const Rational &rhs) {
	if (lhs.numerator_ < rhs.numerator_)
	  return true;
	if (rhs.numerator_ < lhs.numerator_)
	  return false;
	return lhs.denominator_ < rhs.denominator_;
  }
  friend bool operator>(const Rational &lhs, const Rational &rhs) {
	return rhs < lhs;
  }
  friend bool operator<=(const Rational &lhs, const Rational &rhs) {
	return !(rhs < lhs);
  }
  friend bool operator>=(const Rational &lhs, const Rational &rhs) {
	return !(lhs < rhs);
  }

 private:
  // Добавьте поля
  int numerator_, denominator_;
};

Rational operator+(const Rational &lhs, const Rational &rhs) {

  if (lhs.Denominator() == rhs.Denominator()) {
	return {lhs.Numerator() + rhs.Numerator(), lhs.Denominator()};
  } else {
	int nok = (lhs.Denominator() * rhs.Denominator()) / gcd(lhs.Denominator(), rhs.Denominator());
	int lhs_num = nok / lhs.Denominator() * lhs.Numerator();
	int rhs_num = nok / rhs.Denominator() * rhs.Numerator();
	return {lhs_num + rhs_num, nok};
  }

}

Rational operator-(const Rational &lhs, const Rational &rhs) {

  if (lhs.Denominator() == rhs.Denominator()) {
	return {lhs.Numerator() - rhs.Numerator(), lhs.Denominator()};
  } else {
	int nok = (lhs.Denominator() * rhs.Denominator()) / gcd(lhs.Denominator(), rhs.Denominator());
	int lhs_num = nok / lhs.Denominator() * lhs.Numerator();
	int rhs_num = nok / rhs.Denominator() * rhs.Numerator();
	return {lhs_num - rhs_num, nok};
  }

}

Rational operator*(const Rational &lhs, const Rational &rhs) {
  return {lhs.Numerator() * rhs.Numerator(), lhs.Denominator() * rhs.Denominator()};
}

Rational operator/(const Rational &lhs, const Rational &rhs) {
  return {lhs.Numerator() * rhs.Denominator(), lhs.Denominator() * rhs.Numerator()};
}

int main() {
  {
	Rational a(2, 3);
	Rational b(4, 3);
	Rational c = a * b;
	bool equal = c == Rational(8, 9);
	if (!equal) {
	  cout << "2/3 * 4/3 != 8/9" << endl;
	  return 1;
	}
  }

  {
	Rational a(5, 4);
	Rational b(15, 8);
	Rational c = a / b;
	bool equal = c == Rational(2, 3);
	if (!equal) {
	  cout << "5/4 / 15/8 != 2/3" << endl;
	  return 2;
	}
  }

  cout << "OK" << endl;
  return 0;
}