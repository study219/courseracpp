// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <vector>

using namespace std;

int main() {
  ifstream input("input.txt");
  int n, m, tmp;
  input >> n >> m;
  vector<vector<int>> table(n);
  for (int i = 0; i < n; ++i) {
	for (int j = 0; j < m; ++j) {
	  if (j != m - 1) {
		input >> tmp;
		input.ignore(1);
		cout << setw(10) << tmp << " ";
	  } else {
		input >> tmp;
		input.ignore(1);
		cout << setw(10) << tmp;
	  }

	}
	cout << endl;
  }




  return 0;
}