// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <numeric>
using namespace std;

class Rational {
 public:
  Rational() {
	// Реализуйте конструктор по умолчанию
	numerator_ = 0;
	denominator_ = 1;
  }

  Rational(int numerator, int denominator) {
	// Реализуйте конструктор
	int nod = gcd(numerator, denominator);
	numerator_ = numerator / nod;
	denominator_ = denominator / nod;
	if (denominator_ < 0 ) {
		denominator_*=-1;
		numerator_*=-1;
	}

  }

  int Numerator() const {
	// Реализуйте этот метод
	return numerator_;
  }

  int Denominator() const {
	// Реализуйте этот метод
	return denominator_;
  }

 private:
  // Добавьте поля
  int numerator_, denominator_;
};

int main() {
  {
	const Rational r(3, 10);
	if (r.Numerator() != 3 || r.Denominator() != 10) {
	  cout << "Rational(3, 10) != 3/10" << endl;
	  return 1;
	}
  }

  {
	const Rational r(8, 12);
	if (r.Numerator() != 2 || r.Denominator() != 3) {
	  cout << "Rational(8, 12) != 2/3" << endl;
	  return 2;
	}
  }

  {
	const Rational r(-4, 6);
	if (r.Numerator() != -2 || r.Denominator() != 3) {
	  cout << "Rational(-4, 6) != -2/3" << endl;
	  return 3;
	}
  }

  {
	const Rational r(-4, -6);
	if (r.Numerator() != 2 || r.Denominator() != 3) {
	  cout << "Rational(-4, -6) != 2/3" << endl;
	  return 6;
	}
  }

  {
	const Rational r(4, -6);
	if (r.Numerator() != -2 || r.Denominator() != 3) {
	  cout << "Rational(4, -6) != -2/3" << endl;
	  return 3;
	}
  }

  {
	const Rational r(0, 15);
	if (r.Numerator() != 0 || r.Denominator() != 1) {
	  cout << "Rational(0, 15) != 0/1" << endl;
	  return 4;
	}
  }

  {
	const Rational defaultConstructed;
	if (defaultConstructed.Numerator() != 0 || defaultConstructed.Denominator() != 1) {
	  cout << "Rational() != 0/1" << endl;
	  return 5;
	}
  }

  cout << "OK" << endl;
  return 0;
}
