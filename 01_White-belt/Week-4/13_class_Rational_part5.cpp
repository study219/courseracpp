// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <numeric>
#include <sstream>
#include <vector>
#include <map>
#include <set>

using namespace std;

class Rational {
 public:
  Rational() {
	// Реализуйте конструктор по умолчанию
	numerator_ = 0;
	denominator_ = 1;
  }

  Rational(int numerator, int denominator) {
	// Реализуйте конструктор
	int nod = gcd(numerator, denominator);
	numerator_ = numerator / nod;
	denominator_ = denominator / nod;
	if (denominator_ < 0) {
	  denominator_ *= -1;
	  numerator_ *= -1;
	}

  }

  int Numerator() const {
	// Реализуйте этот метод
	return numerator_;
  }

  int Denominator() const {
	// Реализуйте этот метод
	return denominator_;
  }

  friend bool operator==(const Rational &lhs, const Rational &rhs) {
	return lhs.numerator_ == rhs.numerator_ &&
		lhs.denominator_ == rhs.denominator_;
  }
  friend bool operator!=(const Rational &lhs, const Rational &rhs) {
	return !(rhs == lhs);
  }
  friend bool operator<(const Rational &lhs, const Rational &rhs) {
	return lhs.Numerator() / (double)lhs.Denominator() < rhs.Numerator() / (double)rhs.Denominator();

  }
  friend bool operator>(const Rational &lhs, const Rational &rhs) {
	return rhs < lhs;
  }
  friend bool operator<=(const Rational &lhs, const Rational &rhs) {
	return !(rhs < lhs);
  }
  friend bool operator>=(const Rational &lhs, const Rational &rhs) {
	return !(lhs < rhs);
  }
  friend ostream &operator<<(ostream &os, const Rational &rational) {
	os << rational.numerator_ << "/" << rational.denominator_;
	return os;
  }
  friend istream &operator>>(istream &os, Rational &rational) {

	int numerator, denominator;
	char c;
	if (os >> numerator && os >> c && os >> denominator && c == '/') {
	  rational = Rational(numerator, denominator);
	}

	return os;
  }

 private:
  // Добавьте поля
  int numerator_, denominator_;
};

Rational operator+(const Rational &lhs, const Rational &rhs) {

  if (lhs.Denominator() == rhs.Denominator()) {
	return {lhs.Numerator() + rhs.Numerator(), lhs.Denominator()};
  } else {
	int nok = (lhs.Denominator() * rhs.Denominator()) / gcd(lhs.Denominator(), rhs.Denominator());
	int lhs_num = nok / lhs.Denominator() * lhs.Numerator();
	int rhs_num = nok / rhs.Denominator() * rhs.Numerator();
	return {lhs_num + rhs_num, nok};
  }

}

Rational operator-(const Rational &lhs, const Rational &rhs) {

  if (lhs.Denominator() == rhs.Denominator()) {
	return {lhs.Numerator() - rhs.Numerator(), lhs.Denominator()};
  } else {
	int nok = (lhs.Denominator() * rhs.Denominator()) / gcd(lhs.Denominator(), rhs.Denominator());
	int lhs_num = nok / lhs.Denominator() * lhs.Numerator();
	int rhs_num = nok / rhs.Denominator() * rhs.Numerator();
	return {lhs_num - rhs_num, nok};
  }

}

Rational operator*(const Rational &lhs, const Rational &rhs) {
  return {lhs.Numerator() * rhs.Numerator(), lhs.Denominator() * rhs.Denominator()};
}

Rational operator/(const Rational &lhs, const Rational &rhs) {
  return {lhs.Numerator() * rhs.Denominator(), lhs.Denominator() * rhs.Numerator()};
}

int main() {
  {
	const set<Rational> rs = {{1, 2}, {1, 25}, {3, 4}, {3, 4}, {1, 2}};
	if (rs.size() != 3) {
	  cout << "Wrong amount of items in the set" << endl;
	  return 1;
	}

	vector<Rational> v;
	for (auto x : rs) {
	  v.push_back(x);
	}
	if (v != vector<Rational>{{1, 25}, {1, 2}, {3, 4}}) {
	  cout << "Rationals comparison works incorrectly" << endl;
	  return 2;
	}
  }

  {
	map<Rational, int> count;
	++count[{1, 2}];
	++count[{1, 2}];

	++count[{2, 3}];

	if (count.size() != 2) {
	  cout << "Wrong amount of items in the map" << endl;
	  return 3;
	}
  }

  cout << "OK" << endl;
  return 0;
}
