// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main() {
  ifstream input("input.txt");
  ofstream output("output.txt");
  string line;
  if (input) {
	while (getline(input, line)) {
	  output << line << endl;
	}
  }

  return 0;
}