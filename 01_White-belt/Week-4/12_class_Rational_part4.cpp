// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <numeric>
#include <sstream>
using namespace std;

class Rational {
 public:
  Rational() {
	// Реализуйте конструктор по умолчанию
	numerator_ = 0;
	denominator_ = 1;
  }

  Rational(int numerator, int denominator) {
	// Реализуйте конструктор
	int nod = gcd(numerator, denominator);
	numerator_ = numerator / nod;
	denominator_ = denominator / nod;
	if (denominator_ < 0) {
	  denominator_ *= -1;
	  numerator_ *= -1;
	}

  }

  int Numerator() const {
	// Реализуйте этот метод
	return numerator_;
  }

  int Denominator() const {
	// Реализуйте этот метод
	return denominator_;
  }

  friend bool operator==(const Rational &lhs, const Rational &rhs) {
	return lhs.numerator_ == rhs.numerator_ &&
		lhs.denominator_ == rhs.denominator_;
  }
  friend bool operator!=(const Rational &lhs, const Rational &rhs) {
	return !(rhs == lhs);
  }
  friend bool operator<(const Rational &lhs, const Rational &rhs) {
	if (lhs.numerator_ < rhs.numerator_)
	  return true;
	if (rhs.numerator_ < lhs.numerator_)
	  return false;
	return lhs.denominator_ < rhs.denominator_;
  }
  friend bool operator>(const Rational &lhs, const Rational &rhs) {
	return rhs < lhs;
  }
  friend bool operator<=(const Rational &lhs, const Rational &rhs) {
	return !(rhs < lhs);
  }
  friend bool operator>=(const Rational &lhs, const Rational &rhs) {
	return !(lhs < rhs);
  }
  friend ostream &operator<<(ostream &os, const Rational &rational) {
	os << rational.numerator_ << "/" << rational.denominator_;
	return os;
  }
  friend istream &operator>>(istream &os, Rational &rational) {

	int numerator, denominator;
	char c;
	if (os >> numerator && os >> c && os >> denominator && c == '/') {
	  rational = Rational(numerator, denominator);
	}

	return os;
  }

 private:
  // Добавьте поля
  int numerator_, denominator_;
};

Rational operator+(const Rational &lhs, const Rational &rhs) {

  if (lhs.Denominator() == rhs.Denominator()) {
	return {lhs.Numerator() + rhs.Numerator(), lhs.Denominator()};
  } else {
	int nok = (lhs.Denominator() * rhs.Denominator()) / gcd(lhs.Denominator(), rhs.Denominator());
	int lhs_num = nok / lhs.Denominator() * lhs.Numerator();
	int rhs_num = nok / rhs.Denominator() * rhs.Numerator();
	return {lhs_num + rhs_num, nok};
  }

}

Rational operator-(const Rational &lhs, const Rational &rhs) {

  if (lhs.Denominator() == rhs.Denominator()) {
	return {lhs.Numerator() - rhs.Numerator(), lhs.Denominator()};
  } else {
	int nok = (lhs.Denominator() * rhs.Denominator()) / gcd(lhs.Denominator(), rhs.Denominator());
	int lhs_num = nok / lhs.Denominator() * lhs.Numerator();
	int rhs_num = nok / rhs.Denominator() * rhs.Numerator();
	return {lhs_num - rhs_num, nok};
  }

}

Rational operator*(const Rational &lhs, const Rational &rhs) {
  return {lhs.Numerator() * rhs.Numerator(), lhs.Denominator() * rhs.Denominator()};
}

Rational operator/(const Rational &lhs, const Rational &rhs) {
  return {lhs.Numerator() * rhs.Denominator(), lhs.Denominator() * rhs.Numerator()};
}

int main() {
  {
	ostringstream output;
	output << Rational(-6, 8);
	if (output.str() != "-3/4") {
	  cout << "Rational(-6, 8) should be written as \"-3/4\"" << endl;
	  return 1;
	}
  }

  {
	istringstream input("5/7");
	Rational r;
	input >> r;
	bool equal = r == Rational(5, 7);
	if (!equal) {
	  cout << "5/7 is incorrectly read as " << r << endl;
	  return 2;
	}
  }

  {
	istringstream input("");
	Rational r;
	bool correct = !(input >> r);
	if (!correct) {
	  cout << "Read from empty stream works incorrectly" << endl;
	  return 3;
	}
  }

  {
	istringstream input("5/7 10/8");
	Rational r1, r2;
	input >> r1 >> r2;
	bool correct = r1 == Rational(5, 7) && r2 == Rational(5, 4);
	if (!correct) {
	  cout << "Multiple values are read incorrectly: " << r1 << " " << r2 << endl;
	  return 4;
	}

	input >> r1;
	input >> r2;
	correct = r1 == Rational(5, 7) && r2 == Rational(5, 4);
	if (!correct) {
	  cout << "Read from empty stream shouldn't change arguments: " << r1 << " " << r2 << endl;
	  return 5;
	}
  }

  {
	istringstream input1("1*2"), input2("1/"), input3("/4");
	Rational r1, r2, r3;
	input1 >> r1;
	input2 >> r2;
	input3 >> r3;
	bool correct = r1 == Rational() && r2 == Rational() && r3 == Rational();
	if (!correct) {
	  cout << "Reading of incorrectly formatted rationals shouldn't change arguments: "
		   << r1 << " " << r2 << " " << r3 << endl;

	  return 6;
	}
  }

  cout << "OK" << endl;
  return 0;
}