// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <string>
#include <iomanip>
#include <vector>

using namespace std;

struct Birthday {
  int day;
  int month;
  int year;
};

struct Student {
  string first_name;
  string last_name;
  Birthday birthday;
};

int main() {

  int n,m;
  cin >> n;
  vector<Student> students(n);
  for (auto &student: students) {
	cin >> student.first_name >> student.last_name >> student.birthday.day >> student.birthday.month
		>> student.birthday.year;
  }

  cin >> m;
  while (m) {
	int q;
	string command;
	cin >> command >> q;

	if(q > n || q < 1) {
	  cout << "bad request" << endl;
	} else {
	  --q;
	  if (command == "name") {
		cout << students[q].first_name << " " << students[q].last_name << endl;
	  } else if (command == "date") {
		cout << students[q].birthday.day << "." << students[q].birthday.month
			 << "." << students[q].birthday.year << endl;
	  } else {
		cout << "bad request" << endl;
	  }
	}

	--m;
  }

  return 0;
}