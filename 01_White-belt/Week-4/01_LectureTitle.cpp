// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <utility>
#include <algorithm>


using namespace std;

struct Specialization {
  explicit Specialization(string value) : value(std::move(value)) {}
  string value;
};

struct Course {
  explicit Course(string value) : value(std::move(value)) {}
  string value;
};

struct Week {
  explicit Week(string value) : value(std::move(value)) {}
  string value;
};


struct LectureTitle {
  LectureTitle(const Specialization& new_specialization, const Course& new_course, const Week& new_week){
	specialization = new_specialization.value;
	course = new_course.value;
	week = new_week.value;
  }

  string specialization;
  string course;
  string week;

};

int main() {
  LectureTitle title(
	  Specialization("C++"),
	  Course("White belt"),
	  Week("4th")
  );



  return 0;
}