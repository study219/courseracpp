// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	vector<bool> line;
	int q;
	cin >> q;
	string command;
	int n;
	while (q)
	{
		cin >> command;
		if (command != "WORRY_COUNT")
		{
			cin >> n;
			if (command == "COME")
			{
				if (n > 0)
				{
					line.insert(line.end(), n, false);

				}
				else
				{
					line.resize((line.size() + n));
				}

			}
			if (command == "WORRY")
			{
				line[n] = true;
			}
			if (command == "QUIET")
			{
				line[n] = false;
			}

		}
		else
		{
			cout << count(line.begin(), line.end(), true) << endl;
		}
		q--;
	}

	return 0;
}
