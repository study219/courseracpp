// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <string>
#include <vector>

using namespace std;

void MoveStrings(vector<string>& source, vector<string>& destination)
{
	destination.insert(destination.end(), source.begin(), source.end());
	source.clear();
}

int main()
{

	return 0;
}
