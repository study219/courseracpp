// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>

int Factorial(int f) {
  if (f < 2) {
    return 1;
  }
  int result = 1;
  while (f) {
    result *= f;
    --f;
  }
  return result;
}

int main() {


  return 0;
}
