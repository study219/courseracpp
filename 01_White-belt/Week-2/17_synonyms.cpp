// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <string>
#include <set>
#include <map>

using namespace std;

int main()
{
	map<string, set<string>> synonyms;

	int q;
	cin >> q;
	while (q)
	{
		string command;
		cin >> command;
		if (command == "ADD")
		{
			string word1, word2;
			cin >> word1 >> word2;
			synonyms[word1].insert(word2);
			synonyms[word2].insert(word1);
		}
		else if (command == "CHECK")
		{
			string word1, word2;
			cin >> word1 >> word2;
			if (synonyms.count(word1) && synonyms.count(word2))
			{
				if (synonyms.at(word1).count(word2))
				{
					cout << "YES" << endl;
				}
				else
				{
					cout << "NO" << endl;
				}
			}
			else
			{
				cout << "NO" << endl;
			}

		}
		else if (command == "COUNT")
		{
			string word;
			cin >> word;
			if (synonyms.count(word))
			{
				cout << synonyms.at(word).size() << endl;
			}
			else
			{
				cout << 0 << endl;
			}

		}

		--q;
	}

	return 0;
}
