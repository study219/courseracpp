// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <string>
#include <vector>
#include <map>

using namespace std;

map<char, int> BuildCharCounters(const string& word)
{
	map<char, int> result;
	for (const auto& w: word)
	{
		result[w]++;
	}
	return result;
}

int main()
{
	int n;
	cin >> n;


	string word1, word2;
	while (n)
	{
		cin >> word1 >> word2;
		if (BuildCharCounters(word1) == BuildCharCounters(word2))
		{
			cout << "YES" << endl;
		}
		else
		{
			cout << "NO" << endl;
		}

		n--;
	}

	return 0;
}
