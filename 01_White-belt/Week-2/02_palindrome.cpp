// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <string>

using namespace std;

bool IsPalindrom(string str){
  if (str.size() <= 1){
    return true;
  }
  for (int i = str.size() - 1, j = 0; j < str.size()/2; --i, ++j) {
    if (str[i] != str[j]){
      return false;
    }
  }
  return true;
}

int main() {
  cout << IsPalindrom("madam") << endl;
  cout << IsPalindrom("gentleman") << endl;
  cout << IsPalindrom("genasdtleman") << endl;
  cout << IsPalindrom("x") << endl;
  cout << IsPalindrom("xx") << endl;
  cout << IsPalindrom("xxx") << endl;

  return 0;
}
