// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

int MaxNum(int nums)
{
	vector<int> digits;
	int result = 0;
	while (nums)
	{
		digits.push_back(nums % 10);
		nums = nums / 10;
	}
	sort(digits.begin(), digits.end());
	reverse(digits.begin(), digits.end());

	int numsCount = digits.size() - 1;
	for (int digit: digits)
	{
		result += +digit * pow(10, numsCount);
		numsCount--;
	}


	return result;
}


int main()
{
	int n;
	vector<int> nums;
	cin >> n;
	while (n > 0)
	{
		int num;
		cin >> num;
		nums.push_back(num);
		--n;
	}

	for (int num : nums)
	{
		cout << MaxNum(num) << " ";
	}

	return 0;
}
