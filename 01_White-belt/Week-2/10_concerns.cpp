// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

void Add(vector<string>& currentMonthWork, string& work)
{
	currentMonthWork.push_back(work);
}

void Dump(vector<string>& currentMonthWork)
{
	cout << currentMonthWork.size() << " ";
	for (auto& s: currentMonthWork)
	{
		cout << s << " ";
	}
	cout << endl;
}

vector<int> daysInMonth = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

void Next(int& currentMonth, vector<vector<string>>& currentMonthWork)
{
	int nextMonthDays = (currentMonth + 1) % 12;

	if (daysInMonth[currentMonth] < daysInMonth[nextMonthDays])
	{
		currentMonthWork.resize(daysInMonth[nextMonthDays]);
	}
	else if (daysInMonth[currentMonth] > daysInMonth[nextMonthDays])
	{
		vector<vector<string>> currentMonthWorkCopy = currentMonthWork;
		currentMonthWork.resize(daysInMonth[nextMonthDays]);
		for (int i = daysInMonth[nextMonthDays]; i < daysInMonth[currentMonth]; ++i)
		{
			if (!currentMonthWorkCopy[i].empty())
			{
				currentMonthWork[daysInMonth[nextMonthDays] - 1].insert(
						end(currentMonthWork[daysInMonth[nextMonthDays] - 1]),
						begin(currentMonthWorkCopy[i]),
						end(currentMonthWorkCopy[i]));
			}
		}
	}
	currentMonth++;
	currentMonth %= 12;
}

int main()
{

	int currentMonth = 0;
	vector<vector<string>> currentMonthWork(daysInMonth[currentMonth]);

	int q;
	cin >> q;

	string command;

	while (q)
	{
		cin >> command;
		if (command == "NEXT")
		{
			Next(currentMonth, currentMonthWork);
		}
		else
		{
			int day;
			cin >> day;
			--day;

			if (command == "ADD")
			{
				string work;
				cin >> work;
				Add(currentMonthWork[day], work);
			}
			else if (command == "DUMP")
			{
				Dump(currentMonthWork[day]);
			}
		}
		q--;
	}


	return 0;
}
