// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <vector>
#include <map>
#include <set>

using namespace std;



int main()
{
	int q;
	map<set<string>, int> stops;
	set<string> stopsNames;
	cin >> q;
	while (q){
		int n;
		cin >> n;
		while (n){
			string stopName;
			cin >> stopName;
			stopsNames.insert(stopName);
			--n;
		}
		int busNum = stops.size() + 1;
		if (!stops.count(stopsNames)){
			stops[stopsNames] = busNum;
			cout << "New bus " << busNum << endl;
		}
		else {
			cout << "Already exists for " << stops[stopsNames] << endl;
		}
		stopsNames.clear();

		--q;
	}

	return 0;
}
