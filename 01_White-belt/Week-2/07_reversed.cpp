// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <string>
#include <vector>

using namespace std;
vector<int> Reversed(vector<int> v){
	vector<int> temp = v;
	for (size_t i = 0; i < temp.size() ; ++i)
	{
		v[i] = temp[temp.size() - i - 1];
	}
	return v;
}

int main()
{

	return 0;
}
