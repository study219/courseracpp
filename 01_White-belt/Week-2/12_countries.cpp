// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <string>
#include <vector>
#include <map>

using namespace std;

void ChangeCapital(map<string, string>& countryToCapital, const string& country, const string& newCapital)
{
	if (!countryToCapital.count(country))
	{
		countryToCapital[country] = newCapital;
		cout << "Introduce new country " << country << " with capital " << newCapital << endl;
	}
	else if (countryToCapital[country] == newCapital)
	{
		cout << "Country " << country << " hasn't changed its capital" << endl;
	}
	else
	{
		cout << "Country " << country << " has changed its capital from " << countryToCapital[country] << " to "
			 << newCapital << endl;
		countryToCapital[country] = newCapital;
	}
}

void Rename(map<string, string>& countryToCapital, const string& oldCountryName, const string& newCountryName)
{
	if (oldCountryName == newCountryName || !countryToCapital.count(oldCountryName) ||
		countryToCapital.count(newCountryName))
	{
		cout << "Incorrect rename, skip" << endl;
	}
	else
	{
		countryToCapital[newCountryName] = countryToCapital[oldCountryName];
		countryToCapital.erase(oldCountryName);
		cout << "Country " << oldCountryName << " with capital " << countryToCapital.at(newCountryName)
			 << " has been renamed to " << newCountryName << endl;
	}

}

void About(const map<string, string>& countryToCapital, const string& country)
{
	if (countryToCapital.count(country))
	{
		cout << "Country " << country << " has capital " << countryToCapital.at(country) << endl;
	}
	else
	{
		cout << "Country " << country << " doesn't exist" << endl;
	}
}

void Dump(const map<string, string>& countryToCapital)
{
	if (countryToCapital.empty())
	{
		cout << "There are no countries in the world" << endl;
	}
	else
	{
		for (const auto& [country, capital]: countryToCapital)
		{
			cout << country << "/" << capital << " ";
		}
	}
	cout << endl;
}

int main()
{
	map<string, string> countryToCapital;

	/*Rename(countryToCapital, "FakeCountry", "FarFarAway");
	About(countryToCapital, "FarFarAway");
	Dump(countryToCapital);
	ChangeCapital(countryToCapital, "TsardomOfRussia", "Moscow");
	ChangeCapital(countryToCapital, "TsardomOfRussia", "Moscow");
	ChangeCapital(countryToCapital, "ColonialBrazil", "Salvador");
	ChangeCapital(countryToCapital, "TsardomOfRussia", "SaintPetersburg");
	Rename(countryToCapital, "TsardomOfRussia", "RussianEmpire");
	ChangeCapital(countryToCapital, "RussianEmpire", "Moscow");
	ChangeCapital(countryToCapital, "RussianEmpire", "SaintPetersburg");
	ChangeCapital(countryToCapital, "ColonialBrazil", "RioDeJaneiro");
	Dump(countryToCapital);*/
	int q;
	cin >> q;
	string command;
	while (q)
	{
		cin >> command;
		if (command == "DUMP")
		{
			Dump(countryToCapital);
		}
		else if (command == "ABOUT")
		{
			string country;
			cin >> country;
			About(countryToCapital, country);
		}
		else if (command == "RENAME")
		{
			string oldCountryName, newCountryName;
			cin >> oldCountryName >> newCountryName;
			Rename(countryToCapital, oldCountryName, newCountryName);
		}
		else if (command == "CHANGE_CAPITAL")
		{
			string country, newCapital;
			cin >> country >> newCapital;
			ChangeCapital(countryToCapital, country, newCapital);
		}
		--q;
	}


	return 0;
}
