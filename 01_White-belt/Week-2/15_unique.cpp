// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <string>
#include <set>

using namespace std;


int main()
{
	int n;
	cin >> n;
	set<string> unique;
	while (n){
		string str;
		cin >> str;
		unique.insert(str);
		--n;
	}
	cout << unique.size() << endl;

	return 0;
}
