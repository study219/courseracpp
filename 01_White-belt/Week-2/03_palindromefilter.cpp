// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <string>
#include <vector>

using namespace std;

bool IsPalindrom(string str){
  if (str.size() <= 1){
    return true;
  }
  for (int i = str.size() - 1, j = 0; j < str.size()/2; --i, ++j) {
    if (str[i] != str[j]){
      return false;
    }
  }
  return true;
}

vector<string> PalindromFilter (const vector<string>& words,  int minLength){
  vector<string> result;
  for (const auto& w: words) {
    if (w.size() >= minLength){
      if (IsPalindrom(w)){
        result.push_back(w);
      }
    }
  }
  return result;
}

void PrintResult(const vector<string>& words){
  for (const auto& w: words) {
    cout << w << ", ";
  }
  cout << endl;
}

int main() {
  PrintResult(PalindromFilter({"abacaba", "aba"},5));
  PrintResult(PalindromFilter({"abacaba", "aba"},2));
  PrintResult(PalindromFilter({"weew", "bro", "code"},4));

  return 0;
}
