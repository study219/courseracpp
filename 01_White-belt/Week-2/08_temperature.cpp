// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main()
{
	int days, tempSum = 0;
	cin >> days;
	vector<int> temperatures(days);
	for (auto& d: temperatures)
	{
		cin >> d;
		tempSum += d;
	}
	int midTemp = tempSum / days;
	vector<int> tempDays;
	for (int i = 0; i < temperatures.size(); ++i)
	{
		if (temperatures[i] > midTemp)
		{
			tempDays.push_back(i);
		}
	}

	cout << tempDays.size() << endl;
	for (auto& d: tempDays)
	{
		cout << d << " ";
	}

	return 0;
}
