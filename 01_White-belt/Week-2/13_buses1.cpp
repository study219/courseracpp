// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <string>
#include <vector>
#include <map>

using namespace std;

void NewBus(map<string, vector<string>>& busesToStops, map<string, vector<string>>& stopsToBuses, const string& bus,
		const vector<string>& stops)
{
	busesToStops[bus] = stops;
	for (const auto& s: stops)
	{
		stopsToBuses[s].push_back(bus);
	}
}

void StopsForBus(const map<string, vector<string>>& busesToStops, const map<string, vector<string>>& stopsToBuses,
		const string& bus)
{
	if (!busesToStops.count(bus))
	{
		cout << "No bus" << endl;
	}
	else
	{
		const vector<string>& stops = busesToStops.at(bus);
		vector<string> result;
		for (const auto& s: stops)
		{
			cout << "Stop " << s << ":";
			for (const auto& b: stopsToBuses.at(s))
			{
				if (b != bus)
				{
					result.push_back(b);
				}
			}
			if (result.empty())
			{
				cout << " no interchange";
			}
			else
			{
				for (const auto& r: result)
				{
					cout << " " << r;
				}
			}
			cout << endl;
			result.clear();
		}
	}
}

void BusesForStop(const map<string, vector<string>>& stopsToBuses, const string& stop)
{
	if (!stopsToBuses.count(stop))
	{
		cout << "No stop" << endl;
	}
	else
	{
		for (const auto& bus: stopsToBuses.at(stop))
		{
			cout << bus << " ";
		}
		cout << endl;
	}
}

void AllBuses(const map<string, vector<string>>& busesToStops)
{
	if (busesToStops.empty())
	{
		cout << "No buses" << endl;
	}
	else
	{
		for (const auto&[bus, stops]: busesToStops)
		{
			cout << "Bus " << bus << ":";
			for (const auto& s: stops)
			{
				cout << " " << s;
			}
			cout << endl;
		}
		cout << endl;
	}
}

int main()
{
	/*AllBuses(busesToStops);
BusesForStop(stopsToBuses, "Marushkino");
StopsForBus(busesToStops, stopsToBuses, "32K");
NewBus(busesToStops, stopsToBuses, "32", {"Tolstopaltsevo", "Marushkino", "Vnukovo"});
NewBus(busesToStops, stopsToBuses, "32K", {"Tolstopaltsevo", "Marushkino", "Vnukovo", "Peredelkino", "Solntsevo", "Skolkovo"});
BusesForStop(stopsToBuses, "Vnukovo");
NewBus(busesToStops, stopsToBuses, "950", {"Kokoshkino", "Marushkino", "Vnukovo", "Peredelkino", "Solntsevo", "Troparyovo"});
NewBus(busesToStops, stopsToBuses, "272", {"Vnukovo", "Moskovsky", "Rumyantsevo", "Troparyovo"});
StopsForBus(busesToStops, stopsToBuses, "272");
AllBuses(busesToStops);*/

	map<string, vector<string>> busesToStops;
	map<string, vector<string>> stopsToBuses;

	int n;
	cin >> n;
	while (n)
	{
		string command;
		cin >> command;
		if (command == "ALL_BUSES")
		{
			AllBuses(busesToStops);
		}
		else if (command == "BUSES_FOR_STOP")
		{
			string stop;
			cin >> stop;
			BusesForStop(stopsToBuses, stop);
		}
		else if (command == "STOPS_FOR_BUS")
		{
			string bus;
			cin >> bus;
			StopsForBus(busesToStops, stopsToBuses, bus);
		}
		else if (command == "NEW_BUS")
		{
			string bus;
			cin >> bus;
			int stopCount;
			cin >> stopCount;
			vector<string> stops;
			while (stopCount){
				string stopName;
				cin >> stopName;
				stops.push_back(stopName);
				stopCount--;
			}
			NewBus(busesToStops, stopsToBuses, bus, stops);
		}
		--n;
	}


	return 0;
}
