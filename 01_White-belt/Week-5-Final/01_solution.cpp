// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>
#include <string>
#include <sstream>
#include <set>
#include <map>
#include <iomanip>

using namespace std;

// Реализуйте функции и методы классов и при необходимости добавьте свои

class Date {
 public:
  Date() = default;
  Date(int year, int month, int day) : year_(year), month_(month), day_(day) {
	if (month > 12 || month < 1) throw domain_error("Month value is invalid: " + to_string(month));
	if (day > 31 || day < 1) throw domain_error("Day value is invalid: " + to_string(day));
  }
  int GetYear() const {
	return year_;
  }
  int GetMonth() const {
	return month_;
  }
  int GetDay() const {
	return day_;
  }
  bool operator<(const Date &rhs) const {
	if (year_ < rhs.year_)
	  return true;
	if (rhs.year_ < year_)
	  return false;
	if (month_ < rhs.month_)
	  return true;
	if (rhs.month_ < month_)
	  return false;
	return day_ < rhs.day_;
  }
  bool operator>(const Date &rhs) const {
	return rhs < *this;
  }
  bool operator<=(const Date &rhs) const {
	return !(rhs < *this);
  }
  bool operator>=(const Date &rhs) const {
	return !(*this < rhs);
  }
  bool operator==(const Date &rhs) const {
	return year_ == rhs.year_ &&
		month_ == rhs.month_ &&
		day_ == rhs.day_;
  }
  bool operator!=(const Date &rhs) const {
	return !(rhs == *this);
  }
  friend ostream &operator<<(ostream &os, const Date &date) {
	os << setfill('0') << setw(4) << date.year_ << "-" << setw(2) << date.month_ << "-" << setw(2) << date.day_;
	return os;
  }
  friend istream &operator>>(istream &os, Date &date) {
	string raw_date;
	os >> raw_date;
	stringstream ss(raw_date);

	int year, month, day;
	if (ss >> year && ss.peek() == '-' && ss.ignore(1) && ss >> month && ss.peek() == '-' && ss.ignore(1) && ss >> day
		&& ss.eof()) {
	  date = Date(year, month, day);
	} else {
	  throw domain_error("Wrong date format: " + raw_date);
	}
	return os;
  }
 private:
  int year_;
  int month_;
  int day_;
};

class Database {
 public:
  void Add(const Date &date, const string &event) {
	db_[date].insert(event);
  }
  bool DeleteEvent(const Date &date, const string &event) {
	if (db_.count(date)) {
	  return db_[date].erase(event);
	}
	return false;
  }

  int DeleteDate(const Date &date) {
	if (db_.count(date)) {
	  int deleted_count = int(db_.at(date).size());
	  db_.erase(date);
	  return deleted_count;
	}
	return 0;
  }

  set<string> Find(const Date &date) const {
	if (db_.count(date)) {
	  return db_.at(date);
	}
	return {};
  }

  void Print() const {
	for (const auto&[kDate, kEvents]: db_) {
	  for (const auto &k_event: kEvents) {
		cout << kDate << " " << k_event << endl;
	  }
	}
  }
 private:
  map<Date, set<string>> db_;
};

int main() {

  Database db;

  string command;
  while (getline(cin, command)) {
	if (command.empty()) continue;

	try {
	  stringstream stream(command);
	  stream >> command;

	  if (command == "Print" && stream.eof()) {
		db.Print();
	  } else if (command == "Add") {
		Date date{};
		string event;
		stream >> date >> event;
		db.AddEvent(date, event);
	  } else if (command == "Del") {
		Date date{};
		stream >> date;
		if (stream.eof()) {
		  cout << "Deleted " << db.DeleteDate(date) << " events" << endl;
		} else {
		  string event;
		  stream >> event;
		  if (db.DeleteEvent(date, event)) {
			cout << "Deleted successfully" << endl;
		  } else {
			cout << "Event not found" << endl;
		  }
		}
	  } else if (command == "Find") {
		Date date{};
		stream >> date;
		auto events = db.Find(date);
		for (const auto &e: events) {
		  cout << e << endl;
		}

	  } else {
		throw domain_error("Unknown command: " + command);
	  }

	}
	catch (exception &error) {
	  cout << error.what() << endl;
	  break;
	}
  }

  return 0;
}

